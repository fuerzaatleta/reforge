import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import spanish from "./app/i18n/es";
import english from "./app/i18n/en";

// the translations
// (tip move them in a JSON file and import them)
const resources = {
    es: spanish,
    en: english
};

i18n
    .use(initReactI18next) // passes i18n down to react-i18next
    .init({
        resources,
        lng: "es",
        keySeparator: false, // we do not use keys in form messages.welcome
        interpolation: {
            escapeValue: false // react already safes from xss
        }
    });
export default i18n;
