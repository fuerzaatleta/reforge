import React from 'react';
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import App from './app/app';
import app from './src/AppFramework';
import './i18n';
import './app/mobx/mobx'
import {log} from './app/utils/logging';

const isHermes = () => !!global.HermesInternal ? "[ YES ]" : "[ NO ]";
log.info("Hermes javascript engine... " + isHermes());
AppRegistry.registerComponent(appName, () => App);
app.initialize();
