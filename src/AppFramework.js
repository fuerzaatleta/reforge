import React from 'react';
import WorkoutManager from './framework/modules/WorkoutManager';

class AppFramework {

    screen = "null";

    constructor(props) {
        this.navigation = React.createRef();
        this.workout = new WorkoutManager();
    }

    initialize() {
        // RNBootSplash.hide({ duration: 1000 });
        // setTimeout(() => RNBootSplash.hide({ duration: 500 }), 1000 )
    }

    /** Nombre de la pantalla actual **/
    getScreen() {
        return this.screen;
    }

    update() {

    }

    navigateTo(screen, params) {
        this.navigation.current.navigate(screen, params);
        this.screen = screen;
    }

    navigate(screen, key) {
        this.navigation.current.navigate(screen, key);
        this.screen = screen;
    }

    showModal(name) {
        const event = {};
        event.type = "MODAL";
        event.name = name;
        app.dispatch(event);
    }

    navigateBack() {
        this.navigation.current.goBack();
    }

    dispatch(event) {
        console.warn("DEPRECATED DISPATCH");
        // Nexus().dispatch(event);
    }
}

let app = new AppFramework();
export default app;
