import {Card, Content, Icon} from 'native-base';
import {Alert, Text, View} from 'react-native';
import {Colors, ProgressBar} from 'react-native-paper';
import React, {useState} from 'react';
import {LineChart} from 'react-native-chart-kit';
import { Dimensions } from "react-native";

const chartConfig = {
    backgroundGradientFrom: "#b5ffdd",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "#ffffff",
    backgroundGradientToOpacity: 0.5,
    color: (opacity = 1) => `rgba(45, 45, 45, ${opacity})`,
    strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: false // optional
};

const data = {
    labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio"],
    datasets: [
        {
            data: [20, 45, 40, 65, 75, 98, 124],
            color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`, // optional
            strokeWidth: 2 // optional
        }
    ],
    legend: ["Repeticiones"] // optional
};

const UserStats = () => {
    const charts = ["VOLUME", "WEIGHT", "FAILURES", "REPETITIONS", "SETS", "HOURS", "CALORIES"];
    let [state, setState] = useState(0);

    return (
        <View onTouchStart={(event) => onTouch(event)}>
            <ChartSwicher state={state} />
        </View>
    );

    function onTouch(event) {
        const screenDimensionX = Math.round(Dimensions.get('window').width);
        const halfScreenX = screenDimensionX / 2;
        const x = event.nativeEvent.locationX;
        if(x > halfScreenX)
            next();
        else
            previous();
    }

    function next() {
        if(state < charts.length)
            setState(++state);
        else if(state >= charts.length)
            setState(0);
    }

    function previous() {

    }
};

const ChartSwicher = (props) => {
    if(props.state === 0)
        return <WeightAnalytics/>;
    else if(props.state === 1)
        return <SetAnalytics/>;
    else
        return <WeightAnalytics/>;
};

const WeightAnalytics = () => {
    return (
        <View>
            <LineChart
                data={data}
                width={Dimensions.get("window").width}
                height={230}
                chartConfig={chartConfig}
            />
        </View>
    )
};

const SetAnalytics = () => {
    return (
        <View>
            <LineChart
                data={data}
                width={Dimensions.get("window").width}
                height={230}
                chartConfig={chartConfig}
            />
        </View>
    )
};

const FailureAnalytics = () => {
    return (
        <View>
            <LineChart
                data={data}
                width={Dimensions.get("window").width}
                height={230}
                chartConfig={chartConfig}
            />
        </View>
    )
};

export default UserStats;