import React, {useState} from 'react';
import {StyleSheet, Text, TouchableNativeFeedback, View} from 'react-native';
import {Card, Content, Icon} from 'native-base';
import {Badge, Colors, ProgressBar} from 'react-native-paper';
import Theme from '../../../app/theme/theme';
import app from '../../AppFramework';

const PersonalData = () => {
    let [notification] = useState(false);
    return (
        <View>
            {/*<Text style={styles.title}>Características Físicas</Text>*/}
            <View style={styles.container}>
                <TouchableNativeFeedback onPress={() => { app.navigateTo("ExercisesScreen")} }>
                    <View style={styles.box}>
                        <Icon type="FontAwesome5" style={[styles.icon, {fontSize: 29}]} name='book' />
                        <Text style={styles.text}>Ejercicios</Text>
                    </View>
                </TouchableNativeFeedback>
                <TouchableNativeFeedback onPress={() => { app.navigateTo("MetricsScreen")} }>
                    <View style={styles.box}>
                        <Icon style={[styles.icon, {fontSize: 29}]} type="Entypo" name='ruler' />
                        <View style={{flexDirection: "row", justifyContent: "center"}}>
                            { notification ? <Badge style={styles.badge} size={8} /> : <View/> }
                            <Text style={styles.text}>Medidas</Text>
                        </View>
                    </View>
                </TouchableNativeFeedback>
                <TouchableNativeFeedback onPress={() => { app.navigateTo("CalendarScreen")} }>
                    <View style={styles.box}>
                        <Icon style={[styles.icon, {fontSize: 29}]} type="FontAwesome" name='calendar-o' />
                        <Text style={styles.text}>Calendario</Text>
                    </View>
                </TouchableNativeFeedback>
            </View>
        </View>
    )
};

const styles = {
    badge: {
        flex: 0,
        marginTop: 5,
        marginBottom: 5,
        marginRight: 5,
    },
    container: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "center"
    },
    title: {
        fontSize: 18,
        color: "gray",
        marginBottom: 10,
    },
    icon: {
        color: "#a99d9d",
        textAlign: "center",
        marginBottom: 7,
        marginTop: 7,
    },
    text: {
        fontSize: 15,
        textAlign: "center"
    },
    box: {
        flex: 1,
        borderRadius: 5,
        borderColor: "#D4D4D4",
        borderWidth: 1,
        paddingTop: 20,
        paddingBottom: 20,
        margin: 5,
    },
};

export default PersonalData;
