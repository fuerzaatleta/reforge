import {Card, Content, Icon} from 'native-base';
import {Alert, Text, View, TouchableNativeFeedback} from 'react-native';
import {Colors, ProgressBar} from 'react-native-paper';
import React, {useState} from 'react';
import app from '../../AppFramework';

const FatigueMetrics = () => {
    let [fatigue, setFatigue] = useState(0);
    return (
        <View>
            <TouchableNativeFeedback>
                <Card style={styles.card}>
                    <View style={styles.row}>
                        <Text style={styles.txtTitle}>Fatiga Muscular (0%)</Text>
                        <Icon style={{color: "#656565", fontSize: 25}}
                              name='information-outline'
                              type="MaterialCommunityIcons"
                              onPress={() => btnInfo()} />
                    </View>
                    <ProgressBar style={styles.progress} progress={fatigue} color={Colors.green600} />
                    <View>
                        <View style={styles.row}>
                            <Icon style={{color: "#656565", fontSize: 26, flex: 0.12}}
                                  name='check-circle'
                                  type="MaterialCommunityIcons"/>
                            <Text style={styles.txtSubtitle}>Tu cuerpo está totalmente descansado y listo para la acción. ¿Empezamos?</Text>
                            {/*<Text style={styles.txtSubtitle}>Has entrenado muy bien y estás próximo a tu límite. ¡Enhorabuena!</Text>*/}
                        </View>
                    </View>
                </Card>
            </TouchableNativeFeedback>
        </View>
    );

    function btnInfo() {
        Alert.alert(
            'Fatiga Muscular',
            'La fatiga muscular es la pérdida parcial o total del músculo para producir fuerza. ' +
            'En sí es beneficiosa, pero un exceso puede provocar lesiones o síntomas por sobreentranemiento.',
            [
                {
                    text: 'Más Info',
                    onPress: () => console.log('Ask me later pressed')
                },
                { text: 'ENTENDIDO', onPress: () => console.log('OK Pressed') }
            ],
            { cancelable: false }
        );
    }
};


const styles = {
    txtTitle: {
        fontSize: 17,
        flex: 1,
    },
    txtSubtitle: {
        fontSize: 15,
        flex: 1,
    },
    card: {
        padding: 20,
        borderRadius: 9,
    },
    progress: {
        marginTop: 12,
        marginBottom: 11,
        borderRadius: 12,
        height: 7,
    },
    row: {
        flexDirection: "row",
        flex: 1,
        alignContent: "center",
        alignItems: "center",
    },
};

export default FatigueMetrics;
