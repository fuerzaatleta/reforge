import React from 'react';
import {View} from 'react-native';
import Theme from '../../app/theme/theme';

const Box = (props) => {
    return (
        <View style={[props.style, styles.container]}>
            {props.children}
        </View>
    )
};

const styles = {
    container: {
        backgroundColor: "white",
        borderColor: Theme.SEPARATOR_COLOR,
        borderWidth: 1,
        margin: 17,
        marginBottom: 12,
        borderRadius: 8,
    }
};

export default Box;
