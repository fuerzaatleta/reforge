import React, {useState} from 'react';
import {ActionSheet, Body, Card, CardItem, Container, Icon, Right, Text, View, Thumbnail} from 'native-base';
import {TouchableNativeFeedback, TouchableOpacity} from 'react-native';
import theme from '../../../app/theme/theme';
import app from '../../AppFramework';
import {Image} from 'react-native';
import {Assets} from '../../../app/assets/Assets';
import {ActivityIndicator, TouchableRipple} from 'react-native-paper';
import {observer} from 'mobx-react';
import {useWorkoutStore} from '../../../app/mobx/workout/deprecated-workout-store';

const SessionSelector = observer((props) => {
    const store = useWorkoutStore();
    return (
        <View style={session.workouts}>
            { renderSessions() }
        </View>
    );

    function renderSessions() {
        const list = [];
        store.workout().sessions.forEach((session, index) => {
            list.push(<Item key={index} index={index} session={session} />);
        });
        return list;
    }
});

const welcome = (props) => {
    return (
        <Container style={welcomeScreen.container}>
            <Image style={welcomeScreen.img} source={Assets.WORKOUT_ALERT}/>
            <Text style={welcomeScreen.title}>Oops. Tu rutina está vacía </Text>
            <Text style={welcomeScreen.subtitle}>Para añadir una sesión utiliza el botón (+) situado en la esquina inferior derecha.</Text>
        </Container>
    )
};

const content = (props) => {

};

const Item = (props) => {
    const store = useWorkoutStore();
    return (
            <TouchableNativeFeedback
                useForeground
                style={session.touchable}
                background={TouchableNativeFeedback.Ripple(theme.RIPPLE_COLOR, false)}
                onPress={() => onPressed()}>
                <View style={session.cardItem} bordered>
                    <View style={session.daybox}>
                        <Text style={session.daytext}>{ getWeekday() }</Text>
                    </View>
                    <View style={session.boxContent}>
                        <Text style={session.title}>{ getName() }</Text>
                        <Text style={session.subtitle}>Última vez: 10/2/2020</Text>
                    </View>
                    <View style={session.optionsBox}>
                        <TouchableRipple borderless style={session.cardOptions} onPress={() => btnEdit()} rippleColor="rgba(0, 0, 0, .1)">
                            <Icon style={session.icon} type="FontAwesome" name="ellipsis-v" size={70}/>
                        </TouchableRipple>
                    </View>
                </View>
            </TouchableNativeFeedback>
    );

    function onPressed() {
        const session = store.workout().sessions[props.index];
        store.select(session);
        app.navigateTo("SessionViewer");
    }

    function getWeekday() {
        switch (props.session.weekday) {
            case "monday": return "Lun";
            case "tuesday": return "Mar";
            case "wednesday": return "Mie";
            case "thursday": return "Jue";
            case "friday": return "Vie";
            case "saturday": return "Sab";
            case "sunday": return "Dom";
            default: return "Null";
        }
    }

    function getName() {
        if(props.session.name === "" || props.session.name == null)
            return "Sesión sin Título";
        return props.session.name;
    }

    function btnEdit() {
        const BUTTONS = ["Editar", "Duplicar", "Eliminar", "Cancelar"];
        const DESTRUCTIVE_INDEX = 3;
        const CANCEL_INDEX = 4;
        ActionSheet.show(
            {
                options: BUTTONS,
                cancelButtonIndex: CANCEL_INDEX,
                destructiveButtonIndex: DESTRUCTIVE_INDEX,
                label: "Sesión de Entrenamiento"
            },
            selection => { onActionSheetChanged(selection) }
        )
    }

    function onActionSheetChanged(index) {
        if(index === 2) {
            store.workout().removeSession(props.index);
        }
    }
};

const welcomeScreen = {
    img: {
        height: 135,
        width: 135,
        marginBottom: 20,
    },
    title: {
        fontSize: 21,
        color: "#4f4f4f"
    },
    subtitle: {
        fontSize: 16,
        color: "#4f4f4f",
        marginLeft: 20,
        marginRight: 20,
        textAlign: "center",
    },
    btn: {
        marginTop: 12,
        borderRadius: 20,
        elevation: 0,
    },
    btnContent: {
        paddingLeft: 15,
        paddingRight: 15,
    },
    container: {
        alignItems: "center",
        justifyContent: 'center',
    }
};

const session = {
    touchable: {
        borderRadius: 12,
    },
    daytext: {
        color: "#d34024",
        fontWeight: "bold",
        padding: 8,
        paddingLeft: 2,
        paddingRight: 2,
    },
    daybox: {
        borderWidth: 1.5,
        borderColor: "#d34024",
        paddingLeft: 12,
        paddingRight: 12,
        borderRadius: 10,
        marginRight: 22,
        marginLeft: 5,
    },
    title: {
        color: "#2e2e30",
        fontWeight: "bold",
    },
    subtitle: {
        color: "#474747",
        fontSize: 15,
    },
    workouts: {
        marginTop: 15,
        marginBottom: 10,
    },
    card: {
        elevation: 0,
        borderRadius: 7,
        // bordermWidth: 0,
        // borderBottomColor: "#ff3a28",
    },
    cardItem: {
        padding: 15,
        flexDirection: "row",
        alignItems: 'center',
        // elevation: 0,
        borderRadius: 7,
        height: 78,
        // borderWidth: 2,
    },
    cardOptions: {
        width: 35,
        height: 42,
        justifyItems: "center",
        textAlignVertical: "center",
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 6,
    },
    icon: {
        color: "#a4a4a4",
        fontSize: 22,
    },
    optionsBox: {
        position: 'absolute',
        right: 12,
    },
    boxContent: {
        // backgroundColor: "gray",
    },
};

export default SessionSelector;
