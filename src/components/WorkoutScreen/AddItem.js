import React from 'react';
import {View, Text} from 'react-native';
import {TouchableRipple} from 'react-native-paper';
import app from '../../AppFramework';

const Index = (props) => {
    return (
        <View style={styles.container}>
            <TouchableRipple style={styles.touchable} borderless onPress={() => app.navigateTo("AddSession")}>
                <Text style={styles.title}>Añadir días a esta rutina</Text>
            </TouchableRipple>
        </View>
    )
};

const styles = {
    touchable: {
        backgroundColor: "rgba(217,219,235,0.38)",
        borderRadius: 12,
        padding: 12,
        alignItems: "center",
    },
    container: {
        marginTop: 10,
        // paddingLeft: 20,
        // paddingRight: 20,
    },
    title: {
        color: "#292929",
        fontSize: 16,
        marginBottom: -3,
    },
    subtitle: {
        color: "white",
        fontSize: 15,
    }
};

export default Index;
