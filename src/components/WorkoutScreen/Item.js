import React from 'react';
import {Text, View} from 'react-native';
import {TouchableRipple} from 'react-native-paper';
import app from '../../AppFramework';

const Item = (props) => {

    return (
        <TouchableRipple style={styles.touchable} borderless onPress={onPress}>
            <View style={styles.container}>
                <View style={styles.day}>
                    <Text style={styles.dayText}>{props.day}</Text>
                </View>
                <View style={styles.description}>
                    <Text style={styles.title}>{props.name}</Text>
                    <Text style={styles.subtitle}>{props.exercises} ejercicios</Text>
                    <Text style={styles.subtitle}>Última vez: {props.last}</Text>
                </View>
            </View>
        </TouchableRipple>
    );

    function onPress() {
        app.workout.selectSession(props.id);
        app.navigateTo("SessionViewer");
    }
};

const styles = {
    touchable: {
        paddingLeft: 25,
        paddingRight: 25,
    },
    container: {
        flexDirection: "row",
        marginTop: 15,
        marginBottom: 15,
    },
    day: {
        backgroundColor: "#e63e26",
        flex: 0.55,
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
    },
    description: {
        marginLeft: 25,
    },
    dayText: {
        fontSize: 19,
        color: "white",
    },
    title: {
        fontSize: 22,
    },
    subtitle: {
        fontSize: 15,
        color: "#8f8f8f",
    },

};

export default Item;
