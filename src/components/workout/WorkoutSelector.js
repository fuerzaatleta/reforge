import React, { Component, useState } from "react";
import {StyleSheet, TouchableNativeFeedback, View, TouchableOpacity} from 'react-native';
import {Thumbnail, Icon, Button, ActionSheet} from "native-base"
import app from '../../AppFramework';
import {Assets} from '../../../app/assets/Assets';

const CIRCLE_SIZE = 1.35;
const THEME_COLOR = "#597aff";

const WorkoutSelector = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.workoutList}>
                { renderList() }
                <AddButton/>
            </View>
        </View>
    );

    function renderList() {
        const list = [];
        app.workout.selectedWorkout.sessions.forEach((workout, index) => {
            list.push(<WorkoutItem key={index} workout={workout} />);
        });
        return list;
    }
};

const WorkoutItem = (props) => {
    let currentWorkoutIndex = 0;
    return (
        <View style={styles.col}>
            <TouchableOpacity
                onLongPress={() => onLongPress()}
                onPress={() => onPress(props.workout)}>
                <Thumbnail style={itemStyles()} square scaleX={CIRCLE_SIZE} scaleY={CIRCLE_SIZE}
                           source={itemIcon(props.workout)}/>
            </TouchableOpacity>
        </View>
    );

    function itemIcon(workout) {
        const data = workout;
        let icon = Assets.PLACEHOLDER;
        if(data != null) {
            icon = Assets.get(data.icon);
        }
        return icon;
    }

    function itemStyles() {
        if(currentWorkoutIndex === props.index)
            return styles.selectedBtn;
        else
            return styles.btn;
    }

    function onLongPress() {
        app.navigateTo("UpdateWorkout");
        tracker.select(props.workout);
    }

    function onPress(workout) {
        tracker.select(workout);
    }

};

const AddButton = (props) => {
    return (
        <View style={styles.col}>
            <Button style={styles.addBtn} transparent onPress={() => app.navigateTo("AddSession")}>
                <Icon style={styles.icon} type="MaterialCommunityIcons" name="plus" />
            </Button>
        </View>
    )
};

const styles = StyleSheet.create({
    btn: {
        borderRadius: 7,
    },
    selectedBtn: {
        // opacity: 0.7,
        // backgroundColor: "#000000",
        // borderWidth: 1,
        borderColor: "#0087ea",

        borderRadius: 7,
    },
    addBtn: {
        width: 73,
        height: 77,
        borderRadius: 7,
        borderColor: THEME_COLOR,
        borderStyle: "dashed",
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    container: {
        marginBottom: 10,
    },
    workoutList: {
        width: "100%",
        flexDirection: "row",
    },
    col: {
        // backgroundColor: "gray",
        // scrollview: 0.22,
        width: 83,
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon: {
        color: THEME_COLOR,
    }
});

export default WorkoutSelector;
