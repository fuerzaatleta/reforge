import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Animated,
    TouchableOpacity,
    TouchableHighlight,
    Dimensions,
} from 'react-native';
import Interactable from 'react-native-interactable';
import {Assets} from '../../../app/assets/Assets';
import {RectButton} from "react-native-gesture-handler";
import Icon from 'react-native-vector-icons/MaterialIcons';

const screenWidth = Math.round(Dimensions.get('window').width);
const Screen = Dimensions.get('window');
const AnimatedIcon = Animated.createAnimatedComponent(Icon);

export class SwipeableElement extends Component {
    constructor(props) {
        super(props);
        this.deltaX = new Animated.Value(0);
        this.state = {isMoving: false, position: 1};
    }
    render() {
        const activeOpacity = this.state.position !== 1 ? 0.5 : 1;
        return (
            <Animated.View style={[styles.background, {
                backgroundColor: this.deltaX.interpolate({
                        inputRange: [0, 1],
                        outputRange: ['rgb(221,44,0)', 'rgb(56,142,60)'],
                        extrapolateRight: 'clamp',
                    }
                )}
            ]}>
                <View style={{position: 'absolute', right: 0, height: "100%", flexDirection: 'row', alignItems: 'center'}}>
                    <TouchableOpacity style={[styles.button]} onPress={this.onButtonPress.bind(this, 'trash')}>
                        <AnimatedIcon name={"delete-forever"} size={32} style={
                            [styles.icon, {
                                opacity: this.deltaX.interpolate({
                                    inputRange: [-75, -50],
                                    outputRange: [1, 0],
                                    extrapolateLeft: 'clamp',
                                    extrapolateRight: 'clamp'
                                }),
                                transform: [{
                                    scale: this.deltaX.interpolate({
                                        inputRange: [-75, -50],
                                        outputRange: [1, 0.7],
                                        extrapolateLeft: 'clamp',
                                        extrapolateRight: 'clamp'
                                    })
                                }]
                            }
                            ]} />
                    </TouchableOpacity>
                </View>

                <View style={{position: 'absolute', left: 0, height: "100%", flexDirection: 'row', alignItems: 'center'}}>
                    <TouchableOpacity style={[styles.button]} onPress={this.onButtonPress.bind(this, 'done')}>
                        <AnimatedIcon name={"edit"} size={29} source={Assets.PLACEHOLDER} style={
                            [styles.icon, {
                                opacity: this.deltaX.interpolate({
                                    inputRange: [50, 75],
                                    outputRange: [0, 1],
                                    extrapolateLeft: 'clamp',
                                    extrapolateRight: 'clamp'
                                }),
                                transform: [{
                                    scale: this.deltaX.interpolate({
                                        inputRange: [50, 75],
                                        outputRange: [0.7, 1],
                                        extrapolateLeft: 'clamp',
                                        extrapolateRight: 'clamp'
                                    })
                                }]
                            }
                            ]} />
                    </TouchableOpacity>
                </View>

                <Interactable.View
                    ref={el => this.interactableElem = el}
                    horizontalOnly={true}
                    snapPoints={[
                        {x: screenWidth, damping: 0.5, tension: 1000},
                        {x: 0, damping: 0.5, tension: 1000},
                        {x: -screenWidth, damping: 0.5, tension: 1000 }
                    ]}
                    // boundaries={{top: 0, bottom: 0}}
                    onSnap={this.onSnap.bind(this)}
                    onDrag={this.onDrag.bind(this)}
                    onStop={this.onStopMoving.bind(this)}
                    dragToss={0.01}
                    animatedValueX={this.deltaX}>
                    <TouchableHighlight onPress={this.onRowPress.bind(this)} activeOpacity={activeOpacity} underlayColor={'white'}>
                            {this.props.children}
                    </TouchableHighlight>
                </Interactable.View>

            </Animated.View>
        );
    }

    onSnap({nativeEvent}) {
        const { index } = nativeEvent;
        this.setState({position: index});
    }

    onRowPress() {
        const { isMoving, position } = this.state;
        if (!isMoving && position !== 1) {
            this.interactableElem.snapTo({index: 1});
        }
    }

    onDrag({nativeEvent}) {
        const { state } = nativeEvent;
        console.log("drag: " + state);
        if (state === 'start')
            this.setState({isMoving: true});
    }

    onStopMoving() {
        this.setState({isMoving: false});
    }

    onButtonPress(name) {
        alert(`Button ${name} pressed`);
    }
}

const styles = StyleSheet.create({
    background: {
        backgroundColor: "white",
    },
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    button: {
        width: 75,
        height: "100%",
        justifyContent: "center",
        alignItems: "center",
    },
    icon: {
        margin: 20,
        color: "white",
    },
});
