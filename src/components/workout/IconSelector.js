import React, {Component, useState} from 'react';
import {ScrollView, TouchableOpacity} from 'react-native';
import {Grid, Row} from 'react-native-easy-grid';
import {List, Thumbnail} from 'native-base';
import {Assets} from '../../../app/assets/Assets';

let selected = null;
let callback = null;

const IconSelector = (props) => {
    callback = props.callback;
    return (
        <ScrollView horizontal style={styles.scroll} showsHorizontalScrollIndicator={false}>
            <Grid>
                <Row>
                    <Item icon="ICON_RUNNING"/>
                    <Item icon="ICON_TNT"/>
                    <Item icon="ICON_DUMBELL"/>
                    <Item icon="ICON_GASMASK"/>
                    <Item icon="ICON_TIGER"/>
                    <Item icon="ICON_FIST"/>
                    <Item icon="ICON_CARDIO"/>
                </Row>
                <Row>
                    <Item icon="ICON_CALENDAR"/>
                    <Item icon="ICON_KETTLEBELL"/>
                    <Item icon="ICON_SHOE"/>
                    <Item icon="ICON_MACHINE"/>

                    <Item icon="ICON_CHICKEN"/>
                    <Item icon="ICON_GYMBUILDING"/>
                    <Item icon="ICON_BOXING"/>

                    <Item icon="ICON_ABS"/>
                    <Item icon="PLACEHOLDER"/>
                    <Item icon="PLACEHOLDER"/>
                    <Item icon="PLACEHOLDER"/>
                </Row>
            </Grid>
        </ScrollView>
    );
};

const Item = (props) => {

    return (
        <TouchableOpacity style={styles.touchable} activeOpacity={0.4} onPress={() => onPressed()}>
            <Thumbnail style={itemStyle()} square scaleX={1.25} scaleY={1.25} source={Assets.get(props.icon)}/>
        </TouchableOpacity>
    );

    function itemStyle() {
        if(selected === this)
            return [styles.item, styles.btnSelected];
        else
            return [styles.item];
    }

    function onPressed() {
        selected = this;
        callback(props.icon);
    }

};

const styles = {
    btnSelected: {
        opacity: 1,
        backgroundColor: "#000000",
    },
    scroll: {
        width: "100%",
        paddingTop: 7,
        paddingBottom: 7,
    },
    gallery: {
        marginTop: 15,
        backgroundColor: "gray",
    },
    item: {
        marginLeft: 9,
        margin: 9,
        borderRadius: 7,
        flex: 0.166,
    },
    touchable: {
        color: "#dd692f",
    }
};

export default IconSelector;
