import React, {Component} from 'react';
import {TouchableOpacity, Image, Animated, TouchableNativeFeedback } from 'react-native';
import {Body, Button, Icon, Left, ListItem, Right, Thumbnail} from 'native-base';
import theme from '../../../app/theme/theme';
import { StyleSheet, Text, View, I18nManager } from 'react-native';
import {SwipeableRow} from '../deprecated/SwipeableRow';

function ExerciseItem(props) {
    return (
        <SwipeableRow>
            <View style={sessionItem.container}>
                <View style={sessionItem.left}>
                    <Icon onLongPress={props.drag} style={sessionItem.draggable} type="MaterialCommunityIcons" name="drag-vertical"/>
                </View>
                <View style={sessionItem.center}>
                    <Thumbnail square large style={sessionItem.img} />
                </View>
                <View style={sessionItem.right}>
                    <Text style={sessionItem.title}>Press de Banca</Text>
                    <Text style={sessionItem.subtitle}>3 serires de 12 repeticiones</Text>
                </View>
            </View>
        </SwipeableRow>
    );
}

const sessionItem = {
    draggable: {
        color: "gray",
        fontSize: 30,
    },
    left: {
        justifyContent: "center",
        padding: 6,
    },
    img: {
        // width: "100%",
        // height: "100%",
        borderRadius: 12,
        backgroundColor: "#ffffff",
        borderColor: "#d5d5d5",
        borderWidth: 1,
    },
    container: {
        paddingTop: 2,
        paddingBottom: 2,
        backgroundColor: "rgb(255,255,255)",
        flexDirection: "row",
        borderColor: "rgb(220,224,229)",
        borderBottomWidth: 1,
    },
    title: {
        fontSize: 18,
    },
    subtitle: {
        fontSize: 14,
        color: theme.SUBTITLE_COLOR,
    },
    center: {
        justifyContent: "center",
        alignItems: "center",
        marginTop: 7,
        marginBottom: 7,
    },
    right: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 20,
    }
};

export default ExerciseItem;
