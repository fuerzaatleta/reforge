import {CheckBox, Text, View} from 'native-base';
import React, {useState} from 'react';
import theme from '../../../app/theme/theme';

let callback;

const WeekSelector = (props) => {
    callback = props.onValueChange;
    return (
        <View style={styles.container}>
            <InputItem name="Lun" value="monday"/>
            <InputItem name="Mar" value="tuesday"/>
            <InputItem name="Mie" value="wednesday"/>
            <InputItem name="Jue" value="thursday"/>
            <InputItem name="Vie" value="friday"/>
            <InputItem name="Sab" value="saturday"/>
            <InputItem name="Dom" value="sunday"/>
        </View>
    )
};

const InputItem = (props) => {
    const ACTIVE_COLOR = theme.PRIMARY_COLOR;
    const DISABLED_COLOR = "#646464";
    let [selected, setSelected] = useState(false);

    return (
        <View style={styles.inputItem}>
            <CheckBox
                style={styles.checkbox}
                color={selected ? ACTIVE_COLOR : DISABLED_COLOR}
                checked={selected}
                onPress={() => onPressed() }/>
            <Text style={styles.text}>{props.name}</Text>
        </View>
    );

    function onPressed() {
        callback(props.value);
        setSelected(!selected);
    }
};

const styles = {
    container: {
        marginTop: 15,
        marginBottom: 27,
        flexDirection: "row",
    },
    text: {
        paddingLeft: 5,
    },
    checkbox: {
        borderRadius: 2,
    },
    inputItem: {
        flex: 0.142,
    },
    title: {

    }
};

export default WeekSelector;
