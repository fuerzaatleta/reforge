import {Text, View} from 'react-native';
import {CheckBox, Icon, Input} from 'native-base';
import React,{useState} from 'react';
import theme from '../../../app/theme/theme';
import {Checkbox, TextInput} from 'react-native-paper';
import {SwipeableElement} from './SwipeableElement';
import {SwipeableRow} from '../deprecated/SwipeableRow';

const Index = (props) => {
    let [value, setValue] = useState("12");
    return (
        <SwipeableElement>
            <View style={styles.container}>
                <View style={styles.left}>
                    <Icon type="MaterialCommunityIcons" style={{color: "#34aa25", fontSize: 32}} name='check-circle' />
                    <Text> Hecho</Text>
                </View>
                <View style={styles.middle}>
                    <Text placeholder='25' style={styles.input}>25</Text>
                    <Text style={{fontSize: 22, color: "#c7c7c7", marginLeft: 20, marginRight: 20}}>x</Text>
                    <Text placeholder='50' style={styles.input}>25</Text>
                </View>
                <View style={styles.right}>
                    <Text>60s </Text>
                    <Icon type="MaterialCommunityIcons" style={{color: "#c53d1d", fontSize: 28}} name='timer' />
                </View>
            </View>
        </SwipeableElement>
    )
};

const styles = {
    container: {
        height: 72,
        paddingTop: 2,
        paddingBottom: 2,
        backgroundColor: "rgb(255,255,255)",
        flexDirection: "row",
        borderColor: theme.SEPARATOR_COLOR,
        borderBottomWidth: 1,
    },
    input: {
        textAlign: "center",
        marginTop: 10,
        width: 50,
        // height: 46,
        // backgroundColor: "white",
        fontSize: 24,
        color: "#c6c6c6",
    },
    left: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: 'center',
        flex: 0.25,
    },
    middle: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: 'center',
        flex: 0.5,
    },
    right: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: 'center',
        flex: 0.25,
    },
};

export default Index;
