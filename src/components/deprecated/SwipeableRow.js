import {Animated, default as I18nManager, StyleSheet, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Component} from 'react';
import {RectButton} from "react-native-gesture-handler";
import Swipeable from "react-native-gesture-handler/Swipeable";
import React from 'react';

const AnimatedIcon = Animated.createAnimatedComponent(Icon);

export class SwipeableRow extends Component {

    render() {
        const { children } = this.props;
        return (
            <Swipeable
                onSwipeableLeftWillOpen={this.props.onOpenLeft}
                onSwipeableRightWillOpen={this.props.onOpenRight}
                friction={1}
                leftThreshold={40}
                rightThreshold={40}
                renderLeftActions={this.renderLeftActions}
                renderRightActions={this.renderRightActions}
            >
                {children}
            </Swipeable>
        );
    }

    renderLeftActions = (progress, dragX) => {
        const scale = dragX.interpolate({
            inputRange: [0, 80],
            outputRange: [0, 1],
            extrapolate: 'clamp',
        });
        return (
            <RectButton style={styles.leftAction}>
                <AnimatedIcon
                    name="edit"
                    size={30}
                    color="#fff"
                    style={[styles.actionIcon, { transform: [{ scale }] }]}
                />
            </RectButton>
        );
    };

    renderRightActions = (progress, dragX) => {
        const scale = dragX.interpolate({
            inputRange: [-80, 0],
            outputRange: [1, 0],
            extrapolate: 'clamp',
        });
        return (
            <RectButton style={styles.rightAction}>
                <AnimatedIcon
                    name="delete-forever"
                    size={30}
                    color="#fff"
                    style={[styles.actionIcon, { transform: [{ scale }] }]}
                />
            </RectButton>
        );
    };
}

const styles = StyleSheet.create({
    leftAction: {
        flex: 1,
        // backgroundColor: 'rgb(56,142,60)',
        backgroundColor: 'rgb(221,44,0)',
        // justifyContent: 'scrollview-end',
        alignItems: 'center',
        flexDirection: I18nManager.isRTL ? 'row' : 'row-reverse'
    },
    actionIcon: {
        width: 30,
        marginHorizontal: 10
    },
    rightAction: {
        alignItems: 'center',
        flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row',
        backgroundColor: 'rgb(221,44,0)',
        flex: 1,
        // justifyContent: 'scrollview-end'
    }
});
