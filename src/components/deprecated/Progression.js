import React, {Component} from "react";
import {ImageBackground, StyleSheet, Text, TouchableNativeFeedback, View} from 'react-native';
import app from '../../AppFramework';
import * as Progress from 'react-native-progress';
import {Assets} from '../../../app/assets/Assets';

class Progression extends Component {
    render() {
        return (
                <TouchableNativeFeedback
                    useForeground
                    style={style.touchable}
                    onPress={() => app.navigateTo("WorkoutStats")}>
                    <View>
                        <ImageBackground
                            source={Assets.STATS}
                            resizeMode="stretch"
                            style={style.image}
                            imageStyle={style.image_imageStyle}>
                            <View style={style.view}>

                                    <Text style={style.weight}>1025kg</Text>
                                    <Text style={style.level}>Nivel 2 - Elefante</Text>
                                    <Progress.Bar style={style.progressbar} progress={0.75} width={240} height={9} color={"#c1ceff"} />
                            </View>
                        </ImageBackground>
                    </View>
                </TouchableNativeFeedback>
        );
    }
}

const style = StyleSheet.create({
    touchable: {
      borderRadius: 12,
    },
    title: {
        color: "#121212",
        fontSize: 20,
        marginBottom: 10,
    },
    view: {
        width: "100%",
        height: 200,
        // backgroundColor: "rgba(230, 230, 230,1)",
        borderRadius: 12,
        alignItems: "center",
        justifyContent: "center",
    },
    weight: {
        fontSize: 36,
        color: "white",
    },
    level: {
        fontSize: 15,
        color: "white",
    },
    progressbar: {
        marginTop: 22,
    },
    image: {
        resizeMode: "cover",
    },
    image_imageStyle: {
    borderRadius: 12,
    },


});

export default Progression;
