import AppFramework from '../../AppFramework';
import {useSelector} from 'react-redux';


class ExerciseSearch extends React.Component {

    keyword = useSelector(state => state.keyword);
    filters = useSelector(state => state.filter);

    exerciseFilters = {
        SHOULDER: 'shoulders',
        CHEST: 'chest',
        ABS: 'shoulders',
        BACK: 'back',
        ARMS: 'arms',
        LEGS: 'legs',

        NO_GEAR: 'noGear',
        SMITH_MACHINE: 'smithMachine',
        DUMBELL: 'dumbell',
        BARBELL: 'barbell',
        KETTLEBELL: 'kettlebell',
    };

    /** Realiza una búsqueda y retorna un serializeArray. **/
    searchExercise() {
        const exercises = app.exercises.collection;
    }

    /** Añade un filtro a la búsqueda **/
    addFilter(filter){

    }

    /** Elimina un filtro de la búsqueda **/
    removeFilter(filter) {

    }

    /** Elimina todos los filtros **/
    resetFilters() {

    }

    constructor(props) {
        super(props);
    }

}
