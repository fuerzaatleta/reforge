import React from 'react';
import {Icon, Thumbnail, View} from 'native-base';
import {StyleSheet, Text} from 'react-native';
import {useSelector} from 'react-redux';

const Completable = (props) => {
    return (
        <View>
            <Thumbnail square large
                       fadeDuration={0}
                       style={props.style}
                       source={props.source}/>
            <Overlay complete={props.completed}/>
        </View>
    );
};

const Overlay = (props) => {
    if(props.complete)
        return (
            <View style={styles.overlay}>
                <Icon style={styles.icon} type="FontAwesome5" name="check"/>
            </View>
        );
    return <View/>;
};

const styles = {
    img: {
        width: 88,
        height: 88,
        borderRadius: 12,
        backgroundColor: "#000000",
        borderColor: "#d5d5d5",
        borderWidth: 1,
    },
    overlay: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'rgba(50,211,118,0.75)',
        borderRadius: 12,
        marginBottom: 8,
        marginTop: 8,
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon: {
        fontSize: 25,
        color: "white",
    }
};

export default Completable;
