import React, { Component } from "react";
import {StyleSheet, View, Text, TouchableNativeFeedback} from 'react-native';
import { Icon } from "native-base";
import theme from '../../../app/theme/theme';
import {Col, Grid} from 'react-native-easy-grid';

function SessionItem(props) {
    return (
        <View style={styles.view}>
            <TouchableNativeFeedback useForeground style={styles.touchable}>
                <View>
                    <View style={styles.dayLabelRow}>
                        <View style={styles.dayLabel}>
                            <Text style={styles.weekday}>{props.weekday}</Text>
                        </View>
                        <Text style={styles.title}>{props.title}</Text>
                    </View>
                    <View style={styles.dayLabelRowFiller}></View>
                </View>
            </TouchableNativeFeedback>
        </View>
    );
}

const styles = StyleSheet.create({
    touchable: {

    },
    container: {
        backgroundColor: "rgba(255,255,255,1)",
        justifyContent: "center"
    },
    view: {
        width: "100%",
        height: 57,
        backgroundColor: "rgba(255,255,255,1)",
        alignSelf: "center"
    },
    dayLabel: {
        width: 85,
        // height: 26,
        backgroundColor: "rgba(240,72,49,1)",
        borderRadius: 12,
        justifyContent: "center"
    },
    title: {
        color: "rgba(0,0,0,1)",
        fontSize: 16,
        fontFamily: "roboto-500",
        marginLeft: 10,
        // marginTop: 4
    },
    dayLabelRow: {
        flexDirection: "row",
        paddingTop: 15,
        paddingBottom: 15,
    },
    dayLabelRowFiller: {
        flex: 1,
        flexDirection: "row"
    },
    icon: {
        color: "rgba(131,131,131,1)",
        fontSize: 25,
        marginRight: 9,
        marginTop: 19
    },
});

export default SessionItem;
