import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';


const Notification = (props) => {
    if(props.visible)
        return (
            <TouchableOpacity onPress={props.onPress}>
                <View style={styles.container}>
                    <Text style={styles.text}>{props.title}</Text>
                </View>
            </TouchableOpacity>
        );
    else
        return <View/>;
};

const styles = {
    container: {
        backgroundColor: "#2eb73a",
        justifyContent: "center",
        alignItems: "center",
        height: 37,
    },
    text: {
        fontSize: 16,
        color: "white",
    }

};

export default Notification;