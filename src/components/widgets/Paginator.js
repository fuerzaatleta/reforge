import React, {useState} from 'react';
import {StatusBar, StyleSheet, Text, View} from 'react-native';
import {Icon} from "native-base";
import {Dimensions} from "react-native";
import app from '../../AppFramework';
import {TouchableRipple} from 'react-native-paper';
import Theme from '../../../app/theme/theme';

const STATUSBAR_COLOR = "#d63110";
const STATUSBAR_CONTENT = "light";

const Paginator = (props) => {
    StatusBar.setBackgroundColor(STATUSBAR_COLOR);
    StatusBar.setBarStyle(STATUSBAR_CONTENT);
    let [currentPage, setCurrentPage] = useState(0);
    if(props.pages == null)
        props.pages = [];

    return (
        <View style={styles.root} onTouchStart={(event) => onScreenTouch(event)}>
            <PageMark size={props.pages.length} page={currentPage}/>
            <View style={styles.controls}>
                <Icon style={styles.close} name="close" onPress={() => exit()}/>
                <TouchableRipple style={styles.skipRipple} borderless onPress={() => exit()} rippleColor="rgba(0, 0, 0, .1)">
                    <Text style={styles.skip}>SALTAR</Text>
                </TouchableRipple>
            </View>
        <View style={styles.content}>
            { props.pages[currentPage] }
        </View>

        </View>
    );

    function exit() {
        app.navigateBack();
        StatusBar.setBackgroundColor("white", true);
        StatusBar.setBarStyle("dark-content", true);
    }

    function onScreenTouch(event) {
        const screenDimensionX = Math.round(Dimensions.get('window').width);
        const halfScreenX = screenDimensionX / 2;
        const x = event.nativeEvent.locationX;
        x > halfScreenX ? next() : previous();
    }

    function next() {
        if(currentPage < props.pages.length - 1) {
            setCurrentPage(++currentPage);
        } else if(currentPage >= props.pages.length - 1) {
            app.navigateBack();
        }
    }

    function previous() {
        if(currentPage > 0)
            setCurrentPage(--currentPage);
    }

    function getCurrentPage() {
        const current = props.pages[currentPage];
        if(current == null)
            return <View/>;
        return current;
    }
};

const PageMark = (props) => {
    return (
        <View style={styles.container}>
            { generate() }
        </View>
    );

    function generate() {
        const list = [];
        for (let i = 0; i < props.size; i++)
            list.push(<View key={i} style={style(i)}/>);
        return list;
    }

    function style(index) {
        if(props.page >= index)
            return styles.activeMarker;
        return styles.marker;
    }
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        backgroundColor: STATUSBAR_COLOR,
    },
    content: {
        flex: 1,
    },
    controls: {
        marginTop: 10,
        flexDirection: "row",
    },
    close: {
        flex: 1,
        fontSize: 34,
        color: "white",
        marginLeft: 30,
        marginRight: 30,
        textAlignVertical: "center",
        height: 30,
        width: 30,
    },
    closeRipple: {
        flex: 1,
        height: 40,
    },
    skipRipple: {
        marginLeft: 10,
        marginRight: 10,
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
    },
    skip: {
        fontSize: 15,
        color: "white",
        marginLeft: 25,
        marginRight: 25,
        marginTop: 7,
        marginBottom: 7,
        letterSpacing: 1.5,
    },
    subtitle: {
        color: 'white',
        fontSize: 16,
        textAlign: "center",
        // backgroundColor: "gray",
        margin: 5,
        marginBottom: 25,
    },
    card: {
        marginTop: 14,
        marginLeft: 10,
        marginRight: 10,
    },
    text: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 18,
    },
    container: {
        flexDirection: "row",
    },
    activeMarker: {
        height: 6,
        flex: 1,
        backgroundColor: "rgba(255,255,255,1)",
        marginRight: 2,
        marginLeft: 2,
    },
    marker: {
        height: 6,
        flex: 1,
        backgroundColor: "rgba(255,255,255,0.25)",
        marginRight: 2,
        marginLeft: 2,
    }
});

export default Paginator;
