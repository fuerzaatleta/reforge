import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground, Text, TouchableNativeFeedback} from "react-native";
import app from '../../AppFramework';
import theme from '../../../app/theme/theme';

function HorizontalButton(props) {
    return (
            <TouchableNativeFeedback
                useForeground
                style={styles.touchable}
                background={TouchableNativeFeedback.Ripple(theme.RIPPLE_COLOR, false)}
                onPress={() => app.navigateTo(props.screen)}>
                <View style={[styles.container, props.style]}>
                <ImageBackground
                    source={props.image}
                    resizeMode="repeat"
                    style={styles.image}
                    imageStyle={styles.image_imageStyle}>
                    <Text style={styles.title}>{props.name}</Text>
                </ImageBackground>
                </View>
            </TouchableNativeFeedback>
    );
}

const styles = StyleSheet.create({
    touchable: {
        borderRadius: 12,
    },
    container: {
        backgroundColor: "rgba(230, 230, 230,1)",
        borderRadius: 12,
        margin: 5,
    },
    image: {
        borderRadius: 12,
        flex: 1,
        overflow: "hidden",
        padding: 25,
    },
    image_imageStyle: {
        resizeMode: "cover",
    },
    title: {
        color: "rgba(255,255,255,1)",
        fontSize: 17,
        fontFamily: "roboto-500",
    }
});

export default HorizontalButton;
