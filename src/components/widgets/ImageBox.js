import {Body, Content, Left, ListItem, Thumbnail} from 'native-base';
import {Text} from 'react-native';
import React from 'react';

function index(props) {
    return (
        <ListItem thumbnail style={styles.item}>
            <Left>
                <Thumbnail square source={props.image} />
            </Left>
            <Body>
                <Text>{props.title}</Text>
                <Text note numberOfLines={3}>{props.content}</Text>
            </Body>
        </ListItem>
    )
}

const styles = {
   item: {
       borderBottomWidth: 0,
       borderTopWidth: 0,
   }
};

export default index;
