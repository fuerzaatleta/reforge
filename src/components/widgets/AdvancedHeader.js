import React, {Component} from 'react';
import {Body, Button, Container, Header, Icon, Left, Right, Title} from 'native-base';
import theme from '../../../app/theme/theme';
import {TouchableNativeFeedback} from 'react-native';
import app from '../../AppFramework';


function index(props) {
    return (
        <Header
            androidStatusBarColor={props.color}
            iosBarStyle={props.dark ? "dark-content" : "light-content"}
            style={styles.header}>
            <Left>
                <TouchableNativeFeedback
                    useForeground
                    background={TouchableNativeFeedback.Ripple(theme.RIPPLE_COLOR, true)}>
                    <Button transparent onPress={() => {app.navigateBack()}}>
                        <Icon name='arrow-back' />
                    </Button>
                </TouchableNativeFeedback>
            </Left>
            <Body>
                <Title>{props.title}</Title>
            </Body>
            <Right>
            </Right>
        </Header>
    );
}

const styles = {
    header: {
        backgroundColor: theme.STRENGTH_HEADER,
        color: "white",
    },
    icon: {
        color: "white",
    },
    left: {
        flex : 0.,
    }
};

export default index;
