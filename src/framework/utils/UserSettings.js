import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

class UserSettings {

    static instance = null;

    constructor() {
        this.settings = new Map();
        this._recoverFromCloud();
    }

    setValue(key, value) {
        const obj = {};
        obj[key] = value;

        if(key == null || key === "") {
            console.warn("Triying to apply a setting with an empty key");
        } else {
            firestore()
                .collection("user_settings")
                .doc(auth().currentUser.uid)
                .set(obj)
                .done();
        }
    }

    getValue(key) {
        firestore()
            .collection("user_settings")
            .doc(auth().currentUser.uid)
            .get()
            .done();
    }

    _recoverFromCloud() {

    }

}

let instance;
const Singleton = () => {
    if(instance == null)
        instance = new UserSettings();
    return instance;
};

export default Singleton;