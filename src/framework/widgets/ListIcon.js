import React from 'react';
import {View, Image, Text} from 'react-native';
import {Icon} from 'native-base';

const ListIcon = (props) => {
    return (
        <View style={[styles.container, props.styles]}>
            <Icon name="check" type="FontAwesome5" style={styles.icon} />
            <Text style={styles.title}>{props.text}</Text>
        </View>
    )
};

const styles = {
    container: {
        backgroundColor: "#fff",
        flexDirection: "row",
    },
    title: {
        width: "95%",
        fontSize: 17,
    },
    icon: {
        fontSize: 23,
        marginRight: 14,
        color: "#d64320",
        textAlignVertical: "center",
        textAlign: "center",
        padding: 5,
    }
};

export default ListIcon;
