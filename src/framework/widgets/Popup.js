import React, {useState} from 'react';
import Modal from 'react-native-modal';

const Popup = (props) => {
    // const event = useSelector(state => state.Nexus);
    const event = {};
    let [visible, setVisible] = useState();

    if(event.event.name === props.name && !visible) {
        setVisible(true);
    }

    return (
        <Modal backdropOpacity={0} isVisible={visible}>
            {props.children}
        </Modal>
    )
};

export default Popup;
