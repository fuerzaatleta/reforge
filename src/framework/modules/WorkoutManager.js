
class WorkoutManager {

    constructor() {
        this.selectedWorkout = null;
        this.selectedSession = null;
        this.selectedExercise = null;
        this.workouts = [];
    }

    select(item) {
        console.warn("[WARN] Calling a deprecated function.");
    }

    save() {
        console.warn("[WARN] Calling a deprecated function.");
    }

    async load() {
        console.warn("[WARN] Calling a deprecated function.");

    }

    selectSession(id) {
        console.warn("[WARN] Calling a deprecated function.");
    }

    selectExercise(id) {
        console.warn("[WARN] Calling a deprecated function.");
    }


}
export default WorkoutManager;
