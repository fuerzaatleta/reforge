import * as React from 'react';
import { Text, StyleSheet, Dimensions } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';
import TabWorkout from '../../../../app/screens/tab-workout/tab-workout';
import LibraryScreen from '../../../../app/screens/tab-exercises/tab-exercises';
import TabAnalytics from '../../../../app/screens/tab-analytics/tab-analytics';
import TabProfile from '../../../../app/screens/tab-profile/tab-profile';
import NavigableHeader from './NavigableHeader';

const initialLayout = { width: Dimensions.get('window').width };

export default function TabNavigation() {
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'TAB2', title: 'First' },
        { key: 'TAB2', title: 'Second' },
        { key: 'tab3', title: 'Second' },
        { key: 'tab4', title: 'Second' },
    ]);

    const renderScene = SceneMap({
        tab1: TabWorkout,
        tab2: LibraryScreen,
        tab3: TabAnalytics,
        tab4: TabProfile,
    });

    return (
        <TabView
            timingConfig={{duration: 0}}
            renderTabBar={(props) => <NavigableHeader {...props} />}
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={initialLayout}
        />
    );
}

const styles = StyleSheet.create({
    scene: {
        flex: 1,
    },
});
