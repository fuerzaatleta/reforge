import React, {useContext, useState} from 'react';
import {View, Image, TouchableOpacity, StatusBar} from 'react-native';
import {Thumbnail} from 'native-base';
import {Assets} from '../../../../app/assets/Assets';
import Theme from '../../../../app/theme/theme';
import { TouchableRipple } from 'react-native-paper';
import {observer} from 'mobx-react';
import AsyncStorage from '@react-native-async-storage/async-storage';

/* AddSession of the selected icon */
let selection = "TAB1";

let nav = null;
let current = null;
const ICON_SIZE = 26;
const NavigableHeader = ({ state, descriptors, navigation, position }) => {
    nav = navigation;
    current = state.index;
    // navigation.navigate("TAB3");
    return (
            <View style={styles.header}>
                <View style={styles.container}>
                    <Tab id={"TAB1"}
                         index={0}
                         current={current}
                         iconActive={Assets.TAB1_ACTIVE}
                         iconDisabled={Assets.TAB1_DISABLED}
                         style={styles.tab}/>
                    <Tab id={"TAB2"}
                         index={1}
                         current={current}
                         iconActive={Assets.TAB2_ACTIVE}
                         iconDisabled={Assets.TAB2_DISABLED}
                         style={styles.tab}/>
                    <Tab id={"TAB3"}
                         index={2}
                         current={current}
                         iconActive={Assets.TAB3_ACTIVE}
                         iconDisabled={Assets.TAB3_DISABLED}
                         style={styles.tab}/>
                    <Tab id={"TAB4"}
                         index={3}
                         current={current}
                         iconActive={Assets.TAB4_ACTIVE}
                         iconDisabled={Assets.TAB4_DISABLED}
                         style={styles.tab}/>
                </View>
            </View>
    )
};

const Tab = observer((props) => {

    return (
        <View style={styles.iconContainer}>
            <TouchableRipple style={styles.touchable} onPress={() => onSelected()}>
                <Thumbnail fadeDuration={0} style={styles.icon} square source={ getIcon() }/>
            </TouchableRipple>
        </View>
    );

    function onSelected() {
        selection = props.id;
        AsyncStorage.setItem("current_tab", props.id);
        nav.navigate(props.id);

    }

    function getIcon() {
        if(props.index === props.current)
            return props.iconActive;
        else
            return props.iconDisabled;
    }
});

const styles = {
    touchable: {
        // backgroundColor: "yellow",
        width: "100%",
        height: "100%",
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        height: 56,
        // borderBottomWidth: 1,
        borderBottomColor: Theme.SEPARATOR_COLOR,
        elevation: 3,
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#fff",
        flexDirection: "row",
        flex: 1,
        // borderTopWidth: 1,
        borderColor: "#eaeaea",
    },
    tab: {

    },
    iconContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon: {
        height: ICON_SIZE,
        width: ICON_SIZE,
    },
};

export default NavigableHeader;
