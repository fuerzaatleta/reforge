import {toHHMMSS} from '../../utils/StringUtil';

class RestTimer {

    timeInterval = null;
    time = 59;

    start(seconds) {
        if(this.timeInterval != null) {
            console.warn("Rest timer already running.")
        } else {
            this.time = seconds;
            this.timeInterval = setInterval(this.restInterval.bind(this), 1000);
        }
    }

    cancel() {
        this.time = 0;
        clearInterval(this.timeInterval);
    }

    restInterval() {
        // Nexus().dispatch("UPDATE_TIMER");
        this.time = this.time - 1;
        if(this.time <= 0) {
            this.time = 0;
            clearInterval(this.timeInterval);
        }
    }

    getRestTime() {
        return toHHMMSS(this.time);
    }

}
export default RestTimer;
