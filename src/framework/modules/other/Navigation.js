import React from 'react';
import TabProfile from '../../../../app/screens/tab-profile/tab-profile';
import TabExercises from '../../../../app/screens/tab-exercises/tab-exercises';
import TabAnalytics from '../../../../app/screens/tab-analytics/tab-analytics';
import NavigableHeader from './NavigableHeader';
import {observer} from 'mobx-react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import TabWorkout from '../../../../app/screens/tab-workout/tab-workout';

const Navigation = observer(() => {
    const Tab = createMaterialTopTabNavigator();
    return (
        <Tab.Navigator
            sceneContainerStyle={styles.navigator}
            screenOptions={options}
            tabBar={(props) => <NavigableHeader {...props} />}>
            <Tab.Screen name="TAB1" component={TabWorkout} />
            <Tab.Screen name="TAB2" component={TabExercises} />
            <Tab.Screen name="TAB3" component={TabAnalytics} />
            <Tab.Screen name="TAB4" component={TabProfile} />
        </Tab.Navigator>
    );
});

const styles = {
    container: {
        flex: 1,
        backgroundColor: "#fff",
    },
    navigator: {
        backgroundColor: "#fff",
    }
};

const options = {
    tabBarOptions: {
        activeTintColor: '#fff',
        inactiveTintColor: '#fff',
        activeBackgroundColor: '#fff',
        inactiveBackgroundColor: '#fff',
        style: {
            backgroundColor: '#fff',
        },
    },
};

export default Navigation;
