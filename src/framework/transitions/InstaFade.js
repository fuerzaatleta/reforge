const enter = {
    animation: 'spring',
    config: {
        // stiffness: 25,
        // damping: 1,
        // mass: 1,
        overshootClamping: true,
        restDisplacementThreshold: 0.01,
        restSpeedThreshold: 0.01,
    },
};

const exit = {
    animation: 'spring',
    config: {
        stiffness: 90000,
        damping: 500,
        mass: 1,
        overshootClamping: true,
        restDisplacementThreshold: 0.01,
        restSpeedThreshold: 0.01,
    },
};

const index = {
    headerShown: false,
    transitionSpec: {
        open: enter,
        close: exit,
    },
};

export default index;
