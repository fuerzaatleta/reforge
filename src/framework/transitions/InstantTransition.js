const transition = {
    animation: 'spring',
    config: {
        stiffness: 90000,
        damping: 500,
        mass: 1,
        overshootClamping: true,
        restDisplacementThreshold: 0.01,
        restSpeedThreshold: 0.01,
    },
};

const index = (name) => {
    let header = true;
    if (name === null || name === undefined) {
        header = false;
    }
    return {
        title: name,
        headerShown: header,
        transitionSpec: {
            open: transition,
            close: transition,
        },
    }
};

export default index;
