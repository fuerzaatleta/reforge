const transition = {
    animation: 'fade',
    config: {
        stiffness: 90000,
        damping: 500,
        mass: 1,
        overshootClamping: true,
        restDisplacementThreshold: 0.01,
        restSpeedThreshold: 0.01,
    },
};

const index = {
    headerShown: false,
    transitionSpec: {
        open: transition,
        close: transition,
    },
};

export default index;
