import DirectusSDK from '@directus/sdk-js';
import ExerciseTemplate from './deprecated/workout/datastore/ExerciseTemplate';
import AsyncStorage from '@react-native-async-storage/async-storage';


/**
 * Almacena contenidos en una base de datos local permitiendo
 * el acceso instantaneo a esta información. */
class ExerciseDatabase {
    /* Opción de Desarrollo */
    FORCE_UPDATE = true;
    collection = [];

    constructor() {
        this.directus = new DirectusSDK({
            url: "https://directus.fuerzaatleta.com/",
            project: "fuerzaatleta",
        });
    }

    async OfflineInitialization() {
        console.log("Recovering exercise database data.");
        let result = [];
        let collection = null;
        const json = await AsyncStorage.getItem("EXERCISE_DATA");
        if(json !== null && json !== undefined)
            collection = JSON.parse(json);
        if(collection == null)
            return [];
        else {
            collection.forEach((item) => {
                const obj = new ExerciseTemplate();
                Object.assign(obj, item);
                obj.directus = this.directus;
                obj.initialize();
                result.push(obj);
            });
        }
        return result;
    }

    onRequestReceived(response) {
        console.log("Update available. updating revision " + this.latest + "...");
        this.collection = [];
        response.data.forEach(request => {
            const exercise = new ExerciseTemplate(this.directus);
            exercise.setID(request.id);
            exercise.setName(request.name);
            exercise.setDescription(request.description);
            exercise.setInstructions(request.instructions);
            exercise.setMechanics(request.mechanics);
            exercise.setImage(request.image1, request.image2);
            exercise.setMuscleGroup(request.muscle_group);
            exercise.setStatus(request.status);
            exercise.initialize();
            this.collection.push(exercise);
        });
        console.log("Update finished with " + this.collection.length + " elements");
    }

    async hasUpdates() {
        const request  = await this.directus.api.request("get", "/revisions", {single: 1, sort: "-id"});
        this.latest = request.data.id.toString();
        this.current = await AsyncStorage.getItem("CURRENT_REVISION");
        console.log("Local Revision: " + this.current);
        console.log("Server Revision: " + this.latest);
        return this.latest !== this.current;

    }

    async update() {
        if(this.FORCE_UPDATE)
            await AsyncStorage.setItem("EXERCISE_DATA", "");

        const hasUpdates = await this.hasUpdates();
        const items = await this.OfflineInitialization();

        if(hasUpdates === false && items.length > 0) {
            console.log("No updates available");
            this.collection = items;
        } else {
            await this.directus
                .getItems("exercises")
                .then(response =>  this.onRequestReceived(response))
                .catch(error => { console.log("Directus: " + error) });
            if(this.latest != null)
                await AsyncStorage.setItem("CURRENT_REVISION", this.latest);
            if(this.collection != null)
                await AsyncStorage.setItem("EXERCISE_DATA", JSON.stringify(this.collection));
        }
    }

    search(name, filter): ExerciseTemplate {
        let results = [];
        this.collection.forEach((item) => {
            const query = name.toLowerCase();
            const itemName = item.name.toLowerCase();
            const published = item.status === "published";
            if(itemName.includes(query) && published)
                results.push(item);
        });
        return results;
    }

    searchByID(index): ExerciseTemplate {
        let item = this.collection.find((item) => item.id === index);
        if(item === null || item === undefined)
            item = new ExerciseTemplate();
        return item;
    }

    store() {
        AsyncStorage.setItem("exercises", JSON.stringify(this.collection)).done();
    }
}

export default ExerciseDatabase;
