import {Assets} from '../../assets/Assets';
import {makeAutoObservable} from 'mobx';
import {Content} from '../../mobx/content/content-store';
import Set from './set';
import Serializer from '../serializer';
import {log} from '../logging';

class Exercise {

    id = 0;
    sets = [];
    DEFAULT_SET_NUMBER = 3;

    constructor(id) {
        makeAutoObservable(this);
        this.id = id;
        for (let i = 0; i < this.DEFAULT_SET_NUMBER; i++) {
            this.addExerciseSet();
        }
    }

    /** Devuelve un 'string' que corresponde a su nombre **/
    getName() {
        return this.getContent().name;
    }

    /** Devuelve la URL de la miniatura **/
    getThumbnail() {
        // const thumbnail = this.getContent()?.thumbnail?.data.full_url;
        const thumbnail = this.getContent()
        alert(thumbnail);
        if(thumbnail == null)
            return Assets.PLACEHOLDER;
        else
            return {uri: thumbnail.url}
    }

    getExerciseSet(index) {
        if(index == null || !Number.isInteger(index)) {
            console.info("incorrect index parameter");
            return new Set();
        } else if (index < 0 || index >= this.sets.length) {
            console.info("index out of bounds");
            return new Set();
        } else {
            return this.sets[index];
        }
    }

    /** Añade un set al ejercicio. **/
    addExerciseSet(completed = false, weight = 0, repetitions = 0) {
        const set = new Set();
        set.completed = completed;
        set.repetitions = repetitions;
        set.weight = weight;
        this.sets.push(set);
    }

    getRepetitions(index) {
        const reps = this.sets[index]?.repetitions;
        if(reps == null)
            return 0;
        return reps;
    }

    getSetNumber() {
        return this.sets.length;
    };

    getContent(): Exercise{
        const content = Content.exercises.searchByID(this.id);
        if (content == null) {
           log.warn("Exercise ID is invalid");
        } else {
            return content;
        }
    }

    getAnimation() {
        let frames = [];
        frames.push({ uri: this.getContent()?.full_image_a?.data.full_url });
        frames.push({ uri: this.getContent()?.full_image_b?.data.full_url });
        return frames;
    }

    getVideo() {
        if (this.getContent().video === null) {
           console.info("Unable to load video");
        } else {
            return this.getContent().video?.data?.full_url;
        }
    }

    deleteSet(index) {
        console.log("deleting" + index);
        this.sets.splice(index, 1);
    }

    /** Devuelve 'true' si todos los sets han sido completados **/
    isComplete() {
        let result = true;
        if(this.sets == null) {
            console.info("No sets available");
            return false;
        } else {
            this.sets.forEach((value) => {
                if(!value.completed) {
                    result = false;
                }
            });
            return result;
        }
    }

    reset() {
        if(this.sets == null) {
            console.warn("sets variable is null");
        } else {
            this.sets.forEach((item: Set) => item.complete(false))
        }
    }

    toJSON() {
        return {
            id: this.id,
            repetitions: this.repetitions,
            sets: Serializer.serializeArray(this.sets),
        }
    }

    deserialize(json) {
        if(json == null) {
            console.warn("[WARN] Exercise JSON is null");
        } else {
            const sets = [];
            json.sets.forEach((json) => {
                const set = new Set();
                set.deserialize(json);
                sets.push(set);
            });
            this.id = json.id;
            this.repetitions = json.repetitions;
            this.sets = sets;
        }
    }
}

export default Exercise;
