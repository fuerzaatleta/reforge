import {makeAutoObservable} from 'mobx';

export const SetTypes = {
    NORMAL: 0,
    WARMUP: 1,
    MUSCLE_FAILURE: 2,
    DROPSET: 3,
};

class Set {
    type = SetTypes.NORMAL;
    weight = 0;
    repetitions = 0;
    completed = false;

    constructor() {
        makeAutoObservable(this);
    }

    complete(value) {
        if(value != null) {
            this.completed = value;
            console.log("Exercise set state changed: " + this.completed);
            return this.completed
        } else {
            this.completed = !this.completed;
            console.log("Exercise set state changed: " + this.completed);
            return this.completed;
        }
    }

    getLast() {
        if (this.lastWeight == null || this.lastRepetitions == null)
            return "-";
        else {
            return this.weight + " x " + this.repetitions;
        }
    }

    setType(value) {
        if(value < 0 || value > 3) {
            console.warn("Invalid set type");
        } else {
            this.type = value;
        }
    }

    setWeight(value) {
        if(value < 0) {
            this.lastWeight = this.weight;
            this.weight = 0;
        } else {
            this.lastWeight = this.weight;
            this.weight = value;
        }
    }

    setRepetitions(value) {
        if (value < 0) {
            this.lastRepetitions = this.repetitions;
            this.repetitions = 0;
        } else {
            this.lastRepetitions = this.repetitions;
            this.repetitions = value;
        }
    }

    toJSON() {
        return {
            type: this.type,
            repetitions: this.repetitions,
            weight: this.weight,
            completed: this.completed,
        }
    }

    deserialize(json) {
        this.type = json.type;
        this.repetitions = json.repetitions;
        this.weight = json.weight;
        this.completed = json.completed;
    }
}
export default Set;
