import Session from './session';
import {makeObservable, observable, action} from 'mobx';
import {WorkoutStoreInstance} from '../../mobx/workout/deprecated-workout-store';
import Serializer from '../serializer';

class Workout {
    name = "Entrenamiento sin Título";
    goal = 0;
    description = "undefined";
    sessions = [];

    constructor() {
        makeObservable(this, {
            name: observable,
            description: observable,
            sessions: observable,
            addSession: action,
            removeSession: action,
        });
    }

    addSession(name, weekday) {
        const session = new Session(name, weekday);
        this.sessions.push(session);
        WorkoutStoreInstance.save();
    }

    removeSession(index) {
        this.sessions.splice(index, 1);
        WorkoutStoreInstance.save();
    }

    toJSON() {
        return {
            name: this.name,
            description: this.description,
            sessions: Serializer.serializeArray(this.sessions),
        }
    }

    deserialize(json) {
        if(json == null) {
            console.warn("[WARN] Workout JSON is null");
        } else {
            if(json.sessions === null)
                json.sessions = [];

            const sessions = [];
            json.sessions?.forEach((sessionJSON) => {
                const session = new Session();
                session.deserialize(sessionJSON);
                sessions.push(session);
            });
            this.name = json.name;
            this.description = json.description;
            this.sessions = sessions;
        }
        return this;
    }

    setTitle() {

    }

    getTitle() {

    }

    setDescription() {

    }

    getDescription() {

    }

    setDifficulty() {

    }

    getDifficulty() {

    }

    setGoal() {
        switch (this.goal) {
            case "undefined":
                return "Indefinido";
            case "bulking":
                return "Musculación";
            case "cutting":
                return "Definición";
            case "mantainance":
                return "Mantenimiento";
        }
    }

    getGoal() {
        return "[development]"
    }

    getSession(id) {
        const result = this.sessions[id];
        if(this.sessions.length === 0) {
            return new Session();
        } else {
            return result;
        }
    }

    getNumberOfSessions() {
        return this.sessions.length;
    }

}
export default Workout;
