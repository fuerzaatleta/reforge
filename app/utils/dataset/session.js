import Exercise from './exercise';
import {makeAutoObservable} from 'mobx';
import Serializer from '../serializer';

const EXERCISE_LIMIT = 20;

class Session {
    name = "";
    weekday = "";
    exercises = [];

    constructor(name, weekday) {
        makeAutoObservable(this);
        this.name = name;
        this.weekday = weekday;
    }

    addExercise(id) {
        console.info("Current number of exercises is " + this.exercises.length);
        if(this.exercises >= EXERCISE_LIMIT) {
            console.info("Maximun number of exercises allowed is " + EXERCISE_LIMIT)
        } else {
            const exercise = new Exercise(id);
            this.exercises.push(exercise);
        }
    }

    addExercises(ids) {
        if (ids == null) {
            console.warn("Null passed instead of an serializeArray");
        } else if(!Array.isArray(ids)) {
            console.warn("Parameter must be an serializeArray")
        } else {
            ids.forEach((item) =>  {
                const exercise = new Exercise(item);
                this.exercises.push(exercise);
            });
        }
    }

    count() {
        return this.exercises.length;
    }

    delete(index) {
        this.exercises.splice(index, 1);
    }

    reset() {
        if(this.exercises == null) {
            console.warn("exercises is null");
        } else {
            this.exercises.forEach((item: Exercise) => item.reset())
        }
    }

    duplicate(index) {
        console.info("duplicating session...");
        const item = this.exercises[index];
        const exercise = new Exercise();
        exercise.deserialize(item.toJSON());
        this.exercises.push(exercise);
    }

    getExercises() {
        return this.exercises;
    }

    getExercise(index): Exercise {
        const exercise = this.exercises[index];
        if(exercise == null) {
            console.warn("[WARN] Retrieving null exercise: AddSession: " + index);
            return new Exercise();
        }
        return exercise;
    }

    setExercises(exercises) {
        if(exercises == null) {
            log.warn("Exercise list is null.");
        } else if(!(exercises[0] instanceof Exercise)) {
            log.warn("Exercise list must have an exercise");
        } else {
            this.exercises = exercises;
            log.debug("Exercises added");
        }
    }

    toJSON() {
        return {
            name: this.name,
            weekday: this.weekday,
            exercises: Serializer.serializeArray(this.exercises),
        }
    }

    deserialize(json) {
        if(json == null) {
            console.warn("[WARN] Session JSON is null");
        } else {
            const exercises = [];
            json.exercises.forEach((exerciseJSON) => {
                const exercise = new Exercise();
                exercise.deserialize(exerciseJSON);
                exercises.push(exercise);
            });
            this.name = json.name;
            this.weekday = json.weekday;
            this.exercises = exercises;
        }
    }
}
export default Session;
