import {makeAutoObservable} from 'mobx';

class Timer {

    open = false;
    startTime = 60;
    timer = 60;
    interval = null;
    running = false;

    constructor() {
        makeAutoObservable(this);
    }

    start() {
        if(this.running) {
            console.info("timer already running");
        } else {
            this.interval = setInterval(this.update.bind(this), 1000);
            this.timer = this.startTime;
            this.running = true;
        }
    }

    stop() {
        clearInterval(this.interval);
        this.timer = 0;
        this.running = false;
        this.open = false;
    }

    update() {
        if(this.timer <= 0) {
            this.stop();
        } else {
            this.timer -= 1;
            console.log(this.timer);
        }
    }

    addTime(seconds) {
        this.startTime += seconds;
        this.timer += seconds;
    }

    substractTime(seconds) {
        this.startTime -= seconds;
        this.timer -= seconds;
        if(this.timer < 0) {
            this.timer = 0;
        }
    }

    getTime() {
        return this.timer;
    }

    getProgress() {
        return (100 * this.timer) / this.startTime
    }
}
export default Timer;
