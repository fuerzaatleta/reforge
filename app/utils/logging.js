import { logger, consoleTransport } from "react-native-logs";

const config = {
    severity: "log",
    transport: consoleTransport,
    transportOptions: {
        color: "ansi", // custom option that color consoleTransport logs
    },
    levels: {
        debug: 0,
        info: 1,
        warn: 2,
        error: 3,
    },
    async: true,
    dateFormat: "time",
    printLevel: true,
    printDate: true,
    enabled: true,
};

export const log = logger.createLogger(config);
