import DirectusSDK from '@directus/sdk-js';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {log} from './logging';

/**
 * Almacena contenidos en una base de datos local permitiendo
 * el acceso instantaneo a esta información. */
class Collection {

    constructor(template, collectionID, params = "") {
        if(collectionID == null) {
            collectionID = "MISSING_ID";
            log.error("[NETWORK] Missing Collection ID on constructor");
        }
        this.loaded = false;
        this.template = template;
        this.collection = null;
        this.params = params;
        this.collectionID = collectionID;
        this.directus = new DirectusSDK({
            url: "https://directus.fuerzaatleta.com/",
            project: "fuerzaatleta",
        });
    }

    async initialize() {
        this.onlineRev = await this.getRevisionNumber();
        this.offlineRev = await AsyncStorage.getItem("LOCAL_REVISION");

        /* CASE 1 - First time opening the app, or data was purged */
        if (this.isFirstOpen()) {
            log.debug(this.getName() + " first open");
            await this.download();
        }

        /* CASE 2 - New revision available */
        else if(this.hasUpdate()) {
            log.debug("updates available");
            log.debug("online Revision... " + this.onlineRev);
            log.debug("offline Revision... " + this.offlineRev);
            await this.download();
        }

        /* CASE 3 - No updates */
        else {
            const collection = await this.fetchFromLocal();
            this.setCollection(collection);
        }

        const status = this.collection == null ? "ERR" : "OK";
        log.info(this.getName() + " availability... [ "+ status +" ] ");
    }

    /** true if there is no local data **/
    isFirstOpen() {
        return this.offlineRev == -1 || this.offlineRev == null;
    }

    /** True if updates are available **/
    hasUpdate() {
        return this.onlineRev > this.offlineRev;
    }

    async fetchFromLocal() {
        const result = JSON.parse(await AsyncStorage.getItem(this.collectionID));
        if(result != null)
            this.setReady(true);
        return result;
    }

    async download() {
        log.debug("downloading from server: " + this.collectionID.toUpperCase());
        let version = await this.getRevisionNumber();
        await this.directus.api
            .request("get", this.collectionID  + this.params)
            .then(response => this.setCollection(response.data))
            .catch(error => { console.error("[ERR] Directus: " + error) })
            .finally(() => {
                AsyncStorage.setItem(this.collectionID, JSON.stringify(this.collection));
                this.setRevision(version);
                this.setReady(true);
            });
    }

    async getRevisionNumber() {
        try {
            return await this.directus.api.request("get", "/revisions", {single: 1, sort: "-id"})
                .then(request => { return request.data.id.toString() });
        } catch {
            log.warn("Unable to get revision. (is directus available?)");
            return -1;
        }
    }

    search(name, filter) {
        let results = [];
        if (this.collection == null) {
            return [];
        } else {
            this.collection.forEach((item) => {
                const query = name.toLowerCase();
                const itemName = item.name.toLowerCase();
                const published = item.status === "published";
                if(itemName.includes(query) && published)
                    results.push(item);
            });
        }
        return results;
    }

    setCollection(collection) {
        let result = [];
        if (collection == null) {
            log.debug("setting null collection");
        } else {
            collection.forEach((item) => {
                if(item.state != "draft") {
                    let instance = Object.create(this.template.prototype);
                    Object.assign(instance, item);
                    result.push(instance);
                }
            });
            this.collection = result;
        }
    }

    setRevision(revision) {
        this.localRevision = revision;
        AsyncStorage.setItem("LOCAL_REVISION", this.localRevision);
    }

    getCollection() {
        if (this.collection == null) {
            log.debug("ALERT: Collection returned is null");
            return [];
        } else if (!Array.isArray(this.collection)) {
            log.error("[NETWORK] collection is not an serializeArray");
            return [];
        } else {
            return this.collection
        }
    }

    searchByID(id) {
        let content = this.getCollection();
        let result = content.find((value) => value.id === id);
        if(result === null || result === undefined)
            result = [];
        return result;
    }

    searchByName(input : String) {
        input = input.toLowerCase();
        let content = this.getCollection();
        let result = content?.filter((value) => {
            return value.name.toLowerCase().includes(input);
        });

        if(input == null || input === "") {
            return content;
        } else if(result == null){
            log.info("Result is empty");
            return content;
        } else if(result.length > 0)  {
            log.info("Successful search: " + result.length + " entries found");
            return result;
        } else {
            log.info("Unsuccesful search: " + result.length + " entries found");
            return [];
        }
    }

    store() {
        AsyncStorage.setItem(this.collectionID, JSON.stringify(this.collection)).done();
    }

    getItem(id) {
        return this.collection[id];
    }

    setReady(ready) {
        this.ready = ready;
    }

    isReady() {
        return this.ready;
    }

    getName() {
        if(this.collectionID == null) {
            console.error("Collection is not initialized. No ID passed to constructor.");
        } else {
            return this.collectionID.split('/')[2];
        }
    }

    async isConnected() {
        const result = await this.directus.ping();
        return result === "pong";
    }

    async hasPermission() {
        const result = await this.directus.getMyPermissions();
        const data = result.data.find(value => value.collection == this.getName());
        return data.read === "full";
    }

}
export default Collection;
