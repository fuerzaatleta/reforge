import 'react-native-get-random-values';

export function generateUID() {
    return (Math.random() * 999999).toString();
}
