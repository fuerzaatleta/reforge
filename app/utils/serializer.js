


class Serializer {

    static serializeArray(array) {
        const serialized = [];
        array.forEach((value) => serialized.push( value.toJSON() ));
        return serialized;
    }

    static deserializeArray(array, className) {
        const deserialized = [];
        if(array == null) {
            console.info("Deserialization: parameter is null");
        } else {
            array.forEach((itemJSON) => {
                const item = new className;
                item.deserialize(itemJSON);
                deserialized.push(item);
            });
        }
        return deserialized;
    }

}
export default Serializer;


