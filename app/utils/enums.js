export const muscle = {
    SHOULDERS: 0,
    CHEST: 1,
    BACK:  2,
    TRICEPS: 3,
    BICEPS: 4,
    FOREARMS: 5,
    LEGS: 6,
    GLUTES: 7,
    CALVES: 8,
    ABS: 9,
    CARDIO: 10,
};
