import React from 'react';
import {StatusBar, Text, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {Root} from 'native-base';
import theme from './theme/theme';
import app from '../src/AppFramework';
import {useUserStore} from './mobx/user/user-store';
import LoginNavigation from './navigation/login-navigation';
import RootNavigation from './navigation/root-navigation';
import {observer} from 'mobx-react';
import {MenuProvider} from 'react-native-popup-menu';
import {useSystemStore} from './mobx/system/system-store';

const App = () => {
    const system = useSystemStore();
    let navigation = React.createRef();
    system.setNavigation(navigation);
    app.navigation = navigation; // DEPRECATED
    // StatusBar.setHidden(false);
    // StatusBar.setBackgroundColor("#ffffff");
    // StatusBar.setBarStyle("dark-content");
    return (
        <NavigationContainer ref={navigation}>
            <MenuProvider>
                <Root>
                    <StatusBar
                        animated={true}
                        backgroundColor="#fff"
                        barStyle={"dark-content"}
                        showHideTransition={true}
                        hidden={false} />
                    <Main/>
                </Root>
            </MenuProvider>
        </NavigationContainer>
    );
};

const Main = observer(() => {
    const user = useUserStore();
    if(user.isAuthenticated())
        return <RootNavigation/>;
    else
        return <LoginNavigation/>;
});

export default App;
