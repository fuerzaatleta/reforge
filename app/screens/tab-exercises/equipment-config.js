import React, {useState} from 'react';
import {Image, Text, View} from 'react-native';
import {Icon} from "native-base";
import TouchableRipple from 'react-native-paper/src/components/TouchableRipple/TouchableRipple.native';
import app from '../../../src/AppFramework';
import {Assets} from '../../assets/Assets';

const EquipmentConfig = () => {
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.iconBox}>
                    <TouchableRipple borderless onPress={() => app.navigateBack()} style={styles.icon}>
                        <Icon style={{fontSize: 27}} type="MaterialCommunityIcons" name="arrow-left"/>
                    </TouchableRipple>
                </View>
                <View style={styles.descriptions}>
                    <Text style={styles.title}>¿Cual es tu Equipamiento?</Text>
                    <Text style={styles.description}>Selecciona las herramientas que están a tu alcance.</Text>
                </View>
            </View>
            <View style={styles.content}>
                <Item source={Assets.EQUIPMENT_TRX}
                      title={"Banda Elástica"}
                      description={"Extremadamente práctica, su polivalencia permite trabajar todas las partes del cuerpo."}
                      enabled={false} />
                <Item source={Assets.EQUIPMENT_DUMBELL}
                      title={"Mancuernas"}
                      description={"Las mancuernas te permiten entrenar la fuerza explosiva manteniendo una ámplia libertad de movimientos."}
                      enabled={false} />
                <Item source={Assets.EQUIPMENT_KETTLEBELL}
                      title={"Kettlebell"}
                      description={"La kettlebell te permite entrenar la fuerza explosiva a través de ejercicios dinámicos."}
                      enabled={false} />
            </View>
        </View>
    )
};

const Item = (props) => {
    let [enabled, setEnabled] = useState(false);
    return (
        <TouchableRipple borderless style={styles.item} onPress={() => onClick()}>
            <View>
                <View style={styles.itemContainer}>
                    <Image source={props.source} style={styles.image} />
                    <View style={styles.innerBox}>
                        <Text style={styles.itemTitle}>{props.title}</Text>
                        <Text style={styles.itemSubtitle}>{props.description}</Text>
                    </View>
                </View>
                <Indicator enabled={enabled} />
            </View>
        </TouchableRipple>
    );

    function onClick() {
        setEnabled(!enabled);
    }
};

const Indicator = (props) => {
    const color = props.enabled ? "#49b425" : "#9b9b9b";
    const text = props. enabled ? "ACTIVADO" : "DESACTIVADO";
    return (
        <View style={[styles.indicator, {backgroundColor: color}]}>
            <Text style={styles.text}>{ text }</Text>
        </View>
    )
};

const styles = {
    text: {
        fontSize: 12,
        color: "white",
    },
    indicator: {
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
        minWidth: 105,
        padding: 5,
        position: "absolute",
        bottom: 7,
        right: 7,
        borderRadius: 8,
        backgroundColor: "#49b425",
    },
    itemContainer: {
        flexDirection: "row",
        padding: 15,
    },
    iconBox: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 0.2,
    },
    innerBox: {
        flex: 1,
        paddingLeft: 15,
        // textAlignVertical: "center",
        // textAlign: "center",
        // justifyContent: 'center',
    },
    itemTitle: {
        fontSize: 20,
    },
    itemSubtitle: {
        color: "#505050",
    },
    image: {
        borderRadius: 8,
        height: 105,
        width: 82,
        backgroundColor: "#bbbbbb",
    },
    item: {
        marginBottom: 12,
        borderColor: "#d6d6d6",
        // padding: 15,
        marginLeft: 15,
        marginRight: 15,
        borderRadius: 12,
        borderWidth: 1,
    },
    icon: {
        padding: 12,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15,
        borderRadius: 12,
    },
    header: {
        flexDirection: "row",
        paddingTop: 15,
        paddingBottom: 30,
    },
    descriptions: {
        // marginRight: 15,
        flex: 0.7,
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
    },
    title: {
        fontSize: 20,
    },
    description: {
        color: "#717171",
        fontSize: 17,
    },
};

export default EquipmentConfig;
