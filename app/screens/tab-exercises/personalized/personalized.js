import React from 'react';
import {View} from 'react-native';
import {FAB} from 'react-native-paper';
import {color} from '../../../theme/color';

const Personalized = () => {
    return (
        <View style={styles.container}>
            <FAB
                style={styles.fab}
                small
                icon="plus"
                onPress={() => console.log('Pressed')}
            />
        </View>
    )
};

const styles = {
    container: {
        flex: 1,
        backgroundColor: "#fff",
    },
    fab: {
        backgroundColor: color.primary,
        position: 'absolute',
        margin: 16,
        right: 25,
        bottom: 25,
    },
};

export default Personalized;
