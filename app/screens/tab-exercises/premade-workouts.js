import React from 'react';
import {Text, View, ImageBackground} from 'react-native';
import {TouchableRipple} from 'react-native-paper';
import app from '../../../src/AppFramework';
import {Assets} from '../../assets/Assets';
import Recommended from '../../components/specific/recommended';

const PremadeWorkouts = () => {
    return (
        <View style={styles.screen}>
            <View style={styles.categories}>
                <Cover
                    name="ANAERÓBICO"
                    image={Assets.CATEGORY_WORKOUT}
                    description="Para incrementar la fuerza y masa muscular."
                    onPress={() => app.navigateTo("AdvancedWorkouts")} />
                <Cover
                    name="AERÓBICO"
                    image={Assets.CATEGORY_PRACTICE}
                    description="Para quemar calorías y acelerar el metabolismo."
                    onPress={() => app.navigateTo("Workouts")} />
            </View>
            <View style={styles.content}>
                <Recommended/>
            </View>
        </View>
    )
};

const Cover = (props) => {
    return (
        <TouchableRipple borderless style={styles.touchable} onPress={props.onPress}>
            <ImageBackground
                source={props.image}
                imageStyle={styles.image}
                style={styles.view}>
                <Text style={styles.title}>{props.name}</Text>
                <Text style={styles.subtitle}>{props.description}</Text>
            </ImageBackground>
        </TouchableRipple>
    )
};

const styles = {
    content: {
        paddingRight: 7,
        paddingLeft: 7,
    },
    image: {
        opacity: 0.52,
    },
    view: {
        backgroundColor: "black",
        justifyContent: 'center',
        alignItems: 'center',
        height: "100%",
    },
    categories: {
        flexDirection: "row",
    },
    touchable: {
        backgroundColor: "black",
        flex: 0.5,
        height: 255,
        borderRadius: 8,
        margin: 4,
    },
    screen: {
        paddingTop: 3,
        flex: 1,
        backgroundColor: "#fff",
    },
    title: {
        fontSize: 17.5,
        fontWeight: "bold",
        color: "white",
    },
    subtitle: {
        paddingLeft: 5,
        paddingRight: 5,
        fontSize: 14,
        color: "white",
        textAlignVertical: "center",
        textAlign: "center",
    },
};

export default PremadeWorkouts;
