import React from 'react';
import {Text, View} from 'react-native';
import ExercisePreview from '../../../components/prototype/workout/exercise-preview';
import {useSystemStore} from '../../../mobx/system/system-store';
import {useContentStore} from '../../../mobx/content/content-store';
import {Exercise} from '../../../services/directus/exercise';
import {color} from '../../../theme/color';

const Resume = () => {
    const system = useSystemStore();
    const content = useContentStore();
    const id = system.getFlag("SELECTED_EXERCISE", 0);
    const exercise: Exercise = content.exercises.getItem(id);

    return (
        <View style={styles.root}>
            <ExercisePreview style={styles.img} name={""} video={exercise.getVideo()} />
            <View style={styles.separator}/>
            <View style={styles.container}>
                <Text style={styles.title}>Descripción</Text>
                <Text style={styles.description}>{exercise.getDescription()}</Text>
            </View>
            <View style={styles.separator}/>
            <View style={row.box}>
                <Text style={row.title}>Dificultad</Text>
                <Text style={row.equipment}>{exercise.getDifficulty()}</Text>
            </View>
            <View style={styles.separator}/>
            <View style={row.box}>
                <Text style={row.title}>Equipamiento</Text>
                <Text style={row.equipment}>{exercise.getEquipment()}</Text>
            </View>
            <View style={styles.separator}/>
            <View style={row.box}>
                <Text style={row.title}>Repeticiones Totales</Text>
                <Text>-</Text>
            </View>
            <View style={styles.separator}/>
        </View>
    )
};

const row = {
    title: {
        flex: 1,
        fontSize: 17,
        textAlignVertical: "center",
        justifyContent: 'center',
        alignItems: 'center',
    },
    box: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 12.5,
        paddingBottom: 12.5,
        flexDirection: "row",
        textAlignVertical: "center",
        textAlign: "center",
    },
    equipment: {
        fontSize: 13,
        color: "white",
        backgroundColor: "#6163cc",
        borderRadius: 8,
        padding: 4.5,
        paddingLeft: 12,
        paddingRight: 12,
    },
};

const styles = {
    separator: {
        borderBottomWidth: 1,
        borderColor: color.line,
    },
    root: {
        flex: 1,
        backgroundColor: "#fff",
    },
    container: {
        backgroundColor: "#fff",
        paddingLeft: 20,
        paddingRight: 20,
    },
    title: {
        paddingTop: 20,
        color: "#2d2d2d",
        fontSize: 21,
        paddingBottom: 4,
    },
    description: {
        paddingBottom: 24,
        color: "#373737",
        fontSize: 14.5,
    },
    img: {
        marginTop: 25,
        height: 270,
        width: 270,
    },
};

export default Resume;
