import React from 'react';
import {Text, View} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import Resume from './resume';
import Historic from './historic';
import Charts from './charts';
import {Icon} from "native-base";
import {useSystemStore} from '../../../mobx/system/system-store';
import {useContentStore} from '../../../mobx/content/content-store';
import {Exercise} from '../../../services/directus/exercise';
import TouchableRipple from 'react-native-paper/src/components/TouchableRipple/TouchableRipple';
import app from '../../../../src/AppFramework';
import {color} from '../../../theme/color';

const ExerciseViewer = () => {
    const system = useSystemStore();
    const content = useContentStore();
    const id = system.getFlag("SELECTED_EXERCISE", 0);
    const exercise: Exercise = content.exercises.getItem(id);
    const Tab = createMaterialTopTabNavigator();
    return (
        <View style={styles.container}>
            <View style={header.root}>
                <TouchableRipple rippleColor={"#545454"} borderless style={header.arrowBox} onPress={() => app.navigateBack()}>
                    <Icon style={header.arrow} type="MaterialCommunityIcons" name="arrow-left"/>
                </TouchableRipple>
                <Text style={header.title}>{exercise.getName()}</Text>
            </View>
            <Tab.Navigator
                style={styles.navigator}
                backBehavior="none"
                screenOptions={screen}
                tabBarOptions={options}>
                <Tab.Screen name="Resumen" component={Resume} />
                <Tab.Screen name="Historial" component={Resume} />
                <Tab.Screen name="Estadísticas" component={Charts} />
            </Tab.Navigator>
        </View>
    );
};

const screen = {
    tabBarOptions: {
        activeTintColor: '#fff',
        inactiveTintColor: '#fff',
        activeBackgroundColor: '#fff',
        inactiveBackgroundColor: '#fff',
        style: {
            backgroundColor: '#fff',
        },
    },
};

const options = {
    labelStyle: {
        // textTransform: "capitalize",
        // fontSize: 15,
    },
    indicatorStyle: {
        backgroundColor: "#ff1719",
        height: 2,
    },
};

const header = {
    root: {
        paddingTop: 4,
        paddingBottom: 4,
        flexDirection: "row",
        textAlignVertical: "center",
        textAlign: "center",
        alignItems: 'center',
    },
    title: {
        fontSize: 18,
        color: "#0b0b0b",
    },
    arrowBox: {
        marginLeft: 8,
        marginRight: 12,
        padding: 10,
        borderRadius: 20
    },
    arrow: {
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
        color: "#212121",
        fontSize: 23,
    },
};

const styles = {
    navigator: {

    },
    arrow: {
        borderRadius: 12,
        marginLeft: 12,
        marginRight: 12,
        padding: 9,
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
    },
};

export default ExerciseViewer;
