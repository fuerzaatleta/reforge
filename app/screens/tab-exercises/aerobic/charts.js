import React from 'react';
import {Text, View} from 'react-native';

const Charts = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Instrucciones</Text>
            <Text style={styles.description}>
                1. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis. {"\n\n"}

                2. Praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias. {"\n\n"}

                3. Cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi. {"\n\n"}
            </Text>
            <Text style={styles.title}>Consejos</Text>
            <Text style={styles.description}>
                - At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis. {"\n\n"}

                - Praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias. {"\n\n"}

                - Cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi. {"\n\n"}
            </Text>
        </View>
    )
};

const styles = {
    container: {
        flex: 1,
        backgroundColor: "#fff",
        paddingTop: 15,
        paddingLeft: 20,
        paddingRight: 20,
    },
    title: {
        color: "#2d2d2d",
        fontSize: 21,
        paddingBottom: 4,
    },
    description: {
        paddingTop: 10,
        color: "#494949",
        fontSize: 15.5,
        marginBottom: 20,
    },
};

export default Charts;
