import React, {useState} from 'react';
import {View, FlatList, ActivityIndicator} from 'react-native';
import {Body, Icon, ListItem, Thumbnail, Text} from 'native-base';
import {Searchbar, TouchableRipple} from 'react-native-paper';
import theme from '../../theme/theme';
import {useContentStore} from '../../mobx/content/content-store';
import {Assets} from '../../assets/Assets';
import {config} from '../../config';
import app from '../../../src/AppFramework';
import {useSystemStore} from '../../mobx/system/system-store';
import {observer} from 'mobx-react';

let system;
const TabExercises = observer(() => {
    system = useSystemStore();
    const content = useContentStore();
    let [input, setInput] = useState("");
    let loaded = content.exercises.isLoaded();

    return (
        <View style={styles.container}>
            <View style={styles.searchBox}>
                <Searchbar
                    style={styles.searchbar}
                    placeholder="Buscar un ejercicio"
                    onChangeText={(text) => setInput(text)}
                    value={input}/>
                <View style={styles.config}>
                    <TouchableRipple borderless style={styles.configButton} onPress={() => app.navigateTo("Filters")}>
                        <Icon style={{fontSize: 28, color: "#3b3b3b"}} type="FontAwesome" name="gear"/>
                    </TouchableRipple>
                </View>
            </View>
            { loaded ? <Result/> : <Loading/> }
        </View>
    );

    function Result() {
        let content = useContentStore();
        let collection = content.exercises.searchByName(input);
        if(config.EXERCISE_FILTER) {
            collection = collection.filter((value) => value.thumbnail != null);
        }
        return (
            <FlatList
                data={collection}
                renderItem={Item}
                removeClippedSubviews={true}
                keyExtractor={item => item.id.toString()} />
        )
    }
});

const Loading = () => {
    return (
        <View style={{flex: 1}}>
            <ActivityIndicator animating={true} style={styles.loading} size="large" color={"#e84225"} />
        </View>
    )
};

let image = Assets.PLACEHOLDER;
const Item = (props) => {
    if(props.item.thumbnail != null) {
        image = {uri: props.item.thumbnail.data.full_url};
    }
    return (
        <ListItem onPress={onClick} noIndent thumbnail style={styles.item}>
            <View style={exercise.left}>
                <Thumbnail square style={styles.img} source={image}/>
            </View>
            <Body style={exercise.body}>
                <Text style={styles.title}>{props.item.name}</Text>
            </Body>
        </ListItem>
    );

    function onClick() {
        app.navigateTo("ExerciseViewer");
        system.setFlag("SELECTED_EXERCISE", props.item.id);
    }
};


const styles = {
    configButton: {
        borderRadius: 12,
        padding: 7,
        marginRight: 12,
        marginLeft: -3,
    },
    config: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    loading: {
        position: "absolute",
        marginLeft: "auto",
        marginRight: "auto",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    searchBox: {
        flexDirection: "row",
        marginTop: 17,
        marginBottom: 12,
    },
    searchbar: {
        flex: 1,
        borderRadius: 30,
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 5,
    },
    img: {
        // flex: 0.5,
        // backgroundColor: "#b6b6b6",
        backgroundColor: "white",
        borderWidth: 1,
        borderColor: "#d5d5d5",
        borderRadius: 7,
        height: 62,
        width: 62,
        marginTop: 2.5,
        marginBottom: 2.5,
    },
    title: {
        fontSize: 18,
    },
    item: {
        paddingTop: 6,
        paddingBottom: 6,
        borderTopWidth: 1,
        backgroundColor: "white",
        borderColor: "rgb(220,224,229)",
    },
    container: {
        flex: 1,
        backgroundColor: "white",
    },
    header: {
        backgroundColor: "white",
        marginBottom: 6,
        elevation: 0,
    },
};

const exercise = {
    animatedView: {
        height: 10,
    },
    exerciseSelected: {
        backgroundColor: theme.SUCCESS_COLOR,
        padding: 17,
        flexDirection: "row",
    },
    left: {

    },
    body: {
        borderBottomWidth: 0,
    },
    right: {
        marginRight: 22,
        borderRadius: 2,
    },
};

export default TabExercises;
