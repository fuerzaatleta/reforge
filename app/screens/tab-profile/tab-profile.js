import React, {Component, useState} from 'react';
import {
    Container,
    Content,
    Text,
    Button,
    Thumbnail,
    Icon,
} from 'native-base';
import theme from '../../theme/theme';
import {Col, Grid, Row} from 'react-native-easy-grid';
import app from '../../../src/AppFramework';
import auth from '@react-native-firebase/auth';
import {StatusBar, TouchableNativeFeedback, View} from 'react-native';
import ProfileItem from '../tab-workout/gym/others/profile/widgets/ProfileItem';
import {useUserStore} from '../../mobx/user/user-store';
import {useSystemStore} from '../../mobx/system/system-store';

function UserAvatar() {
    return (
        <Grid style={style.grid}>
            <Col style={theme.center} size={1}>
                <Thumbnail
                    scaleX={1.25} scaleY={1.25}
                    source={ {uri: auth().currentUser.photoURL}}/>
            </Col>
            <Col size={2}>
                <Text style={theme.h2}>{auth().currentUser.displayName}</Text>
                <Text style={theme.h4}>{auth().currentUser.email}</Text>
            </Col>
            <Col style={theme.center} size={0.8}>
                <TouchableNativeFeedback
                    useForeground
                    style={style.touchable}
                    background={TouchableNativeFeedback.Ripple(theme.RIPPLE_COLOR, true)}
                    onPress={() => app.navigateTo("Configuration")}>
                    <View>
                        <Icon style={{color: "#878787", fontSize: 27}} type="FontAwesome" name="gear"/>
                    </View>
                </TouchableNativeFeedback>
            </Col>
        </Grid>
    );
}


function TabProfile(props) {
    const system = useSystemStore();
    const store = useUserStore();
    system.finishSplash(); // Esto es un HACK
    return (
        <Container style={{backgroundColor: theme.CONTAINER_COLOR}}>
            {/*<StatusBar barStyle="light-content" backgroundColor={"#1a1a1a"} />*/}
            <Content>
                <UserAvatar/>
                <View>
                    <ProfileItem icon="card-text-outline" name="Información Personal" screen="NoImplementado"/>
                    <ProfileItem icon="hammer-wrench" name="Equipamiento" screen="NoImplementado"/>
                    <ProfileItem icon="chat-processing" name="Configuración Social" screen="NoImplementado"/>
                    <ProfileItem icon="email" name="Experiencia de Usuario" screen="NoImplementado"/>
                    <ProfileItem icon="flask-empty" name="Laboratorio de Experimentos" screen="NoImplementado"/>
                    <ProfileItem icon="refresh" name="Sincronizar Información" screen="NoImplementado"/>
                    { __DEV__ ? <ProfileItem icon="code-not-equal-variant" name="Desarrolladores" screen="DevelopmentScreen"/> : <View/> }
                </View>
                {/*<Text style={style.header}>Sesión Iniciada como:</Text>*/}
                {/*<Text style={style.email}>{auth().currentUser.email}</Text>*/}
                {/*<Text style={style.build}>Reforge*/}
                {/*    Versión {DeviceInfo.getVersion()} ({DeviceInfo.getBuildNumber()})*/}
                {/*</Text>*/}
            </Content>
            <Button full style={style.logout} onPress={() => store.signout()}>
                <Text>Cerrar Sesión</Text>
            </Button>
        </Container>
    );
}

const style = {
    grid: {
        paddingTop: 30,
        paddingBottom: 27,
        borderBottomWidth: 1,
        borderColor: "#dddddd",
    },
    header: {
        marginTop: 15,
        color: theme.SECONDARY_COLOR,
        fontSize: 15,
        textAlign: 'center',
    },
    email: {
        color: theme.SECONDARY_COLOR,
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    build: {
        color: theme.SECONDARY_COLOR,
        fontSize: 15,
        fontWeight: '300',
        textAlign: 'center',
    },
    logout: {
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        marginTop: 25,
        backgroundColor: theme.PRIMARY_COLOR,
    },
};

export default TabProfile;
