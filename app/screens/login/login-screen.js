import React, { useState, useEffect } from "react";
import {
    StyleSheet,
    View,
    Text, StatusBar, default as Animated,
} from 'react-native';
import {useUserStore} from '../../mobx/user/user-store';
import {Icon, Input, Item} from 'native-base';
import {TouchableRipple} from 'react-native-paper';
import app from '../../../src/AppFramework';
import AsyncButton from '../../components/generic/async-button';
import {observer} from 'mobx-react';

let email, password = "";
const LoginScreen = observer((props) => {
    let [validation1, setValidation1] = useState(false);
    let [validation2, setValidation2] = useState(false);
    const user = useUserStore();
    return (
        <View style={styles.root}>
            <TouchableRipple style={styles.login} borderless onPress={() => app.navigateTo("Register")}>
                <Text style={styles.loginText}>Regístrate</Text>
            </TouchableRipple>
            <Text style={styles.title}>Iniciar Sesión</Text>
            <LoginInput
                validate={value => value.length > 0}
                onChangeText={value => email = value}
                onValidation={state => setValidation1(state)}
                placeholder="Correo Electrónico" />
            <LoginInput
                secure={true}
                validate={value => value.length > 0}
                onChangeText={value => password = value}
                onValidation={state => setValidation2(state)}
                placeholder="Contraseña" />
            <TouchableRipple
                borderless
                disabled={!isValidated()}
                style={isValidated() ? styles.submitOn : styles.submitOff}
                onPress={() => user.authwithCredentials(email, password)}>
                <Text style={isValidated() ? styles.submitTextOn : styles.submitTextOff}>Continuar</Text>
            </TouchableRipple>
            <Separator />
            <AsyncButton
                style={styles.google}
                loading={user.authenticatingWithGoogle}
                onPress={() => user.authWithGoogle() }>
                <View style={styles.socialInnerBox}>
                    <Icon style={styles.icon} type="FontAwesome" name="google"/>
                    <Text style={styles.googleTitle}>Continuar con Google</Text>
                </View>
            </AsyncButton>
            <TouchableRipple
                style={styles.facebook}
                loading={user.authenticatingWithFacebook}
                onPress={() => user.authWithFacebook() }>
                <View style={styles.socialInnerBox}>
                    <Icon style={styles.icon} type="FontAwesome" name="facebook"/>
                    <Text style={styles.facebookTitle}>Continuar con Facebook</Text>
                </View>
            </TouchableRipple>
            <Text style={styles.footer}>
                <Text style={styles.normal}>¿Has olvidado la contraseña? </Text>
                <Text style={styles.highlight} onPress={() => app.navigateTo("NoImplementado")}>Recuperar</Text>
            </Text>
        </View>
    );

    function isValidated() {
        return validation1 && validation2;
    }
});

const LoginInput = (props) => {
    return (
        <View style={styles.inputBox}>
            <Item style={{borderRadius: 8, borderColor: "#c5c5c5"}} regular>
                <Input
                    placeholderTextColor={"#9a9a9a"}
                    style={styles.input}
                    onChangeText={value => onChange(value)}
                    secureTextEntry={props.secure}
                    placeholder={props.placeholder} />
            </Item>
        </View>
    );

    function onChange(value) {
        let validated = props.validate(value);
        props.onValidation(validated);
        props.onChangeText(value);
    }
};

const Separator = () => {
    return (
        <View style={styles.separatorBox}>
            <View style={styles.separator} />
            <Text style={styles.separatorText}>O TAMBIÉN PUEDES</Text>
        </View>
    )
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        backgroundColor: "white",
        padding: 13,
        justifyContent: 'center',
    },
    submitOn: {
        marginTop: 12,
        marginBottom: 18,
        backgroundColor: "#ff3410",
        justifyContent: 'center',
        alignItems: 'center',
        padding: 12.5,
        borderRadius: 6,
    },
    submitOff: {
        marginTop: 12,
        marginBottom: 18,
        backgroundColor: "#c0c0c0",
        justifyContent: 'center',
        alignItems: 'center',
        padding: 12.5,
        borderRadius: 6,
    },
    submitTextOff: {
        fontSize: 16,
        color: "#666666",
    },
    submitTextOn: {
        fontSize: 16,
        color: "#ffffff",
    },
    highlight: {
        color: "#ff2323",
        textDecorationLine: "underline",
        fontSize: 15,
    },
    footer: {
        textAlignVertical: "center",
        textAlign: "center",
        marginTop: 20,
    },
    normal: {
        color: "#505050",
        fontSize: 15,
    },
    login: {
        borderRadius: 5,
        padding: 10,
        paddingLeft: 15,
        paddingRight: 15,
        position: "absolute",
        top: 10,
        right: 14,
    },
    loginText: {
        fontSize: 16.5,
        color: "#656565",
    },
    separatorBox: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
    },
    separator: {
        borderColor: "#c2c2c2",
        width: "100%",
        borderBottomWidth: 1,
    },
    separatorText: {
        // fontWeight: "bold",
        fontSize: 13,
        color: "#434343",
        backgroundColor: "white",
        marginTop: -11,
        paddingLeft: 15,
        paddingRight: 15,
    },
    title: {
        fontSize: 26,
        fontWeight: "bold",
        marginBottom: 6,
    },
    inputBox: {
        paddingTop: 7,
        paddingBottom: 7,
    },
    input: {
        paddingLeft: 14,
        fontSize: 15,
        height: 57,
    },
    inputPlaceholder: {
        color: "#fff",
    },
    submit: {
        marginTop: 12,
        marginBottom: 18,
        backgroundColor: "#c0c0c0",
        justifyContent: 'center',
        alignItems: 'center',
        padding: 12.5,
        borderRadius: 6,
    },
    submitText: {
        fontSize: 16,
        color: "#666666",
    },
    icon: {
        paddingLeft: 6,
        fontSize: 23,
        color: "white",
    },
    socialInnerBox: {
        flexDirection: "row",
    },
    google: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5,
        marginBottom: 8,
        borderRadius: 6,
        padding: 12,
        backgroundColor: "#ff3410",
    },
    facebook: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 5,
        backgroundColor: "#004c98",
        borderRadius: 6,
        padding: 12,
    },
    googleTitle: {
        flex: 1,
        textAlignVertical: "center",
        textAlign: "center",
        color: "white",
    },
    facebookTitle: {
        flex: 1,
        textAlignVertical: "center",
        textAlign: "center",
        color: "white",
    },
});
export default LoginScreen;
