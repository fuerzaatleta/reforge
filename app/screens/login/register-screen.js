import React, { useState, useEffect } from "react";
import {
    StyleSheet,
    View,
    Text, StatusBar,
} from 'react-native';
import {useUserStore} from '../../mobx/user/user-store';
import {useSystemStore} from '../../mobx/system/system-store';
import {Icon, Input, Item} from 'native-base';
import {TouchableRipple} from 'react-native-paper';
import app from '../../../src/AppFramework';
import AsyncButton from '../../components/generic/async-button';
import {observer} from 'mobx-react';

let name, lastname, email, password = "";
const RegisterScreen = observer(() => {
    const system = useSystemStore();
    const user = useUserStore();
    let [validation1, setValidation1] = useState(false);
    let [validation2, setValidation2] = useState(false);
    let [validation3, setValidation3] = useState(false);
    let [validation4, setValidation4] = useState(false);
    system.finishSplash(); // Esto es un HACK
    return (
        <View style={styles.root}>
            <TouchableRipple style={styles.login} borderless onPress={() => app.navigateTo("Login")}>
                <Text style={styles.loginText}>Iniciar Sesión</Text>
            </TouchableRipple>
            <Text style={styles.title}>Vamos a Empezar</Text>
            <LoginInput
                validate={value => value.length > 0}
                onChangeText={value => name = value}
                onValidation={state => setValidation1(state)}
                placeholder="Nombre" />
            <LoginInput
                validate={value => value.length > 0}
                onChangeText={value => lastname = value}
                onValidation={state => setValidation2(state)}
                placeholder="Apellido" />
            <LoginInput
                validate={value => value.length >= 3 && value.includes('@')}
                onChangeText={value => email = value}
                onValidation={state => setValidation3(state)}
                placeholder="Correo Electrónico" />
            <LoginInput
                secure={true}
                validate={value => value.length >= 8}
                onChangeText={value => password = value}
                onValidation={state => setValidation4(state)}
                placeholder="Contraseña (8+ carácteres)" />
            <TouchableRipple
                borderless
                disabled={!isValidated()}
                style={isValidated() ? styles.submitOn : styles.submitOff}
                onPress={() => user.createUser(name, lastname, email, password)}>
                <Text style={isValidated() ? styles.submitTextOn : styles.submitTextOff}>Continuar</Text>
            </TouchableRipple>
            <Separator />
            <AsyncButton
                loading={user.authenticatingWithGoogle}
                style={styles.google}
                onPress={() => user.authWithGoogle() }>
                <View style={styles.socialInnerBox}>
                    <Icon style={styles.icon} type="FontAwesome" name="google"/>
                    <Text style={styles.googleTitle}>Continuar con Google</Text>
                </View>
            </AsyncButton>
            <AsyncButton
                loading={user.authenticatingWithFacebook}
                style={styles.facebook}
                onPress={() => user.authWithFacebook() }>
                <View style={styles.socialInnerBox}>
                    <Icon style={styles.icon} type="FontAwesome" name="facebook"/>
                    <Text style={styles.facebookTitle}>Continuar con Facebook</Text>
                </View>
            </AsyncButton>
            <Text style={styles.footer}>
                <Text style={styles.normal}>Al continuar aceptas la </Text>
                <Text onPress={() => app.navigateTo("Privacy")} style={styles.highlight}>Política de Privacidad</Text>
                <Text style={styles.normal}> así como los </Text>
                <Text onPress={() => app.navigateTo("Terms")} style={styles.highlight}>Términos & Condiciones</Text>
                <Text style={styles.normal}> de reforge.</Text>
            </Text>
        </View>
    );

    function isValidated() {
        return validation1 && validation2 && validation3 && validation4;
    }
});

const LoginInput = (props) => {
    let [validated, setValidated] = useState();
    return (
        <View style={styles.inputBox}>
            <Item style={{borderRadius: 8, borderColor: "#c5c5c5"}} regular>
                <Input
                    placeholderTextColor={"#9a9a9a"}
                    style={styles.input}
                    onChangeText={value => onChange(value)}
                    secureTextEntry={props.secure}
                    placeholder={props.placeholder} />
                { validated ? <Icon style={styles.iconValidation} type="FontAwesome" name="check"/> : <View/>}
            </Item>
        </View>
    );

    function onChange(value) {
        let validated = props.validate(value);
        props.onValidation(validated);
        props.onChangeText(value);
        setValidated(validated);
    }
};

const Separator = () => {
    return (
        <View style={styles.separatorBox}>
            <View style={styles.separator} />
            <Text style={styles.separatorText}>O TAMBIÉN PUEDES</Text>
        </View>
    )
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        backgroundColor: "white",
        padding: 13,
        justifyContent: 'center',
    },
    highlight: {
        color: "#ff2323",
        textDecorationLine: "underline",
        fontSize: 13,
    },
    footer: {
        marginTop: 20,
    },
    normal: {
        color: "#505050",
        fontSize: 13,
    },
    login: {
        borderRadius: 5,
        padding: 10,
        paddingLeft: 15,
        paddingRight: 15,
        position: "absolute",
        top: 10,
        right: 14,
    },
    loginText: {
        fontSize: 16.5,
        color: "#656565",
    },
    separatorBox: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
    },
    separator: {
        borderColor: "#c2c2c2",
        width: "100%",
        borderBottomWidth: 1,
    },
    separatorText: {
        // fontWeight: "bold",
        fontSize: 13,
        color: "#434343",
        backgroundColor: "white",
        marginTop: -11,
        paddingLeft: 15,
        paddingRight: 15,
    },
    title: {
        fontSize: 26,
        fontWeight: "bold",
        marginBottom: 6,
    },
    inputBox: {
        paddingTop: 7,
        paddingBottom: 7,
    },
    input: {
        paddingLeft: 14,
        fontSize: 15,
        height: 57,
    },
    inputPlaceholder: {
        color: "#fff",
    },
    submitOn: {
        marginTop: 12,
        marginBottom: 18,
        backgroundColor: "#ff3410",
        justifyContent: 'center',
        alignItems: 'center',
        padding: 12.5,
        borderRadius: 6,
    },
    submitOff: {
        marginTop: 12,
        marginBottom: 18,
        backgroundColor: "#c0c0c0",
        justifyContent: 'center',
        alignItems: 'center',
        padding: 12.5,
        borderRadius: 6,
    },
    submitTextOff: {
        fontSize: 16,
        color: "#666666",
    },
    submitTextOn: {
        fontSize: 16,
        color: "#ffffff",
    },
    iconValidation: {
        color: "#3ec00d",
        fontSize: 20,
    },
    icon: {
        paddingLeft: 6,
        fontSize: 23,
        color: "white",
    },
    socialInnerBox: {
        flexDirection: "row",
    },
    google: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5,
        marginBottom: 8,
        borderRadius: 6,
        padding: 12,
        backgroundColor: "#ff3410",
    },
    facebook: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 5,
        backgroundColor: "#004c98",
        borderRadius: 6,
        padding: 12,
    },
    googleTitle: {
        flex: 1,
        textAlignVertical: "center",
        textAlign: "center",
        color: "white",
    },
    facebookTitle: {
        flex: 1,
        textAlignVertical: "center",
        textAlign: "center",
        color: "white",
    },
});
export default RegisterScreen;
