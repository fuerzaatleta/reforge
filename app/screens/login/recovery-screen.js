import React from 'react';
import {Text,View} from 'react-native';
import {Item, Input, Icon} from 'native-base';
import {TouchableRipple} from 'react-native-paper';
import {useUserStore} from '../../mobx/user/user-store';

const RecoveryScreen = () => {
    let email = "";
    let password = "";
    let user = useUserStore();
    return (
        <View style={styles.container}>
            <View style={styles.form}>
                <LoginInput
                    secure={false}
                    title={"Correo Electrónico"}
                    onChangeText={value => email = value}
                    placeholder="ejemplo@gmail.com" />
                <LoginInput
                    secure={true}
                    title={"Contraseña"}
                    onChangeText={value => password = value}
                    placeholder="+8 carácteres" />
                <TouchableRipple style={styles.submit} onPress={() => user.createUser(email, password)}>
                    <Text style={styles.submitText}>Registrarse</Text>
                </TouchableRipple>
            </View>
            <View style={styles.social}>
                <View style={styles.separator}/>
                <TouchableRipple style={styles.google} onPress={() => user.authWithGoogle() }>
                    <View style={styles.socialInnerBox}>
                        <Icon style={styles.icon} type="FontAwesome" name="google"/>
                        <Text style={styles.googleTitle}>Continuar con Google</Text>
                    </View>
                </TouchableRipple>
                <TouchableRipple style={styles.facebook} onPress={() => user.authWithFacebook() }>
                    <View style={styles.socialInnerBox}>
                        <Icon style={styles.icon} type="FontAwesome" name="facebook"/>
                        <Text style={styles.facebookTitle}>Continuar con Facebook</Text>
                    </View>
                </TouchableRipple>
            </View>
        </View>
    );

    function LoginInput (props) {
        return (
            <View style={styles.loginInput}>
                <Text style={styles.title}>{ props.title }</Text>
                <Item style={{borderRadius: 8}} regular>
                    <Input
                        style={styles.input}
                        onChangeText={value => props.onChangeText(value)}
                        secureTextEntry={props.secure}
                        placeholder={props.placeholder} />
                </Item>
            </View>
        )
    }
};

const styles = {
    input: {
        fontSize: 15,
        height: 45,
    },
    container: {
        justifyContent: 'center',
        flex: 1,
        padding: 15,
        backgroundColor: "#fff",
    },
    separator: {
        marginTop: 18,
        marginBottom: 18,
        borderColor: "#c1c1c1",
        borderBottomWidth: 1,
    },
    icon: {
        paddingLeft: 6,
        fontSize: 23,
        color: "white",
    },
    socialInnerBox: {
        flexDirection: "row",
    },
    google: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5,
        marginBottom: 8,
        borderRadius: 6,
        padding: 12,
        backgroundColor: "#ff3410",
    },
    facebook: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 5,
        backgroundColor: "#004c98",
        borderRadius: 6,
        padding: 12,
    },
    googleTitle: {
        flex: 1,
        textAlignVertical: "center",
        textAlign: "center",
        color: "white",
    },
    facebookTitle: {
        flex: 1,
        textAlignVertical: "center",
        textAlign: "center",
        color: "white",
    },
    loginInput: {
        paddingTop: 6,
        paddingBottom: 6,
    },
    form: {
        justifyContent: 'center',
    },
    submit: {
        marginTop: 15,
        marginBottom: 8,
        backgroundColor: "#ff3410",
        borderRadius: 5,
    },
    submitText: {
        padding: 14,
        textAlignVertical: "center",
        textAlign: "center",
        color: "white",
    },
    restoreTitle: {
        fontSize: 16.5,
    },
    restoreButton: {
        fontSize: 16.5,
        color: "#ff3410",
        paddingLeft: 10,
    },
    recovery: {
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: "row",
    },
    title: {
        fontSize: 17,
    },
};

export default RecoveryScreen;
