import React from 'react';
import {FlatList, View} from 'react-native';
import {useContentStore} from '../../../mobx/content/content-store';
import GridLibrary from '../../../components/specific/grid-library/grid-library';
import {Equipment} from '../../../mobx/content/workout-db';

const KettlebellWorkouts = () => {
    const content = useContentStore();
    const collection = content.workouts.getByEquipment(Equipment.KETTLEBELL);
    return (
        <View style={styles.container}>
            <FlatList
                numColumns={2}
                data={collection}
                initialNumToRender={6}
                showsVerticalScrollIndicator={false}
                renderItem={(props) => <GridLibrary {...props} />}
                keyExtractor={item => item.id.toString()}
            />
        </View>
    )
};

const styles = {
    container: {
        flex: 1,
        backgroundColor: "#fff",
    }
};

export default KettlebellWorkouts;
