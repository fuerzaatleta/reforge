import React from 'react';
import {View} from 'react-native';
import { Container, Header, Content, Tab, Tabs, Title, Body} from 'native-base';

const WorkoutFinder = () => {
    return (
        <View style={styles.container}>
            <Tabs tabBarUnderlineStyle={styles.underline}>
                <Tab
                    activeTextStyle={styles.activeTextStyle}
                    textStyle={styles.textStyle}
                    tabStyle={styles.tabStyle}
                    activeTabStyle={styles.activeTabStyle}
                    heading="Mis Rutinas">
                    {/*<MyWorkouts/>*/}
                </Tab>
                <Tab
                    tabBarUnderlineStyle={styles.underline}
                    activeTextStyle={styles.activeTextStyle}
                    textStyle={styles.textStyle}
                    tabStyle={styles.tabStyle}
                    activeTabStyle={styles.activeTabStyle}
                    heading="Buscar Rutina">
                    {/*<SearchWorkout/>*/}
                </Tab>
            </Tabs>
        </View>
    )
};

const styles = {
    underline: {
        height: 0,
    },
    activeTextStyle: {
        color: "black",
        fontSize: 18,
    },
    textStyle: {
        color: "black",
        fontSize: 18,
    },
    tabStyle: {
        backgroundColor: "white",
        color: "black",
    },
    activeTabStyle: {
        backgroundColor: "white",
        color: "black",
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
    }
};

export default WorkoutFinder;
