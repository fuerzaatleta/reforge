import React from 'react';
import {ScrollView, View} from 'react-native';
import {Fab, Icon} from 'native-base';
import {TouchableRipple} from 'react-native-paper';
import app from '../../../../../../src/AppFramework';

const FloatingButton = (props) => {
    return (
        <Fab
            style={styles.fab}
            position="topLeft">
            <TouchableRipple style={styles.fabTouchable} borderless onPress={props.onPress}>
                <Icon style={styles.icon} type="MaterialCommunityIcons" name={props.icon}/>
            </TouchableRipple>
        </Fab>
    )
};

const styles = {
    fabTouchable: {
        height: "100%",
        width: "100%",
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: 30,
    },
    fab: {
        backgroundColor: "white",
        position: "absolute",
        top: -3,
        left: -3,
        height: 53,
        width: 53,
        elevation: 2,
    },
};

export default FloatingButton;
