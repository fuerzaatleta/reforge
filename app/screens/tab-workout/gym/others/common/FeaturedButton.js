import React from 'react';
import {Text, View} from 'react-native';
import {Button} from 'react-native-paper';
import app from '../../../../../../src/AppFramework';
import {Container} from "native-base";

const FeaturedButton = (props) => {
    return (
        <View style={styles.container}>
            <Button contentStyle={styles.content} style={styles.button} color={"#fff"} onPress={props.onPress}>
                <Text style={{letterSpacing: 1.4}}>{props.title}</Text>
            </Button>
        </View>
    )
};

const styles = {
    container: {
        position: "absolute",
        bottom: 0,
        width: "100%",
        paddingLeft: 7,
        paddingRight: 7,
        paddingBottom: 7,
    },
    button: {
        backgroundColor: "#f42327",
        borderWidth: 1,
        borderRadius: 5,
    },
    content: {
        padding: 7,
    }
};

export default FeaturedButton;
