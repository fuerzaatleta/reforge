import React from 'react';
import {Text, View} from 'react-native';
import {Button, TouchableRipple} from 'react-native-paper';
import app from '../../../../../../src/AppFramework';
import {Container} from "native-base";

const Index = (props) => {
    return (
        <View style={[styles.container, props.style]}>
            <TouchableRipple style={styles.touchable} borderless onPress={props.onPress}>
                <View style={styles.button} color={"#fff"} onPress={props.onPress}>
                    {props.children}
                </View>
            </TouchableRipple>
        </View>
    )
};

const styles = {
    touchable: {
        borderRadius: 15,
    },
    container: {
        position: "absolute",
        bottom: 0,
        width: "100%",
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 7,
    },
    button: {
        backgroundColor: "#f42327",
        borderWidth: 0,
        borderRadius: 12,
        padding: 12,
        alignItems: "center",
    },
    title: {
        color: "white",
        fontSize: 19,
        fontWeight: "bold",
        marginBottom: -3,
    },
    subtitle: {
        color: "white",
        fontSize: 15,
    }
};

export default Index;
