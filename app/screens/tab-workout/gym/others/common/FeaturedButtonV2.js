import React from 'react';
import {Text, View} from 'react-native';
import {Button, TouchableRipple} from 'react-native-paper';
import {useSystemStore} from '../../../../../mobx/system/system-store';

const FeaturedButtonV2 = (props) => {
    const system = useSystemStore();
    return (
        <View style={styles.container}>
            <TouchableRipple borderless onPress={() => system.countdown.show(true)}>
                <View style={styles.button} color={"#fff"} onPress={props.onPress}>
                    <Text style={styles.title}>{props.title}</Text>
                    {props.subtitle == null ? null : <Text style={styles.subtitle}>{props.subtitle}</Text>}
                </View>
            </TouchableRipple>
        </View>
    )
};

const styles = {
    container: {
        position: "absolute",
        bottom: 10,
        width: "100%",
        paddingLeft: 12,
        paddingRight: 12,
        // paddingBottom: 7,
    },
    button: {
        backgroundColor: "#f42327",
        // borderWidth: 1,
        borderRadius: 8,
        padding: 15,
        alignItems: "center",
    },
    title: {
        color: "white",
        fontSize: 17,

        marginBottom: -3,
    },
    subtitle: {
        color: "white",
        fontSize: 15,
    }
};

export default FeaturedButtonV2;
