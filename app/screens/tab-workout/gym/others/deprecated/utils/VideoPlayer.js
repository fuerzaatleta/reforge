import Video from 'react-native-video';
import React, {useState} from 'react';
import {Container} from "native-base"
import app from '../../../../../../../src/AppFramework';

const VideoPlayer = () => {
    const [player, setPlayer] = useState();
    // this.player.presentFullscreenPlayer();
    return (
        <Container>
            <Video source={{uri: "uri: 'file://assets/video/example.mp4'"}}
                   ref={(ref) => { setPlayer(ref) }}
                   onBuffer={() => onBuffer}
                   onError={() => onVideoError}
                   style={styles.backgroundVideo} />
        </Container>
    );

    function onBuffer() {
        console.log("buffer")
    }

    function onVideoError() {
        console.log("error")
    }
};

const styles = {
    backgroundVideo: {
        backgroundColor: "black",
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
};

export default VideoPlayer
