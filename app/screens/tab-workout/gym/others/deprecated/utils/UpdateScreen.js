import React, {useState, useEffect} from 'react';
import {View, Text} from 'react-native';
import {Spinner} from 'native-base';
import * as Animatable from 'react-native-animatable';
import app from '../../../../../../../src/AppFramework';

const MINIMUM_TIME = 3;
const JUMP_TIME = 1.5;
let interval;
let timeout;

const text = [
    "Reclutando Entrenadores",
    "Convocando Espíritu de Lucha",
    "Endureciendo Fibras Musculares",
    "Desmaterializando Grasa ",
    "Indexando Carbohidratos",
    "Pre-Activando Metabolismo",
    "Ensamblando Proteínas",
    "Activando Modo \"Sin Excusas\"",
    "Refactorizando Hormonas",
    "Resterizando Abdominales",
    "Inicializando Actitud",
    "Construyendo Miofibras",
];

const UpdateScreen = () => {
    let [index, setIndex] = useState(0);
    useEffect(() => {
        interval = setInterval(Timer, 1500);
        return () => clearInterval(interval);
    });

    return (
        <View style={style.container}>
            <Spinner color='#fff' />
            <Text style={style.title}>Actualización en marcha...</Text>
            <Text style={style.subtitle}>{ text[index] }</Text>
        </View>
    );

    function Timer() {
        let random = Math.floor(Math.random() * text.length);
        if(random === index)
            random = Math.floor(Math.random() * text.length);
        setIndex(random);
    }
};

const Loading = () => {

};

const style = {
    container: {
        backgroundColor: "#d6381f",
        flex: 1,
        alignItems: "center",
        justifyContent: 'center',
    },
    title: {
        fontSize: 17,
        color: "white",
    },
    subtitle: {
        fontSize: 23,
        color: "white",
    }
};

export default UpdateScreen;
