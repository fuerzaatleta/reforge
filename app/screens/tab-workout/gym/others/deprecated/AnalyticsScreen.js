import React, {useEffect, useState} from 'react';
import {StatusBar, StyleSheet, Text, View, Image} from 'react-native';
import {Container, Content, Icon, Card, CardItem, Header, Left, Body, Title, List} from 'native-base';
import {useSelector} from 'react-redux';
import Video from 'react-native-video';
import {Assets} from '../../../../../assets/Assets';
import PersonalData from '../../../../../../src/components/analytics/PersonalData';
import UserStats from '../../../../../../src/components/analytics/UserStats';
import {Button, FAB} from 'react-native-paper';
import app from '../../../../../../src/AppFramework';
import ReforgeList from '../profile/widgets/ProfileItem';

function Index(props) {
    const state = useSelector(state => state.WorkoutReducer);
    const open = useState(false);
    return (
        <Content showsVerticalScrollIndicator={false} style={styles.container}>
            {/*<UserStats/>*/}

            {/*<Text style={styles.h1}>Características Físicas</Text>*/}
            {/*<PersonalData/>*/}

            {/*<Text style={styles.h1}>Optimizaciones</Text>*/}
            {/*<List>*/}
            {/*    <ReforgeList icon="flash-circle" name="Constitución Física" dispatch="ACCESS_DENIED"/>*/}
            {/*    <ReforgeList icon="flash-circle" name="Eficiencia de Entrenamiento" dispatch="ACCESS_DENIED"/>*/}
            {/*    <ReforgeList icon="flash-circle" name="Optimización de Horario " dispatch="ACCESS_DENIED"/>*/}
            {/*</List>*/}
        </Content>
    );
}

const styles = StyleSheet.create({
    container: {
        margin: 10,
        marginBottom: 0,
        backgroundColor: 'white',
    },
    h1: {
        fontSize: 18,
        color: "gray",
        marginBottom: 10,
        marginLeft: 7,
        marginTop: 20,
    },
});

export default Index;
