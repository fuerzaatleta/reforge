import React from 'react';
import {View, Text} from 'react-native';
import {Icon} from "native-base";
import {TouchableRipple} from 'react-native-paper';

const INNER_CIRCLE = 90;
const OUTER_CIRCLE = 32;

const CheckupPlay = () => {
    return (
        <View>
            <View style={styles.container}>
                <TouchableRipple style={styles.touchable} borderless onPress={() => console.log("")}>
                    <Icon style={styles.icon} type="FontAwesome5" name="play"/>
                </TouchableRipple>
            </View>
        </View>
    )
};

const styles = {
    touchable: {
        borderRadius: INNER_CIRCLE / 2,
    },
    icon: {
        color: "#4c556c",
        backgroundColor: "#f5f5f5",
        width: INNER_CIRCLE,
        height: INNER_CIRCLE,
        borderRadius: INNER_CIRCLE / 2,
        textAlignVertical: "center",
        textAlign: "center",
    },
    container: {
        backgroundColor: "rgba(255,255,255,0.1)",
        width: INNER_CIRCLE + OUTER_CIRCLE,
        height: INNER_CIRCLE + OUTER_CIRCLE,
        borderRadius: (INNER_CIRCLE + OUTER_CIRCLE) / 2,
        justifyContent: 'center',
        alignItems: 'center',
    }
};

export default CheckupPlay;
