import React from 'react';
import {View, Text} from 'react-native';
import {Thumbnail} from 'native-base';
import {Assets} from '../../../../../../assets/Assets';

const CheckupExercise = () => {
    return (
        <View style={styles.container}>
            <Thumbnail scaleX={1.2} scaleY={1.2} style={styles.image} large square source={Assets.PLACEHOLDER}/>
            <View style={styles.description}>
                <Text style={styles.title}>Curl de Biceps</Text>
                <Text style={styles.subtitle}>Última prueba el 09/07/2019</Text>
            </View>
        </View>
    )
};

const styles = {
    title: {
        color: "white",
        fontSize: 22,
    },
    subtitle: {
        color: "white",
        fontSize: 15,
    },
    description: {
        paddingLeft: 22,
        justifyContent: 'center',
        // alignItems: 'container',
    },
    image: {
        borderRadius: 10,
    },
    container: {
        flexDirection: "row",
        backgroundColor: "rgba(255,255,255,0.22)",
        width: "88%",
        height: 120,
        borderRadius: 12,
        padding: 19,
    }
};

export default CheckupExercise;
