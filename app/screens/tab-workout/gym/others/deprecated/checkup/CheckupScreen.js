import React from 'react';
import {View, StatusBar, Text} from 'react-native';
import CheckupPlay from './CheckupPlay';
import {Body, Container, Header, Icon, Left, Title, Content} from 'native-base';
import {Button} from 'react-native-paper';
import Theme from '../../../../../../theme/theme';
import app from '../../../../../../../src/AppFramework';
import CheckupExercise from './CheckupExercise';

const CheckupScreen = () => {
    return (
        <View style={styles.container}>
            {/*<StatusBar hidden />*/}
            <Header
                androidStatusBarColor={"white"}
                iosBarStyle="dark-content"
                style={{backgroundColor: "transparent", elevation: 0}}>
                <Left>
                    <Button color={"gray"} onPress={() => {app.navigateBack()}}>
                        <Icon style={{color: "white", fontSize: 27}} name='close' />
                    </Button>
                </Left>
                <Body>
                    <Title style={{color: "white"}}>Iniciar Prueba Física</Title>
                </Body>
            </Header>
            <Container style={styles.content}>
                <CheckupPlay/>
                <View style={styles.bottom}>
                    <Text style={styles.title}>Selecciona un Ejercicio</Text>
                    <CheckupExercise/>
                </View>
            </Container>
        </View>
    )
};

const styles = {
    title: {
        color: "white",
        fontSize: 22,
        fontWeight: "400",
        marginBottom: 10,
    },
    content: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#e53633",
    },
    container: {
        flex: 1,
        backgroundColor: "#e53633",
    },
    bottom: {
        position: "absolute",
        bottom: 20,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
};

export default CheckupScreen;
