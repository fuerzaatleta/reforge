import React, {useEffect} from 'react';
import {StatusBar, View, ActivityIndicator} from 'react-native';
import {CalendarList} from 'react-native-calendars';
import {LocaleConfig} from 'react-native-calendars';
import {Body, Header, Icon, Left, Title} from 'native-base';
import {Button} from 'react-native-paper';
import app from '../../../../../../../src/AppFramework';
import Theme from '../../../../../../theme/theme';

const color = "#ff4626";

LocaleConfig.locales['es'] = {
    monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
    monthNamesShort: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
    dayNames: ['Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo'],
    dayNamesShort: ['DOM','LUN','MAR','MIÉ','JUE','VIE','SAB'],
    today: 'Hoy'
};
LocaleConfig.defaultLocale = 'es';

const CalendarScreen = () => {
    return (
        <View style={styles.container}>
            <Header
                androidStatusBarColor={"#ab290c"}
                iosBarStyle="light-content"
                style={{backgroundColor: "#d63e33", borderBottomWidth: 1, borderBottomColor: "#e4e4e4"}}>
                <Left>
                    <Button color={"gray"} onPress={() => {app.navigateBack()}}>
                        <Icon style={{color: "#ffffff", fontSize: 27}} name='arrow-back' />
                    </Button>
                </Left>
                <Body>
                    <Title style={{color: "#fff"}}>Calendario</Title>
                </Body>
                <View>

                </View>
            </Header>
            <CalendarList
                // Callback which gets executed when visible months change in scroll view. Default = undefined
                // onVisibleMonthsChange={(months) => {console.log('now these months are visible', months);}}
                // Max amount of months allowed to scroll to the past. Default = 50
                pastScrollRange={50}
                // Max amount of months allowed to scroll to the future. Default = 50
                futureScrollRange={50}
                // Enable or disable scrolling of calendar list
                scrollEnabled={true}
                markedDates={{
                    '2020-06-08': {selected: true, selectedColor: color},
                    '2020-06-10': {selected: true, selectedColor: color},
                    '2020-06-12': {selected: true, selectedColor: color},
                    '2020-06-17': {selected: true, selectedColor: color},
                    '2020-06-15': {selected: true, selectedColor: color},
                    '2020-06-19': {selected: true, selectedColor: color},
                    '2020-06-22': {selected: true, selectedColor: color},
                    '2020-06-24': {selected: true, selectedColor: color},
                }}
                // showScrollIndicator={true}
                // renderEmptyData = {() => {return (<View />);}}
            />
        </View>
    );
};

const styles = {
    header: {
        backgroundColor: "#fff",
        borderBottomColor: "#d5d5d5",
        borderBottomWidth: 1,
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
    }
};

export default CalendarScreen;
