import React from 'react';
import {View, Text} from 'react-native';
import {Button} from 'react-native-paper';

const Depuration = () => {
    return (
        <View style={styles.container}>
            <Button color={"gray"} onPress={() => console.log("hit")}>
                <Text>Enviar Información al Servidor</Text>
            </Button>
        </View>
    )
};

const styles = {
    container: {
        flex: 1,
        backgroundColor: "#fff",
    }
};

export default Depuration;
