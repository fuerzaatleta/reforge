import {Container, Content, Card, CardItem, Text, Icon, Right, List, ListItem} from 'native-base';
import theme from '../../../../../../theme/theme';
import {Col, Grid, Row} from 'react-native-easy-grid';
import {View} from 'react-native';
import React, {Component, useState} from 'react';
import Switch from 'react-native-paper/src/components/Switch';
import DateTimePicker from '@react-native-community/datetimepicker';

class Notifications extends Component {
    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.card}>
                        <Grid>
                            <Col>
                                <Row style={theme.center}><Text>Frecuencia de Notificación</Text></Row>
                                <Row><Text>BAJA</Text></Row>
                            </Col>
                        </Grid>
                    </View>
                    <Grid style={styles.infoLabel}>
                        <Col><Icon type="Feather" name="info"/></Col>
                        <Col size={6}><Text style={theme.h3}>Todas las notificaciones están desactivadas</Text></Col>
                    </Grid>
                    <List>
                        <NotificationItem name="Lunes"/>
                        <NotificationItem name="Martes"/>
                        <NotificationItem name="Miércoles"/>
                        <NotificationItem name="Jueves"/>
                        <NotificationItem name="Viernes"/>
                        <NotificationItem name="Sábado"/>
                        <NotificationItem name="Domingo"/>
                    </List>
                </Content>
            </Container>
        );
    }
}

class NotificationItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: props.name,
            time: '10:00',
            checked: false,
            showTImePicker: false,
        };
    }

    showTimePicker() {
        this.setState({
            showTimePicker: true,
        });
    }

    TimePicker() {
        if (this.state.showTimePicker === true) {
            return (
                <DateTimePicker
                    testID="dateTimePicker"
                    timeZoneOffsetInMinutes={0}
                    value={new Date()}
                    mode={'time'}
                    is24Hour={true}
                    display="default"/>
            );
        }
    }

    render() {
        return (
            <ListItem noIndent onPress={() => this.showTimePicker()}>
                <Grid>
                    <Col>
                        <Row><Text>{this.state.name}</Text></Row>
                        <Row><Text>{this.state.time}</Text></Row>
                    </Col>
                </Grid>
                <Switch
                    value={this.state.checked}
                    onValueChange={(checked) => this.setState({checked})}/>
            </ListItem>
        );
    }
}

const styles = {
    container: {
        backgroundColor: '#FFFFFF',
        padding: 20,
    },

    card: {
        backgroundColor: '#ffffff',
        borderColor: '#dedede',
        borderWidth: 1,
        borderRadius: 20,
        padding: 15,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
    },
    infoLabel: {
        margin: 25,
    },
};

export default Notifications;
