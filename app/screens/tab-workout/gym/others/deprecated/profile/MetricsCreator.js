import React, {useState} from 'react';
import {ScrollView, Switch, Text, View} from 'react-native';
import {Body, Header, Icon, Input, Left, List, ListItem, Title} from 'native-base';
import {Button, TouchableRipple} from 'react-native-paper';
import app from '../../../../../../../src/AppFramework';
import Theme from '../../../../../../theme/theme';

const weightUnit = "kg";
const measurementUnit = "cm";

const MetricsCreator = () => {
    let [mode, setMode] = useState(false);
    return (
        <View style={{flex: 1}}>

            <ScrollView style={styles.container}>
                <View style={styles.switch}>
                    <Text style={styles.switchText}>Básico</Text>
                    <Switch
                        trackColor={{ false: "#d3d3d3", true: "#d3d3d3" }}
                        thumbColor={mode ? "#f4f3f4" : "#f4f3f4"}
                        value={mode}
                        onValueChange={(value) => setMode(value)}/>
                    <Text style={styles.switchText}>Avanzado</Text>
                </View>
                { mode ? <AdvancedMode/> : <BasicMode/> }
            </ScrollView>
            {/*<Button style={styles.btnSave} contentStyle={styles.btnSaveContent} mode="contained" onPress={() => submit()}>*/}
            {/*    Guardar*/}
            {/*</Button>*/}
        </View>
    )
};

const BasicMode = () => {
    return (
        <List>
            <ListItem style={styles.box} noIndent>
                <Text style={styles.title}>Sexo</Text>
                <Input placeholder="-" style={styles.input} />
            </ListItem>
            <ListItem style={styles.box} noIndent>
                <Text style={styles.title}>Edad</Text>
                <Input placeholder="-" style={styles.input} />
            </ListItem>
            <ListItem style={styles.box} noIndent>
                <Text style={styles.title}>Peso Corporal ({weightUnit})</Text>
                <Input placeholder="-" style={styles.input} />
            </ListItem>
            <ListItem style={styles.box} noIndent>
                <Text style={styles.title}>Altura ({measurementUnit})</Text>
                <Input placeholder="-" style={styles.input} />
            </ListItem>
            <ListItem style={styles.box} noIndent>
                <Text style={styles.title}>Cintura ({measurementUnit})</Text>
                <Input placeholder="-" style={styles.input} />
            </ListItem>
            <ListItem style={styles.box} noIndent>
                <Text style={styles.title}>Cuello ({measurementUnit})</Text>
                <Input placeholder="-" style={styles.input} />
            </ListItem>
            <ListItem style={styles.box} noIndent>
                <Text style={styles.title}>Cadera ({measurementUnit})</Text>
                <Input placeholder="-" style={styles.input} />
            </ListItem>
        </List>
    )
};

const AdvancedMode = () => {
    return (
        <View>
            <List>
                <ListItem style={styles.box} noIndent>
                    <Text style={styles.title}>Peso Corporal ({weightUnit})</Text>
                    <Input placeholder="-" style={styles.input} />
                </ListItem>
                <ListItem style={styles.box} noIndent>
                    <Text style={styles.title}>Hombro ({measurementUnit})</Text>
                    <Input placeholder="-" style={styles.input} />
                </ListItem>
                <ListItem style={styles.box} noIndent>
                    <Text style={styles.title}>Pecho ({measurementUnit})</Text>
                    <Input placeholder="-" style={styles.input} />
                </ListItem>
                <ListItem style={styles.box} noIndent>
                    <Text style={styles.title}>Brazo ({measurementUnit})</Text>
                    <Input placeholder="-" style={styles.input} />
                </ListItem>
                <ListItem style={styles.box} noIndent>
                    <Text style={styles.title}>Antebrazo ({measurementUnit})</Text>
                    <Input placeholder="-" style={styles.input} />
                </ListItem>
                <ListItem style={styles.box} noIndent>
                    <Text style={styles.title}>Muslo ({measurementUnit})</Text>
                    <Input placeholder="-" style={styles.input} />
                </ListItem>
                <ListItem style={styles.box} noIndent>
                    <Text style={styles.title}>Gemelo ({measurementUnit})</Text>
                    <Input placeholder="-" style={styles.input} />
                </ListItem>
                <ListItem style={styles.box} noIndent>
                    <Text style={styles.title}></Text>
                    <Input style={styles.input} />
                </ListItem>
            </List>
        </View>
    )
};

const styles = {
    switch: {
        flexDirection: "row",
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 7,
        borderColor: Theme.SEPARATOR_COLOR,
        marginTop: 20,
        marginBottom: 10,
    },
    switchText: {
        fontSize: 17,
        margin: 20,
    },
    box: {
        paddingTop: 5,
        paddingBottom: 5,
        // borderBottomColor: "#929292",
    },
    icon: {
        color: "white",
        width: 40,

    },
    btnSave: {
        borderRadius: 0,
        backgroundColor: "#18568d",
        // marginTop: 15,
    },
    btnSaveContent: {
        padding: 3,
    },
    title: {
        flex: 1,
        fontSize: 16,
        marginLeft: -14,
        // margin: 15,
    },
    input: {
        flex: 0
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
        paddingLeft: 17,
        paddingRight: 17,
    }
};

export default MetricsCreator;
