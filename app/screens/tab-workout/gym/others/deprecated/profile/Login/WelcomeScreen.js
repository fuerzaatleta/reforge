import React, { useState, useEffect } from 'react'
import {StyleSheet, Text, View, TouchableNativeFeedback, StatusBar, Image} from 'react-native';
import {Assets} from '../../../../../../../assets/Assets';
import Paginator from '../../../../../../../../src/components/widgets/Paginator';

const STATUSBAR_COLOR = "#d63110";
const STATUSBAR_CONTENT = "light";

const WelcomeScreen = () => {
    useEffect(() => {
        StatusBar.setBackgroundColor(STATUSBAR_COLOR);
        StatusBar.setBarStyle(STATUSBAR_CONTENT);
        return function() {
            StatusBar.setBackgroundColor("white");
            StatusBar.setBarStyle("dark-content");
        }
    });

    const pages = [];
    pages.push(<Page1/>);
    pages.push(<Page2/>);
    pages.push(<Page3/>);
    pages.push(<Page4/>);
    pages.push(<Page5/>);
    pages.push(<Page6/>);
    return <Paginator pages={pages} />;
};

const Page1 = () => {
    return (
        <View style={welcome.content}>
            <Image style={welcome.img} source={Assets.WELCOME_HELLO}/>
            <Text style={welcome.title}>¡Hola! Bienvenido a Reforge.</Text>
            <View style={welcome.btnContainer}>
                <Text style={welcome.txtContinue}>PULSA LA PANTALLA PARA CONTINUAR</Text>
            </View>
        </View>
    )
};

const Page2 = () => {
    return (
        <View style={welcome.content}>
            <Text style={welcome.title}>Nuestro algoritmo basado en inteligencia artificial aprende sobre tí y tus patrones de entrenamiento. </Text>
            <View style={welcome.btnContainer}>
                <Text style={welcome.txtContinue}>PULSA LA PANTALLA PARA CONTINUAR</Text>
            </View>
        </View>
    )
};

const Page3 = () => {
    return (
        <View style={welcome.content}>
            <Image style={welcome.img} source={Assets.WELCOME_ANALYTICS}/>
            <Text style={welcome.title}>Entenderás que ocurre en tu cuerpo cuando te ejercitas</Text>
            <View style={welcome.btnContainer}>
                <Text style={welcome.txtContinue}>PULSA LA PANTALLA PARA CONTINUAR</Text>
            </View>
        </View>
    )
};

const Page4 = () => {
    return (
        <View style={welcome.content}>
            <Text style={welcome.title}>Te proporcionamos la variables de entrenamiento óptimas para tu cuerpo</Text>
            <View style={welcome.btnContainer}>
                <Text style={welcome.txtContinue}>PULSA LA PANTALLA PARA CONTINUAR</Text>
            </View>
        </View>
    )
};

const Page5 = () => {
    return (
        <View style={welcome.content}>
            <Text style={welcome.title}>Mira este breve video si deseas entender mejor nuestro algoritmo</Text>
            <View style={{marginTop: 60}}/>
            <View style={{width: "100%"}}>
                <Image style={{width: "100%", height: 200, resizeMode: "center"}} source={Assets.WELCOME_VIDEO} onPress={() => console.log("asd")}/>
            </View>
            <View style={welcome.btnContainer}>
                <Text style={welcome.txtContinue}>PULSA LA PANTALLA PARA CONTINUAR</Text>
            </View>
        </View>
    )
};

const Page6 = () => {
    return (
        <View style={welcome.content}>
            <Text style={welcome.title}>Gracias por participar en esta prueba y ¡Bienvenido!</Text>
            <View style={welcome.btnContainer}>
                <Text style={welcome.txtContinue}>PULSA LA PANTALLA PARA CONTINUAR</Text>
            </View>
        </View>
    )
};


const PageMark = () => {
    return (
        <View style={pagemark.container}>
            <View style={pagemark.marker}/>
            <View style={pagemark.marker}/>
            <View style={pagemark.marker}/>
            <View style={pagemark.marker}/>
            <View style={pagemark.marker}/>
            <View style={pagemark.marker}/>
        </View>
    )
};

const pagemark = {
    container: {
        flexDirection: "row",
    },
    marker: {
        height: 7,
        flex: 1,
        backgroundColor: "rgba(255,255,255,0.25)",
        marginRight: 2,
        marginLeft: 2,
    },
};

const welcome = {
    img: {
        width: "75%",
        height: 315,
    },
    btnContainer: {
        position: "absolute",
        bottom: 30,
        width: "100%",
    },
    btn: {
        color: "#fff",
        marginLeft: 40,
        marginRight: 40,
    },
    btnContent: {

    },
    container: {
        flex: 1,
        backgroundColor: STATUSBAR_COLOR,
    },
    content: {
        flex: 1,
        justifyContent: 'center',
        alignContent: "center",
        alignItems: "center",
    },
    title: {
        fontSize: 27,
        color: 'white',
        textAlign: 'center',
        marginBottom: 20,
        marginTop: 20,
        marginLeft: 25,
        marginRight: 25,
    },
    txtContinue: {
        color: "white",
        fontSize: 14,
        textAlign: "center",
        letterSpacing: 1.5,
    },
};

const styles = StyleSheet.create({
    controls: {
        marginTop: 10,
        flexDirection: "row",
    },
    close: {
        flex: 1,
        fontSize: 34,
        color: "white",
        marginLeft: 30,
        marginRight: 30,
    },
    skip: {
        fontSize: 15,
        color: "white",
        marginLeft: 30,
        marginRight: 30,
        textAlign: "center",
        justifyContent: "center",
        letterSpacing: 1.5,
    },
    container: {
        // textAlignVertical: "container",
        // textAlign: "container",
        // justifyContent: 'container',
        // alignItems: 'container',
    },
    subtitle: {
        color: 'white',
        fontSize: 16,
        textAlign: "center",
        // backgroundColor: "gray",
        margin: 5,
        marginBottom: 25,
    },
    card: {
        marginTop: 14,
        marginLeft: 10,
        marginRight: 10,
    },
    text: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 18,
    }
});

export default WelcomeScreen;
