import React, {useState} from 'react';
import { Text } from 'react-native';
import {
    Menu,
    MenuProvider,
    MenuOptions,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';

const Sandbox = () => {
    let [selection, setSelected] = useState("Default");

    return (
        <MenuProvider style={{flexDirection: 'column', padding: 30}}>
            <Menu onSelect={value => setSelected(value)}>
                <MenuTrigger text={selection} />
                <MenuOptions style={style.menu}>
                    <MenuOption value="" text='Fallo Muscular' />
                    <MenuOption value="Two">
                        <Text>Two</Text>
                    </MenuOption>
                    <MenuOption value={3} text='Three' />
                </MenuOptions>
            </Menu>
        </MenuProvider>
    )
};

const style = {
    menu: {
        margin: 10,
    },
};

export default Sandbox;
