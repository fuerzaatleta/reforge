import React, {Component} from 'react';
import {Image, Text} from 'react-native';
import {Container, Content, Header, Fab, Icon, Button, List, ListItem, Left, Right, Thumbnail, Body } from 'native-base';
import analytics from '@react-native-firebase/analytics';
import ImageBox from '../../../../../../../src/components/widgets/ImageBox';
import app from '../../../../../../../src/AppFramework';
import {Assets} from '../../../../../../assets/Assets';

class Premium extends Component {

    constructor() {
        super();
        analytics().logEvent('suscription_view');
    }

    render() {
        return (
            <Container>
                <Content>
                    <Fab
                        direction="bottom"
                        style={styles.closeButton}
                        position="topLeft"
                        onPress={() => console.log('test')}>
                        <Icon name="close" style={styles.closeIcon}/>
                    </Fab>
                    <Image style={styles.featuredImage} source={Assets.PLACEHOLDER_500}/>
                    <Text style={styles.title}> Beneficios Premium</Text>
                    <ImageBox
                        image={Assets.PREMIUM_FEATURE1}
                        title="Analíticas Avanzadas"
                        content="Desbloquea métricas para averiguar cómo está respondiendo tu cuerpo." />
                    <ImageBox
                        image={Assets.PREMIUM_FEATURE2}
                        title="Librería de Reforge"
                        content="Desbloquea métricas para averiguar cómo está respondiendo tu cuerpo." />
                    <ImageBox
                        image={Assets.PREMIUM_FEATURE3}
                        title="Analíticas Avanzadas"
                        content="Desbloquea métricas para averiguar cómo está respondiendo tu cuerpo." />
                </Content>
                <Button block style={styles.trialButton} onPress={() => {app.store.dispatch(startTrial())}}>
                    <Text style={styles.trialText}>EMPEZAR PRUEBA GRATUITA</Text>
                </Button>
            </Container>
        );
    }
}

const styles = {
    featuredImage: {
        width: '100%',
    },
    title: {
        fontSize: 22,
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 20,
        textAlign: 'center',
    },
    closeIcon: {
        color: 'black',
    },
    closeButton: {
        backgroundColor: '#ffffff',
    },
    trialButton: {
        margin: 25,
        color: '#fff',
        borderRadius: 24,
        height: 50,
        backgroundColor: '#56af6e',
    },
    trialText: {
        color: '#fff',
        fontSize: 16,
        letterSpacing: 0.8,
        // fontWeight: "bold",
    },
};

export default Premium;
