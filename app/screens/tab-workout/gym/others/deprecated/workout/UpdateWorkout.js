import React, {useState} from 'react';
import {
    Content,
    Item, Text, View, Form, Textarea, List, ListItem, Container, Header, Left, Icon, Body, Title, Right,
} from 'native-base';
import {Switch, TextInput, Button} from 'react-native-paper';
import theme from '../../../../../../theme/theme';
import app from '../../../../../../../src/AppFramework';
import {useSelector} from 'react-redux';

const UpdateWorkout = () => {
    let tracker = useSelector(state => state.WorkoutReducer.tracker);
    let currentWorkout = tracker.currentWorkout;
    let workouts = tracker.workouts;
    let [icon, seIcon] = useState();
    let [name, setName] = useState();
    return (
        <Container>
            <Header
                androidStatusBarColor={"white"}
                iosBarStyle="dark-content"
                style={{backgroundColor: "white"}}>
                <Left>
                    <Button color={"gray"} onPress={() => {app.navigateBack()}}>
                        <Icon style={{color: "#656565", fontSize: 27}} name='arrow-back' />
                    </Button>
                </Left>
                <Body>
                    <Title style={{color: "black"}}>Actualizar Rutina</Title>
                </Body>
            </Header>
            <Content style={theme.container}>
                <TextInput
                    style={styles.input}
                    label='Nombre'
                    mode='outlined'
                    value={name}
                    onChangeText={name => setName(name)}/>
                <Text style={styles.text}>Selecciona un Icono</Text>
                <IconSelector parent={this}/>
                <Button style={styles.btnUpdate} mode="contained" onPress={() => btnUpdate()}>
                    Actualizar Rutina
                </Button>
                <Button style={styles.btnDelete} mode="contained" onPress={() => btnDelete()}>
                    Eliminar
                </Button>
            </Content>
        </Container>
    );

    function btnUpdate() {
        app.navigateBack();
    }

    function btnDelete() {
        tracker.remove(currentWorkout);
        app.navigateBack();
    }
};

const styles = {
    header: {

    },
    input: {
        marginBottom: 20,
    },
    text: {
        fontSize: 21
    },
    btnUpdate: {
        marginTop: 8,
        backgroundColor: theme.SUCCESS_COLOR,
    },
    btnDelete: {
        marginTop: 8,
        backgroundColor: theme.DANGER_COLOR,
    },
};

export default UpdateWorkout;
