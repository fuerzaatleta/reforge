
import {View, Text} from 'react-native';
import Popup from '../../../../../../../src/framework/widgets/Popup';
import {Icon} from "native-base";
import React, {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';
import Modal from 'react-native-modal';
import app from '../../../../../../../src/AppFramework';

let showing = false;
const InfoModal = (props) => {
    const event = useSelector(state => state.Nexus);
    let [visible, setVisible] = useState(false);

    if(event.event.name === props.name && !showing) {
        showing = true;
        setVisible(true);
        setTimeout(() => {
            app.dispatch("MODAL_OUT");
            console.log(event.event.name);
            setVisible(false);
            showing = false;
        }, 4000);
    }

    return (
        <Modal animationIn="slideInDown" animationInTiming={1000}
               animationOut="slideOutUp" animationOutTiming={3000}
               backdropOpacity={0.7} isVisible={visible} hasBackdrop={false}
               style={styles.modal}>
            <View pointerEvents='none' style={styles.container}>
                <View style={{flex: 0.22}}>
                    <Icon style={styles.icon}
                          name='check-circle'
                          type="MaterialCommunityIcons" />
                </View>
                <View style={{flex: 1}}>
                    <Text style={styles.text}>¡Enhorabuena! Has desbloqueado un nuevo talento.</Text>
                </View>
            </View>
        </Modal>
    )
};

const styles = {
    modal: {
        margin: 12

    },
    container: {
        borderWidth: 1,
        borderColor: "#e2e2e2",
        marginTop: -560,
        flexDirection: "row",
        height: 80,
        backgroundColor: "rgb(255,255,255)",
        borderRadius: 6,
    },
    icon: {
        fontSize: 32,
        color: "#3baa24",
        height: "100%",
        textAlignVertical: "center",
        textAlign: "center",
    },
    text: {
        fontSize: 17,
        textAlignVertical: "center",
        height: "100%",
        // marginLeft: 5,
    },
};

export default InfoModal;
