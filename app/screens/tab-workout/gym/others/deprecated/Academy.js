import React, {Component, useState} from 'react';
import {StyleSheet, View, TouchableNativeFeedback, FlatList} from 'react-native';
import {Container, Content, Thumbnail, Text, Header, Left, Icon, Body, Title, Right} from 'native-base';
import {Assets} from '../../../../../assets/Assets';
import theme from '../../../../../theme/theme';
import CategoryButton from '../../../../../../src/components/widgets/HorizontalButton';
import {Button, Searchbar} from 'react-native-paper';
import app from '../../../../../../src/AppFramework';

const Academy = (props) => {
    let [input, setInput] = useState("");
    return (
        <Container>
            <View style={styles.searchBox}>
                <Searchbar
                    style={styles.searchbar}
                    placeholder="Buscar una rutina"
                    onChangeText={(text) => setInput(text)}
                    value={input}/>
            </View>
            <FlatList
                data={app.exercises.search(input)}
                renderItem={({ item, index }) => <Item key={index} data={item} parent={this}/> }
                keyExtractor={item => item.id.toString()}/>
        </Container>
    );
};

const Item = () => {
    let score = Math.floor(Math.random() * 80 + 20);
    return (
        <TouchableNativeFeedback onPress={() => console.log("routine")}>
            <View style={styles.item}>
                <View style={{flex: 1}}>
                    <Text style={styles.title}>Driade del Bosque</Text>
                    <Text style={styles.subtitle}>Piernas, Gluteos</Text>
                    <Text style={styles.subtitle}>Dificultad - - -</Text>
                    <Text style={styles.subtitle}>Duración - - -</Text>
                </View>
                <View style={styles.scoreBox}>
                    <RenderScore/>
                    <Text style={{fontSize: 12}}>PUNTUACIÓN</Text>
                </View>
            </View>
        </TouchableNativeFeedback>
    );

    function RenderScore() {
        let color = score >= 70 ? "#408c29" : "#1e1d1d";
        return (
            <View style={{flexDirection: "row"}}>
                <Text style={{color: color}}>{score}</Text>
                <Text>/100</Text>
            </View>
        )
    }
};

const styles = StyleSheet.create({
    title: {
        fontSize: 18,
        fontWeight: "bold",
    },
    subtitle: {
        color: "#7e7e7e",
    },
    item: {
        padding: 20,
        borderTopWidth: 1,
        borderColor: theme.SEPARATOR_COLOR,
        flexDirection: "row",
        alignItems: 'center',
    },
    scoreBox: {
        borderWidth: 1,
        borderColor: theme.SEPARATOR_COLOR,
        padding: 11,
        borderRadius: 12,
        height: 58,
        alignItems: 'center',
        justifyContent: 'center',
    },

    searchBox: {
        marginTop: 15,
        marginBottom: 15,
    },
    searchbar: {
        borderRadius: 30,
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 5,
    },
});

export default Academy;
