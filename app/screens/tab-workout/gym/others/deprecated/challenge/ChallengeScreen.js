import React from 'react';
import {View, Image, Text, ScrollView} from 'react-native';
import ChallengeWeek from './ChallengeWeek';
import {Assets} from '../../../../../../assets/Assets';
import {TouchableRipple} from 'react-native-paper';
import {Container, Fab, Icon} from 'native-base';
import app from '../../../../../../../src/AppFramework';

const ChallengeScreen = () => {
    return (
        <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollview}>
            {/*<FAB*/}
            {/*    large*/}
            {/*    style={styles.fab}*/}
            {/*    icon="chevron-left"*/}
            {/*    onPress={() => app.navigateBack()}/>*/}

                {/*<Fab*/}
                {/*    style={styles.fab}*/}
                {/*    position="topLeft">*/}
                {/*        <Icon style={styles.icon} type="MaterialCommunityIcons" name="chevron-left"/>*/}
                {/*</Fab>*/}
            <View style={{margin: 8}}>
                <Image style={styles.img} source={Assets.RETO_DE_TITANES}/>
            </View>
            <View style={styles.container}>
                <Text style={styles.title}>Reto de Titanes</Text>
                <Text style={styles.subtitle}>Programa de 30 días</Text>
                <Text style={styles.description}>
                    Programa orientado al máximo aumento de masa
                    muscular desde casa con ejercicios basados en
                    mancuernas.
                </Text>
                <ChallengeWeek title="Semana 1"/>
                <ChallengeWeek title="Semana 2"/>
                <ChallengeWeek title="Semana 3"/>
                <ChallengeWeek title="Semana 4"/>
                <View style={{marginBottom: 15}}/>
                <HomeTool title="Prueba Física Final"/>
                <View style={{marginBottom: 25}}/>
            </View>
            <Fab
                style={styles.fab}
                position="topLeft">
                <TouchableRipple style={styles.fabTouchable} borderless onPress={() => app.navigateBack()}>
                    <Icon style={styles.icon} type="MaterialCommunityIcons" name="chevron-left"/>
                </TouchableRipple>
            </Fab>
        </ScrollView>
    )
};

const styles = {
    img: {
        width: "100%",
        height: 250,
        borderRadius: 12,
        marginBottom: 9,

    },
    title: {
        fontSize: 25,
        fontWeight: "bold",
    },
    subtitle: {
        fontSize: 16,
        marginBottom: 18,
        color: "#5c5c5c",
    },
    description: {
        fontSize: 17,
        color: "#3a3a3a",
        marginBottom: 10,
    },
    scrollview: {
        flex: 1,
        backgroundColor: "#fff",
    },
    container: {
        marginLeft: 23,
        marginRight: 23,
    },
    fab: {
        backgroundColor: "white",
        position: "absolute",
        top: -2,
        left: -2,
        height: 57,
        width: 57,
    },
    fabTouchable: {
        height: "100%",
        width: "100%",
        alignItems: "center",
        justifyContent: 'center'
    },
    icon: {
        fontSize: 35,
        color: "black",
    },
};

export default ChallengeScreen;
