import React from 'react';
import {View, Text, FlatList} from 'react-native';
import {Icon} from 'native-base';
import Theme from '../../../../../../theme/theme';

const DATA = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'First Item',
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Second Item',
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Third Item',
    },
];

const ChallengeWeek = (props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>{ props.title }</Text>
            <View style={styles.flatlist}>
                <Item/>
                <Item/>
                <Item/>
                <Item/>
                <Item/>
                <Item/>
                <Item/>
            </View>
        </View>
    )
};

const Item = (props) => {
    return (
        <View style={{flex: 1}}>
            <View style={styles.itemContainer}>
                <Icon style={styles.icon} type="MaterialCommunityIcons" name="play"/>
            </View>
        </View>

    )
};

const styles = {
    itemContainer: {
        height: 37,
        width: 37,
        borderWidth: 2,
        borderRadius: 19,
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: "#d9d2d2",
    },
    icon: {
        fontSize: 24,
        color: "#d9d2d2",

    },
    title: {
        fontSize: 18,
        paddingBottom: 8,
    },
    flatlist: {
        flexDirection: "row",
    },
    container: {
        borderWidth: 1,
        borderColor: Theme.SEPARATOR_COLOR,
        borderRadius: 12,
        backgroundColor: "#fff",
        marginTop: 15,
        flex: 1,
        paddingTop: 10,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 20,
    }
};

export default ChallengeWeek;
