import React, {useState} from 'react';
import {View, Text, TextInput} from 'react-native';
import {TouchableRipple} from 'react-native-paper';
import {Icon} from "native-base";
import { Item, Label, Picker, CheckBox} from 'native-base';
import {useWorkoutStore} from '../../../../../../mobx/workout/deprecated-workout-store';
import Theme from '../../../../../../theme/theme';
import app from '../../../../../../../src/AppFramework';

const AddWorkout = () => {
    const store = useWorkoutStore();
    let [workout, setWorkout] = useState("");
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={{flex: 0.25}}>
                    <TouchableRipple borderless style={styles.backTouchable} onPress={() => app.navigateBack()}>
                        <Icon style={{color: "#656565", fontSize: 27}} name='arrow-back' />
                    </TouchableRipple>
                </View>
                <View style={[{flex: 1}, Theme.center]}>
                    <Text style={styles.headerTitle}>Crear nueva rutina</Text>
                </View>
                <View style={{flex: 0.25}}>
                    <TouchableRipple borderless style={styles.backTouchable} onPress={() => onSubmit()}>
                        <Icon style={{color: "#656565", fontSize: 23}} name='save' />
                    </TouchableRipple>
                </View>
            </View>
            <View style={styles.body}>
                <RowItem name="Lunes" />
                <View style={styles.separator}/>
                <RowItem name="Martes"/>
                <View style={styles.separator}/>
                <RowItem name="Miércoles"/>
                <View style={styles.separator}/>
                <RowItem name="Jueves"/>
                <View style={styles.separator}/>
                <RowItem name="Viernes"/>
                <View style={styles.separator}/>
                <RowItem name="Sábado"/>
                <View style={styles.separator}/>
                <RowItem name="Domingo"/>
            </View>
        </View>
    );

    function onSubmit() {

    }
};

const RowItem = (props) => {
    return (
        <View style={styles.row}>
            <View style={{flex: 1}}>
                <Text style={styles.title}>{props.name}</Text>
            </View>
            <View style={{flex: 1}}>

            </View>
        </View>
    )
};

const styles = {
    body: {
        padding: 25,
        paddingTop: 5,
    },
    picker: {
        height: 20,
    },
    input: {
        backgroundColor: "white",
        padding: 0,
    },
    title: {
        fontSize: 15,
        textAlignVertical: "center",
        justifyContent: 'center',
        alignItems: 'center',
    },

    box: {
        // padding: 23,
        paddingBottom: 16,
        paddingTop: 16,
    },
    row: {
        paddingBottom: 26,
        paddingTop: 26,
        flexDirection: "row",
    },
    separator: {
        borderBottomWidth: 1,
        borderColor: "#d5d5d5",
    },
    save: {
        padding: 12,
        flex: 1,
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
    },
    backTouchable: {
        width: 52,
        height: 52,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        marginRight: 7,
    },
    back: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    header: {
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 15,
        // borderBottomWidth: 1,
        borderColor: Theme.SEPARATOR_COLOR,
        flexDirection: "row",
    },
    content: {
        // flex: 1,
    },
    headerTitle: {
        fontSize: 20,
    },
    headerSubtitle: {
        fontSize: 17,
        color: "#636363",
    },
    container: {
        // padding: 25,
        flex: 1,
        backgroundColor: "#fff",
    }
};

export default AddWorkout;
