import React from 'react';
import {View, Text, ScrollView} from 'react-native';
import {Icon, Thumbnail} from "native-base";
import PurchaseWidget from '../widgets/PurchaseWidget';
import {Assets} from '../../../../../../assets/Assets';
import FloatingButton from '../../common/FloatingButton';
import FeaturedButton from '../../common/FeaturedButton';
import app from '../../../../../../../src/AppFramework';

const PremiumScreen = () => {
    return (
        <View style={{height: "100%"}}>
            <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
                <Thumbnail source={Assets.PREMIUM_SCREEN} style={styles.image} square />
                <View style={styles.features}>
                    <Text style={styles.title}>Prueba Reforge Plus Gratis</Text>
                    <View style={styles.row}>
                        <Icon style={{color: "#4ad447", fontSize: 27}} type='FontAwesome' name='check' />
                        <Text style={styles.listitem}>Accede a la experiencia completa de Reforge.</Text>
                    </View>
                    <View style={styles.row}>
                        <Icon style={{color: "#4ad447", fontSize: 27}} type='FontAwesome' name='check' />
                        <Text style={styles.listitem}>Desbloquea la biblioteca con enriquecedoras lecciones en video.</Text>
                    </View>
                    <View style={styles.row}>
                        <Icon style={{color: "#4ad447", fontSize: 27}} type='FontAwesome' name='check' />
                        <Text style={styles.listitem}>Desbloquea los entrenamientos de fuerza explosiva con mancuernas y kettlebell.</Text>
                    </View>
                </View>
                <View>
                    <PurchaseWidget selected={true} name={"Plan Anual"} price={"4,45€"} description={"Prueba gratuita de 14 días, después 53,40€ al año."}/>
                    <PurchaseWidget selected={false} name={"Plan Mensual"} price={"12,99€"} description={"Prueba gratuita de 14 días, después 12,99€ al mes."}/>
                </View>
                <Text style={styles.bottomText}>Después de la prueba gratuita la suscripción anual es de 57.99€ con renovación anual y la prueba mensual es de 12.99 con renovación mensual.</Text>
                <FloatingButton onPress={()=> app.navigateBack()} icon="close"/>
            </ScrollView>
            <FeaturedButton onPress={() => app.navigateBack()} title={"Iniciar Prueba Gratuita"}/>
        </View>
    )
};

const styles = {
    bottomText: {
        textAlign: "center",
        margin: 15,
        marginBottom: 80,
    },
    features: {
        margin: 40,
        marginBottom: 30,
    },
    row: {
        flexDirection: "row",
        alignItems: "center",
        marginBottom: 10,
    },
    title: {
        fontWeight: "bold",
        fontSize: 21,
        textAlign: "center",
        marginBottom: 17,
        marginTop: -40,
    },
    listitem: {
        fontSize: 16.5,
        marginLeft: 10,
    },
    image: {
        width: "100%",
        height: 300,
        backgroundColor: "black",
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
    }
};

export default PremiumScreen;
