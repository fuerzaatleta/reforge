import React from 'react';
import {View} from 'react-native';
import {Icon, ListItem, Text} from 'native-base';
import app from '../../../../../../../src/AppFramework';
import Theme from '../../../../../../theme/theme';

const Index = (props) => {
    return (
        <ListItem style={style.container} noIndent onPress={() => app.navigateTo(props.screen)}>
            <Icon style={{color: "#878787", fontSize: 23, marginRight: 30, marginLeft: 8}} type="MaterialCommunityIcons" name={props.icon} />
            <Text style={style.title}>{props.name}</Text>
        </ListItem>
    );
};

const style = {
    title: {
        paddingTop: 6,
        paddingBottom: 6,
        // fontSize: 17,
    },
    container: {
        borderBottomColor: "#e5e5e5",
        borderBottomWidth: 1,
        // flex: 1,
        // backgroundColor: "#fff",
    },
};

export default Index;
