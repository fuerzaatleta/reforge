import React, {useState} from 'react';
import {View, Text} from 'react-native';
import { TouchableRipple } from 'react-native-paper';
import {useSelector} from 'react-redux';

const PurchaseWidget = (props) => {
    let action = useSelector(state => state.Nexus.type);
    let [selected, setSelected] = useState(props.selected);
    // Nexus().subscribe("PLAN_SELECTED", onDeselection.bind(this));
    return (
            <View style={[styles.container, getBackgroundColor()]}>
                <TouchableRipple borderless style={styles.touchable} onPress={() => onPressed()}>
                    <View>
                        <View style={{flexDirection: "row"}}>
                            <Text style={[styles.title, getTextColor()]}>{props.name}</Text>
                            <Text style={[styles.price, getTextColor()]}>{props.price}</Text>
                        </View>
                        <Text style={[styles.description, getTextColor()]}>{props.description}</Text>
                    </View>
                </TouchableRipple>
            </View>
    );

    function onPressed() {
        // Nexus().dispatch("PLAN_SELECTED");
        setSelected(true);
    }

    function onDeselection() {
        setSelected(false);
    }

    function getTextColor() {
        switch (selected) {
            case true:
                return {color: "#fff"};
            case false:
                return {color: "#363636"};
        }
    }

    function getBackgroundColor() {
        switch (selected) {
            case true:
                return {backgroundColor: "#f42327"};
            case false:
                return {backgroundColor: "#e3e3e3"};
        }
    }
};

const styles = {
    touchable: {
        padding: 15,
        paddingLeft: 25,
        paddingRight: 25,
        borderRadius: 12,
    },
    title: {
        fontSize: 20,
        fontWeight: "bold",
    },
    price: {
        flex: 1,
        fontSize: 23,
        textAlign: "right",
    },
    description: {
        color: "white",
        fontSize: 18,
    },
    container: {
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 15,
        borderRadius: 12,
    }
};

export default PurchaseWidget;
