import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {Assets} from '../../../../../assets/Assets';
import app from '../../../../../../src/AppFramework';
import {observer} from 'mobx-react';

const EmptyWorkout = observer(() => {
    return (
        <View style={styles.container}>
            <Image style={styles.image} source={Assets.EMPTY}/>
            <Text style={styles.title}>NINGUNA RUTINA</Text>
            <Text style={styles.subtitle}>Añade tu primera rutina</Text>
            <TouchableOpacity onPress={() => app.navigateTo("AddWorkout")} style={styles.button}>
                <Text>AÑADIR NUEVA</Text>
            </TouchableOpacity>
        </View>
    )
});

const styles = {
    image: {
        height: 100,
        width: 100,
        marginBottom: 8,
    },
    container: {
        backgroundColor: "#ffffff",
        flex: 1,
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 19,
        color: "#151515",
    },
    subtitle: {
        fontSize: 15,
    },
    button: {
        borderWidth: 1,
        padding: 10,
        marginTop: 15,
        paddingLeft: 40,
        paddingRight: 40,
        borderRadius: 5,
        borderColor: "#d3d3d3",
    }
};

export default EmptyWorkout;
