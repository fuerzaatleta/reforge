import React from 'react';
import {Text, View} from 'react-native';
import Label from '../../../../components/generic/label/label';

const Filters = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Equipamiento</Text>
            <View style={styles.labels}>
                <Label id="BODYWEIGHT" name="Sin Equipamiento" />
                <Label id="DUMBELL" name="Mancuernas" />
                <Label id="KETTLEBELL" name="Kettlebell" />
            </View>
            <View style={styles.separator}/>
            <View style={styles.labels}>
                <Label name="Banda Elástica" />
            </View>
            <View style={styles.separator}/>
            <Text style={styles.title}>Grupo Muscular</Text>
            <View style={styles.labels}>
                <Label id="CHEST" name="Pecho" />
                <Label id="SHOULDERS" name="Hombros" />
                <Label id="BACK" name="Espalda" />
                <Label id="TRICEPS" name="Triceps" />
                <Label id="BICEPS" name="Biceps" />
            </View>
            <View style={styles.separator}/>
            <View style={styles.labels}>
                <Label id="ABS" name="Abdominales" />
                <Label id="CARDIO" name="Cardio" />
            </View>
        </View>
    )
};

const styles = {
    separator: {
        marginBottom: 15,
    },
    title: {
        marginTop: 15,
        marginBottom: 12,
        fontSize: 18,
    },
    container: {
        padding: 15,
        backgroundColor: "#fff",
        flex: 1,
    },
    labels: {
        // backgroundColor: "yellow",
        flexDirection: "row",
    },
    text: {

    },
};

export default Filters;
