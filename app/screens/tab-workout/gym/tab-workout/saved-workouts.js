import React from 'react';
import {Image, FlatList, View, Text, TouchableOpacity} from 'react-native';
import {observer} from 'mobx-react';
import {useWorkoutStore} from '../../../../mobx/workout/deprecated-workout-store';
import {generateUID} from '../../../../utils/crypto';
import {Icon} from "native-base";
import theme from '../../../../theme/theme';
import StoredCover from './stored-cover';
import app from '../../../../../src/AppFramework';
import {Button, TouchableRipple} from 'react-native-paper';
import {Assets} from '../../../../assets/Assets';

const SavedWorkouts = observer((props) => {
    const store = useWorkoutStore();
    let lenght = store.workouts.length;
    return (
        <View showsVerticalScrollIndicator={false} style={styles.container}>
            <CustomWorkout/>
            <FlatList
                data={store.getWorkouts()}
                renderItem={(value) => <StoredCover tab={props.tab} style={styles.item} item={value.item}/>}
                keyExtractor={() => generateUID()}
            />
        </View>
    )
});

const CustomWorkout = () => {
    return (
        <TouchableRipple borderless
            style={box.touchable}
            rippleColor={theme.RIPPLE_COLOR}
            onPress={() => app.navigateTo("AddWorkout")}>
            <View style={box.container}>
                <Icon style={{fontSize: 20, color: "black", paddingRight: 6}} type="Feather" name="plus"/>
                <Text style={box.title}>Rutina Personalizada</Text>
            </View>
        </TouchableRipple>
    )
};

const box = {
    touchable: {
        marginBottom: 10,
        borderRadius: 10,
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: "row",
        padding: 13,
        borderWidth: 1,
        borderColor: "#d0d0d0",
        borderRadius: 10,
    },
    title: {
        fontSize: 18,
    },
};

const styles = {
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
        paddingTop: 0,
        padding: 15,
        flex: 1,
        backgroundColor: "#fff",
    },
    fab: {
        position: "absolute",
        bottom: 20,
        right: 10,
        backgroundColor: "#ff311d"
    },
    item: {
        marginBottom: 8,
    }
};

export default SavedWorkouts;
