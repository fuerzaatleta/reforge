import React, {useState} from 'react';
import {observer} from 'mobx-react';
import {useWorkoutStore} from '../../../../../mobx/workout/deprecated-workout-store';
import Modal from 'react-native-modal';
import {TouchableOpacity, Text, View} from 'react-native';
import Stepper from '../../../../../components/prototype/workout/stepper';
import ProgressCircle from 'react-native-progress-circle'

const RestModal = observer(() => {
    const store = useWorkoutStore();
    const timer = store.tracker.timer;
    return (
        <Modal
            isVisible={store.tracker.showRestTimer}
            swipeDirection="down"
            onSwipeComplete={() => Skip()}>
            <View style={styles.modal}>
                <View style={{alignItems: "center"}}>
                    <View style={styles.draggable}/>
                </View>
                <Text style={styles.text}>Pausa para el Descanso</Text>
                <View style={styles.progress}>
                    <ProgressCircle
                        percent={timer.getProgress()}
                        radius={70}
                        borderWidth={7}
                        color={"#3399FF"}
                        shadowColor={"#cbcbcb"}
                        bgColor="#fff">
                        <Text style={styles.counter}>{timer.getTime()}</Text>
                    </ProgressCircle>
                </View>
                <Stepper style={styles.stepper}/>
                <TouchableOpacity style={styles.btnSkipContent} onPress={Skip}>
                    <Text style={styles.btnSkipText}>Saltar Descanso</Text>
                </TouchableOpacity>
            </View>
        </Modal>
    );

    function Skip() {
        console.log("skipping...");
        store.tracker.timer.stop();
        store.tracker.showTimer(false);
    }
});

const styles = {
    counter: {
        fontSize: 30,
    },
    draggable: {
        borderWidth: 2,
        borderRadius: 30,
        borderColor: "#272727",
        width: "32%",
    },
    btnSkipText: {
        textAlign: "center",
        fontSize: 17,
    },
    btnSkipContent: {
        backgroundColor: "white",
        borderRadius: 6,
        borderWidth: 1,
        borderColor: "#acacac",
        padding: 9,
    },
    modal: {
        margin: 37,
        backgroundColor: "white",
        borderRadius: 8,
        padding: 20,
    },
    text: {
        fontSize: 24,
        color: "black",
        textAlignVertical: "center",
        textAlign: "center",
        marginTop: 15,
    },
    stepper: {
        paddingBottom: 14,
    },
    progress: {
        alignItems: 'center',
        marginBottom: 30,
        marginTop: 30,
    },
};

export default RestModal;
