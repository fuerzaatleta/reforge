import React, {useState} from 'react';
import {View, Text, TextInput} from 'react-native';
import {TouchableRipple} from 'react-native-paper';
import {Icon} from "native-base";
import Theme from '../../../../theme/theme';
import app from '../../../../../src/AppFramework';
import {useWorkoutStore} from '../../../../mobx/workout/deprecated-workout-store';
import Workout from '../../../../utils/dataset/workout';
import {Picker} from '@react-native-picker/picker';

const AddWorkout = () => {
    const store = useWorkoutStore();
    let [title, setTitle] = useState("");
    let [description, setDescription] = useState();
    let [difficulty, setDifficulty] = useState();
    let [goal, setGoal] = useState();
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={{flex: 0.25}}>
                    <TouchableRipple borderless style={styles.backTouchable} onPress={() => app.navigateBack()}>
                        <Icon style={{color: "#656565", fontSize: 27}} name='arrow-back' />
                    </TouchableRipple>
                </View>
                <View style={[{flex: 1}, Theme.center]}>
                    <Text style={styles.headerTitle}>Crear nueva rutina</Text>
                </View>
                <View style={{flex: 0.25}}>
                    <TouchableRipple borderless style={styles.backTouchable} onPress={() => onSubmit()}>
                        <Icon style={{color: "#656565", fontSize: 23}} name='save' />
                    </TouchableRipple>
                </View>
            </View>
            <View style={styles.body}>
                <View style={styles.box}>
                    <Text style={styles.title}>Título de la Rutina</Text>
                    <TextInput
                        placeholder="ej. Rutina Full Body"
                        style={styles.input}
                        onChangeText={text => setDifficulty(text)}
                        value={difficulty}
                    />
                </View>
                <View style={styles.separator}/>
                <View style={styles.box}>
                    <Text style={styles.title}>Descripción</Text>
                    <TextInput
                        placeholder="Breve explicación sobre la rutina"
                        style={styles.input}
                        onChangeText={text => setDescription(text)}
                        value={description}
                    />
                </View>
                <View style={styles.separator}/>
                <View style={styles.row}>
                    <View style={{flex: 1}}>
                        <Text style={styles.title}>Dificultad</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Picker
                            note
                            mode="dropdown"
                            style={styles.picker}
                            selectedValue={difficulty}
                            onValueChange={(value) => setDifficulty(value)}>
                            <Picker.Item label="Principiante" value="beginner" />
                            <Picker.Item label="Intermedio" value="intermediate" />
                            <Picker.Item label="Avanzado" value="advanced" />
                        </Picker>
                    </View>
                </View>
                <View style={styles.separator}/>
                <View style={styles.row}>
                    <View style={{flex: 1}}>
                        <Text style={styles.title}>Dias por Semana</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Picker
                            note
                            mode="dropdown"
                            style={styles.picker}
                            selectedValue={difficulty}
                            onValueChange={(value) => setDifficulty(value)}>
                            <Picker.Item label="1 día/semana" value="1" />
                            <Picker.Item label="2 días/semana" value="2" />
                            <Picker.Item label="3 días/semana" value="3" />
                            <Picker.Item label="4 días/semana" value="4" />
                            <Picker.Item label="5 días/semana" value="5" />
                            <Picker.Item label="6 días/semana" value="6" />
                            <Picker.Item label="7 días/semana" value="7" />
                        </Picker>
                    </View>
                </View>
                <View style={styles.separator}/>
                <View style={styles.row}>
                    <View style={{flex: 1}}>
                        <Text style={styles.title}>Objetivo</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Picker
                            note
                            mode="dropdown"
                            style={styles.picker}
                            selectedValue={difficulty}
                            onValueChange={(value) => setDifficulty(value)}>
                            <Picker.Item label="Musculación" value="bulking" />
                            <Picker.Item label="Definición" value="cutting" />
                            <Picker.Item label="Mantenimiento" value="mantainance" />
                        </Picker>
                    </View>
                </View>
            </View>
        </View>
    );

    function onSubmit() {
        const workout = new Workout();
        workout.setTitle(title);
        workout.setDescription(description);
        workout.setGoal(goal);
        store.createWorkout(workout);
        app.navigateBack();
    }
};

const styles = {
    body: {
        padding: 25,
        paddingTop: 5,
    },
    picker: {
        height: 20,
    },
    input: {
        backgroundColor: "white",
        padding: 0,
    },
    title: {
        fontSize: 15,
        textAlignVertical: "center",
        justifyContent: 'center',
        alignItems: 'center',
    },

    box: {
        // padding: 23,
        paddingBottom: 16,
        paddingTop: 16,
    },
    row: {
        paddingBottom: 26,
        paddingTop: 26,
        flexDirection: "row",
    },
    separator: {
        borderBottomWidth: 1,
        borderColor: "#d5d5d5",
    },
    save: {
        padding: 12,
        flex: 1,
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
    },
    backTouchable: {
        width: 52,
        height: 52,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        marginRight: 7,
    },
    back: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    header: {
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 15,
        // borderBottomWidth: 1,
        borderColor: Theme.SEPARATOR_COLOR,
        flexDirection: "row",
    },
    content: {
        // flex: 1,
    },
    headerTitle: {
        fontSize: 20,
    },
    headerSubtitle: {
        fontSize: 17,
        color: "#636363",
    },
    container: {
        // padding: 25,
        flex: 1,
        backgroundColor: "#fff",
    }
};

export default AddWorkout;
