import React, {useState} from 'react';
import {Text, ActivityIndicator, ScrollView, View} from 'react-native';
import {observer} from 'mobx-react';
import {useWorkoutStore} from '../../../../mobx/workout/deprecated-workout-store';
import MainCover from './main-cover';
import SessionSelector from '../../../../../src/components/WorkoutScreen/SessionSelector';
import AddItem from '../../../../../src/components/WorkoutScreen/AddItem';

const ActiveWorkout = observer((props) => {
    const store = useWorkoutStore();
    const [state, setState] = useState();

    switch (getState()) {
        case "EMPTY":
            return <EmptyState/>;
        case "LOADING":
            return <LoadingState/>;
        case "LOADED":
            return <LoadedState/>;
    }

    function getState() {
        if (store.workoutsLoaded && store.workouts.length <= 0)
            return "EMPTY";
        if (store.workoutsLoaded && store.workouts.length > 0)
            return "LOADED";
        if (!store.workoutsLoaded)
            return "LOADING";
    }
});

const LoadedState = () => {
    return (
        <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
            <MainCover/>
            <SessionSelector/>
            <AddItem/>
        </ScrollView>
    )
};

const LoadingState = () => {
    return (
        <View style={styles.loading}>
            <ActivityIndicator size="large" color={"#e84225"} />
            <Text style={styles.title}>Cargando Rutinas</Text>
        </View>
    )
};

const EmptyState = () => {
    return (
        <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
        </ScrollView>
    )
};



const styles = {
    title: {
        marginTop: 12,
        color: "#999999",
        fontSize: 17,
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
        padding: 15,
        paddingTop: 0,
    },
    loading: {
        flex: 1,
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
    },
};

export default ActiveWorkout;
