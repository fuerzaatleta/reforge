import React from 'react';
import {View, Text, ImageBackground, Pressable} from 'react-native';
import {useWorkoutStore} from '../../../../mobx/workout/deprecated-workout-store';
import {observer} from 'mobx-react';
import {TouchableRipple} from 'react-native-paper';
import Workout from '../../../../utils/dataset/workout';

const StoredCover = observer((props) => {
    const workout: Workout = props.item;
    const store = useWorkoutStore();

    return (
        <TouchableRipple style={[styles.container, props.style]} borderless onPress={() => onPress()}>
            <ImageBackground style={styles.image} source={getThumbnail()}>
                <View style={styles.text}>
                    <Text style={styles.pretitle}>{workout.getGoal()} - {workout.getNumberOfSessions()} Días</Text>
                    <Text style={styles.title}>{workout.name}</Text>
                    { props.active ? <Text>ACTIVA</Text> : <View/> }
                </View>
            </ImageBackground>
        </TouchableRipple>
    );

    function onPress() {
        props.tab("active");
        store.select(props.item);
    }

    function getThumbnail() {
        return {uri: 'https://directus.fuerzaatleta.com/fuerzaatleta/assets/9q8zknn1muwwoogs'};
    }

    function getCategory(id) {
        if("bulking" === id) {
            return "Musculación";
        } else if("cutting" === id) {
            return "Definición";
        } else if("mantainance" === id) {
            return "Mantenimiento"
        }
    }
});

const styles = {
    container: {
        height: 145,
        backgroundColor: "#000000",
        borderRadius: 12,
    },
    badge: {
        color: "white",
        textAlign: 'center',
        padding: 2,
        backgroundColor: "#ff2f33",
        borderRadius: 8,
        width: 90,
        position: "absolute",
        top: 18,
        left: 20,
    },
    text: {
        position: "absolute",
        left: 20,
        // justifyContent: 'container',
        // alignItems: 'container',
    },
    image: {
        width: "100%",
        height: "100%",
        justifyContent: 'center',
    },
    pretitle: {
        color: "white",
        fontSize: 17,
        marginBottom: -2
    },
    title: {
        color: "white",
        fontSize: 21,
        fontWeight: "bold",
    },
    editText: {
        color: "white",
        fontSize: 14,
        paddingLeft: 8,
    },
    edit: {
        marginLeft: -8,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: "row",
        marginBottom: 15,
        padding: 7,
        borderRadius: 4,
        paddingLeft: 0,
        // backgroundColor: "yellow",
        width: 100
    },
};
export default StoredCover;
