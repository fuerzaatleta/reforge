import React from 'react';
import {View, Text, ImageBackground, Pressable} from 'react-native';
import app from '../../../../../src/AppFramework';
import {Icon} from "native-base";
import theme from '../../../../theme/theme';
import {TouchableRipple} from 'react-native-paper';

const MainCover = (props) => {
    return (
        <TouchableRipple style={[styles.container, props.style]} borderless>
            <ImageBackground style={styles.image} source={getThumbnail()}>
                <View style={styles.text}>
                    <Pressable
                        style={styles.edit}
                        onPress={() => app.navigateTo("AddWorkout")}
                        android_ripple={{color: theme.RIPPLE_COLOR_DARK}}>
                        <Icon style={{fontSize: 20, color: "white"}} type="Feather" name="edit"/>
                        <Text style={styles.editText}>EDITAR</Text>
                    </Pressable>
                    <Text style={styles.pretitle}>Musculación - 5 Días</Text>
                    <Text style={styles.title}>Rutina Personalizada</Text>
                </View>
            </ImageBackground>
        </TouchableRipple>
    );

    function getThumbnail() {
        return {uri: 'https://directus.fuerzaatleta.com/fuerzaatleta/assets/9q8zknn1muwwoogs'};
    }

    function getCategory(id) {
        if("bulking" === id) {
            return "Musculación";
        } else if("cutting" === id) {
            return "Definición";
        } else if("mantainance" === id) {
            return "Mantenimiento"
        }
    }
};

const styles = {
    container: {
        height: 145,
        backgroundColor: "#000000",
        borderRadius: 12,
    },
    badge: {
        color: "white",
        textAlign: 'center',
        padding: 2,
        backgroundColor: "#ff2f33",
        borderRadius: 8,
        width: 90,
        position: "absolute",
        top: 18,
        left: 20,
    },
    text: {
        position: "absolute",
        left: 20,
        // justifyContent: 'container',
        // alignItems: 'container',
    },
    image: {
        width: "100%",
        height: "100%",
        justifyContent: 'center',
    },
    pretitle: {
        color: "white",
        fontSize: 17,
        marginBottom: -2
    },
    title: {
        color: "white",
        fontSize: 21,
        fontWeight: "bold",
    },
    editText: {
        color: "white",
        fontSize: 14,
        paddingLeft: 8,
    },
    edit: {
        marginLeft: -8,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: "row",
        marginBottom: 15,
        padding: 7,
        borderRadius: 4,
        paddingLeft: 0,
        // backgroundColor: "yellow",
        width: 100
    },
};

export default MainCover;
