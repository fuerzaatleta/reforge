import React,{useState} from 'react';
import {Text, Dimensions, StyleSheet} from 'react-native';
import SavedWorkouts from './saved-workouts';
import ActiveWorkout from './active-workout';
import {useWorkoutStore} from '../../../../mobx/workout/deprecated-workout-store';
import EmptyWorkout from './exception/empty-workout';
import {observer} from 'mobx-react';
import {TabBar, TabView} from 'react-native-tab-view';
import theme from '../../../../theme/theme';

const initialLayout = { width: Dimensions.get('window').width };

const TabWorkout = observer(() => {
    const store = useWorkoutStore();
    const [index, setIndex] = useState(0);
    const workouts = store.getWorkouts().length;
    const [routes] = useState([
        { key: 'active', title: 'Rutina Activa' },
        { key: 'archive', title: 'Rutinas Guardadas' },
    ]);
    if(workouts <= 0 && store.isLoaded()) {
        return <EmptyWorkout/>
    }

    return (
        <TabView
            style={styles.container}
            activeColor={styles.activeColor}
            inactiveColor={styles.inactiveColor}
            renderTabBar={renderTabBar}
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={initialLayout}
        />
    );
});

const renderScene = ({ route, jumpTo }) => {
    switch (route.key) {
        case 'active':
            return <ActiveWorkout tab={jumpTo} />;
        case 'archive':
            return <SavedWorkouts tab={jumpTo} />;
    }
};

const renderTabBar = (props) => (
    <TabBar
        {...props}
        pressColor={theme.INPUT_RIPLE}
        renderLabel={renderLabel}
        indicatorStyle={styles.indicator}
        labelStyle={styles.label}
        style={styles.tabbar}
    />
);

const renderLabel = ({ route, focused, color }) => {
    return (
        <Text style={styles.title}>{route.title}</Text>
    )
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: "white",
    },
    title: {
        fontSize: 17,
    },
    label: {
        color: "black",
        fontSize: 15,
    },
    tabbar: {
        paddingTop: 4,
        paddingBottom: 4,
        backgroundColor: "white",
        elevation: 0,
    },
    indicator: {
        backgroundColor: "black",
        height: 0,
    },
    inactiveColor: {
        backgroundColor: "white",
    },
    scene: {
        flex: 1,
    },
});

export default TabWorkout;
