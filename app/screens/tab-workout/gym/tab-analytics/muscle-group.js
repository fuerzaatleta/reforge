import React, {useState} from 'react';
import {Text, View} from 'react-native';
import {TouchableRipple} from 'react-native-paper';
import * as Progress from 'react-native-progress';
import theme from '../../../../theme/theme';
import app from '../../../../../src/AppFramework';
import {Content} from '../../../../mobx/content/content-store';
import {muscle} from '../../../../utils/enums';
import {useTranslation} from 'react-i18next';


const MuscleGroup = (props) => {
    const { t, i18n } = useTranslation();
    const muscleID = muscle[props.item];
    const items = Content.exercises.searchByMuscle(muscleID);
    return (
        <TouchableRipple
            rippleColor={theme.INPUT_RIPLE}
            style={styles.touchable}
            borderless onPress={() => app.navigateTo("ExerciseList")}>
            <View style={styles.container}>
                <Text style={styles.title}>{t(props.item.toLowerCase())}</Text>
                <Text style={styles.tag}>{items.length} </Text>
            </View>
        </TouchableRipple>
    )
};

const ProgressWidget = (props) => {
    let [progress, setProgress] = useState(Math.floor(props.progress * 100));
    if(props.progress >= 1)
        return <Text style={styles.excelent}>EN FORMA</Text>;

    return (
        <View style={styles.progress}>
            <View style={styles.progresstext}>
                <Text style={styles.efficiency}>EFICIENCIA</Text>
                <Text style={styles.percent}>{progress}%</Text>
            </View>
            <Progress.Bar color={"#37be4d"}
                          unfilledColor={"#d4d4d4"}
                          borderWidth={0}
                          progress={props.progress}
                          height={6}
                          width={116} />
        </View>
    )
};

const styles = {
    tag: {
        color: "white",
        backgroundColor: "#5f9fd0",
        borderRadius: 4,
        padding: 5,
        width: 42,
        textAlign: "center",
        // paddingRight: 15,
        // paddingLeft: 15,
    },
    excelent: {
        backgroundColor: "#37be4d",
        color: "white",
        borderRadius: 6,
        padding: 5,
        paddingLeft: 10,
        paddingRight: 10,

    },
    touchable: {
        marginBottom: 8,
        borderRadius: 5,
    },
    progressbar: {

    },
    efficiency: {
        fontSize: 11,
        flex: 1,
    },
    percent: {
        fontSize: 11,
    },
    progress: {
        flex: 0.5,
    },
    progresstext: {
        flexDirection: "row",
        margin: 1,
        marginBottom: 3,
    },
    title: {
        fontSize: 17,
        color: "#4b4b4b",
        flex: 1,
    },
    container: {
        flexDirection: "row",
        borderWidth: 1,
        borderRadius: 5,
        borderColor: "#d9d9d9",
        padding: 25,
        paddingLeft: 20,
        paddingRight: 20,
        flex: 1,
        backgroundColor: "#fff",
    }
};

export default MuscleGroup;
