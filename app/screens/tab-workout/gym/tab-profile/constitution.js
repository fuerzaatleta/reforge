import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {TextInput, TouchableRipple} from 'react-native-paper';
import {Icon} from "native-base";
import Theme from '../../../../theme/theme';
import app from '../../../../../src/AppFramework';
import { Container, Header, Content, Form, Item, Input, Label, DatePicker} from 'native-base';

const ConstitutionScreen = () => {
    let [bodyweight, setBodyweight] = useState(app.storage.getItem("BODYWEIGHT"));
    let [height, setHeight] = useState(app.storage.getItem("HEIGHT"));
    let [birthday, setBirthday] = useState(app.storage.getItem("BIRTHDAY"));
    let [weekday, setWeekday] = useState(app.storage.getItem("WEEKDAY"));
    let [neck, setNeck] = useState(app.storage.getItem("NECK"));
    let [waist, setWaist] = useState(app.storage.getItem("WAIST"));
    let [hips, setHips] = useState(app.storage.getItem("HIPS"));
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={{flex: 0.25}}>
                    <TouchableRipple borderless style={styles.backTouchable} onPress={() => app.navigateBack()}>
                        <Icon style={{color: "#656565", fontSize: 27}} name='arrow-back' />
                    </TouchableRipple>
                </View>
                <View style={[{flex: 1}, Theme.center]}>
                    <Text style={styles.headerTitle}>Constitución Física</Text>
                </View>
                <View style={{flex: 0.25}}>
                    <TouchableRipple borderless style={styles.backTouchable} onPress={onSubmit}>
                        <Icon style={{color: "#656565", fontSize: 23}} name='save' />
                    </TouchableRipple>
                </View>
            </View>
            <Text style={styles.description}>Tus datos permiten a nuestro algoritmo ofrecerte recomendaciones específicas a tu constitución física</Text>
            <View style={styles.content}>
                <View>
                    <TextInput
                        style={styles.input100}
                        label="Fecha de Nacimiento"
                        value={birthday}
                        onChangeText={text => setBirthday(text)}/>
                </View>
                <View style={{flexDirection: "row"}}>
                    <TextInput
                        style={styles.input50}
                        placeholder={"centímetros"}
                        label="Altura"
                        value={height}
                        keyboardType="numeric"
                        onChangeText={text => setHeight(text)}/>
                    <TextInput
                        style={styles.input50}
                        placeholder={"kilogramos"}
                        label="Peso Corporal"
                        value={bodyweight}
                        keyboardType="numeric"
                        onChangeText={text => setBodyweight(text)}/>
                </View>
                <View style={styles.separator}/>

                <View>
                    <Text style={styles.title}>Mediciones Corporales</Text>
                    <TextInput
                        style={styles.input100}
                        label="Cuello"
                        placeholder={"centímetros"}
                        value={neck}
                        keyboardType="numeric"
                        onChangeText={text => setNeck(text)}/>
                    <TextInput
                        style={styles.input100}
                        placeholder={"centímetros"}
                        label="Cadera"
                        value={hips}
                        keyboardType="numeric"
                        onChangeText={text => setHips(text)}/>
                    <TextInput
                        style={styles.input100}
                        placeholder={"centímetros"}
                        label="Cintura"
                        value={waist}
                        keyboardType="numeric"
                        onChangeText={text => setWaist(text)}/>
                </View>
            </View>
        </View>
    );

    function onSubmit() {
        app.storage.setItem("BIRTHDAY", birthday);
        app.storage.setItem("HEIGHT", height);
        app.storage.setItem("BODYWEIGHT", bodyweight);
        app.storage.setItem("NECK", neck);
        app.storage.setItem("WAIST", waist);
        app.storage.setItem("HIPS", hips);
        app.navigateBack();
    }
};

const styles = {
    title: {
        fontSize: 20,
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
    },
    separator: {
        // borderBottomWidth: 1,
        marginTop: 40,
        // marginBottom: 35,
    },
    input100: {
        backgroundColor: "white",
        marginLeft: 10,
        marginRight: 10
    },
    input50: {
        backgroundColor: "white",
        flex: 0.5,
        marginLeft: 10,
        marginRight: 10
    },
    label: {
        flex: 0.85,
    },
    input: {
        flex: 0.15,
        fontSize: 17
    },
    description: {
        fontSize: 15,
        // marginTop: 15,
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
    },
    save: {
        padding: 12,
        flex: 1,
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
    },
    backTouchable: {
        width: 52,
        height: 52,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        marginRight: 7,
    },
    back: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    header: {
        // borderBottomWidth: 1,
        borderColor: Theme.SEPARATOR_COLOR,
        flexDirection: "row",
    },
    content: {
        // flex: 1,
        paddingTop: 20,
    },
    headerTitle: {
        fontSize: 20,
    },
    headerSubtitle: {
        fontSize: 17,
        color: "#636363",
    },
    container: {
        padding: 20,
        flex: 1,
        backgroundColor: "#fff",
    }
};
export default ConstitutionScreen;
