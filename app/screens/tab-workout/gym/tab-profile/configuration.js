import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import {TouchableRipple} from 'react-native-paper';

const Configuration = () => {
    return (
        <ScrollView style={styles.container}>
            <Text style={styles.title}>Unidades & Localización</Text>
            <TouchableRipple style={styles.touchable} onPress={() => console.log("asd")}>
                <View>
                    <Text style={styles.itemTitle}>Idioma</Text>
                    <Text style={styles.itemValue}>Español</Text>
                </View>
            </TouchableRipple>
            <TouchableRipple style={styles.touchable} onPress={() => console.log("asd")}>
                <View>
                    <Text style={styles.itemTitle}>Unidad de Peso</Text>
                    <Text style={styles.itemValue}>Kilogramo (kg)</Text>
                </View>
            </TouchableRipple>
            <TouchableRipple style={styles.touchable} onPress={() => console.log("asd")}>
                <View>
                    <Text style={styles.itemTitle}>Unidad de Medida</Text>
                    <Text style={styles.itemValue}>Centímetro (cm)</Text>
                </View>
            </TouchableRipple>
            <TouchableRipple style={styles.touchable} onPress={() => console.log("asd")}>
                <View>
                    <Text style={styles.itemTitle}>Primer Día de la Semana</Text>
                    <Text style={styles.itemValue}>Lunes</Text>
                </View>
            </TouchableRipple>
            <View style={styles.separator}/>
            <Text style={styles.title}>General</Text>
            <TouchableRipple style={styles.touchable} onPress={() => console.log("asd")}>
                    <Text style={styles.itemTitle}>Notificaciones Push</Text>
            </TouchableRipple>
            <TouchableRipple style={styles.touchable} onPress={() => console.log("asd")}>
                <View>
                    <Text style={styles.itemTitle}>Mantener Pantalla Encendida</Text>
                    <Text style={styles.itemValue}>La pantalla permanecerá encendida durante el periodo de entrenamiento.</Text>
                </View>
            </TouchableRipple>
            <TouchableRipple style={styles.touchable} onPress={() => console.log("asd")}>
                <View>
                    <Text style={styles.itemTitle}>Efectos de Sonido</Text>
                    <Text style={styles.itemValue}>Incluye todos los efectos de sonido</Text>
                </View>
            </TouchableRipple>
            <View style={styles.separator}/>
            <Text style={styles.title}>Entrenamiento</Text>
            <TouchableRipple style={styles.touchable} onPress={() => console.log("asd")}>
                <View>
                    <Text style={styles.itemTitle}>Repeticiones por Defecto</Text>
                    <Text style={styles.itemValue}>8 Repeticiones</Text>
                </View>
            </TouchableRipple>
        </ScrollView>
    )
};

const styles = {
    separator: {
        borderBottomWidth: 1,
        borderBottomColor: "#dcdcdc",
        paddingTop: 10,
        paddingBottom: 10,
    },
    itemTitle: {
        fontSize: 16,
    },
    itemValue: {
        paddingTop: 3,
        color: "#696969",
    },
    touchable: {
        paddingLeft: 24,
        paddingBottom: 15,
        paddingTop: 15,
    },
    title: {
        fontSize: 18,
        padding: 24,
        paddingBottom: 10,
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
    }
};

export default Configuration;
