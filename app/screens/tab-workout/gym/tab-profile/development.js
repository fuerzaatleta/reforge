import React from 'react';
import {View, Text} from 'react-native';
import {TouchableRipple} from 'react-native-paper';
import app from '../../../../../src/AppFramework';
import {Persistence} from '../../../../mobx/persistence/persistence-store';

const data = [];
data.push({text: "Pantalla Sandbox", screen: "Sandbox"});

const Development = () => {
    return (
        <View style={styles.container}>
            <TouchableRipple borderless onPress={() => app.navigateTo("Sandbox")}>
                <View>
                    <Text style={styles.text}>Pantalla Sandbox</Text>
                </View>
            </TouchableRipple>
            <TouchableRipple borderless onPress={() => Persistence.save()}>
                <View>
                    <Text style={styles.text}>Persistence Save</Text>
                </View>
            </TouchableRipple>
            <TouchableRipple borderless onPress={() => Persistence.reset()}>
                <View>
                    <Text style={styles.text}>Reset Persistence</Text>
                </View>
            </TouchableRipple>
        </View>
    )
};

const Item = (data) => {
    return (
        <TouchableRipple borderless onPress={() => app.navigateTo("Sandbox")}>
            <View>
                <Text style={styles.text}>{data.item.text}</Text>
            </View>
        </TouchableRipple>
    )
};

const styles = {
    container: {
        flex: 1,
        backgroundColor: "#fff",
        padding: 20,
    },
    text: {
        padding: 10,
        color: "black",
        fontSize: 18,
        marginTop: 5,
        marginBottom: 5,
    },
};

export default Development;
