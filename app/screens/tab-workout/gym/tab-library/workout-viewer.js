import React from 'react';
import {View, Text} from 'react-native';
import {observer} from 'mobx-react';
import SessionBox from '../../../../components/prototype/library/session-box';
import SegmentBar from '../../../../components/prototype/library/segment-bar';
import FeaturedButtonV2 from '../others/common/FeaturedButtonV2';
import BasicHeader from '../../../../components/prototype/library/basic-header';
import {Content} from '../../../../mobx/content/content-store';

const WorkoutViewer = observer(() => {
    const content = Content.workouts;
    const workout = content.getItem();
    return (
        <View style={styles.root}>
            <BasicHeader title="Full Body Principiante"/>
            <View style={styles.container}>
                <View style={styles.detail}>
                    <View style={styles.row}>
                        <Text style={styles.subtitle}>Duración</Text>
                        <SegmentBar value={3} max={4} style={styles.segmentbar}/>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.subtitle}>Dificultad</Text>
                        <SegmentBar value={1} max={4} style={styles.segmentbar}/>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.subtitle}>Sesiones</Text>
                        <Text style={styles.daysperweek}>3 días por semana</Text>
                    </View>
                </View>
                <Text style={styles.descriptionTitle}>Descripción</Text>
                <Text style={styles.text}>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus</Text>
                <View style={styles.space}/>
                <SessionBox day="1" title="Pecho & Abdominales"/>
                <SessionBox day="2" title="Espalda & Gemelos" />
                <SessionBox day="3" title="Piernas"/>
            </View>
            <FeaturedButtonV2 title={"SELECCIONAR PLAN"}/>
        </View>

    )
});

const styles = {
    detail: {
        marginBottom: 20,
        marginTop: 20,
    },
    descriptionTitle: {
        fontSize: 20,
        paddingBottom: 8,
        color: "black"
    },
    space: {
        paddingBottom: 10,
    },
    root: {
        backgroundColor: "#fff",
        flex: 1,
    },
    segmentbar: {
        width: 200,
    },
    separator: {
        borderBottomWidth: 1,
        borderColor: "#dddddd",
    },
    row: {
        flexDirection: "row",
        padding: 9,
        paddingRight: 0,
        paddingLeft: 0,
    },
    title: {
        fontSize: 17,
        paddingBottom: 7,
    },
    subtitle: {
        flex: 1,
        fontSize: 17,
    },
    text: {
        color: "#505050",
        fontSize: 15,
    },
    container: {
        padding: 20,
        paddingTop: 0,
    },
    bottom: {
        position: "aboslute",
        bottom: 0,
    },
    daysperweek: {
        fontSize: 15,
        color: "#575757",
    },
};

export default WorkoutViewer;
