import React from 'react';
import {Text, View} from 'react-native';

const InternetException = () => {
    return (
        <View style={styles.container}>
            <Text>Problemas de Conexión</Text>
        </View>
    )
};

const styles = {
    container: {
        flex: 1,
        backgroundColor: "#fff",
    }
};

export default InternetException;
