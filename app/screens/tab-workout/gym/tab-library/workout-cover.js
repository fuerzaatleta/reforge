import React from 'react';
import {View, Text, ImageBackground} from 'react-native';
import app from '../../../../../src/AppFramework';
import {Assets} from '../../../../assets/Assets';
import {TouchableRipple} from 'react-native-paper';

const WorkoutCover = (props) => {
    return (
        <TouchableRipple style={[styles.container, props.style]} borderless onPress={onPress}>
            <ImageBackground style={styles.image} source={getThumbnail()}>
                { getLabel(props.data.item.premium) }
                <View style={styles.text}>
                    <Text style={styles.pretitle}>{getCategory(props.data.item.goal) + " - " + props.data.item.days + " días"}</Text>
                    <Text style={styles.title}>{props.data.item.title}</Text>
                </View>
            </ImageBackground>
        </TouchableRipple>
    );

    function onPress() {
        app.navigateTo("WorkoutViewer");
    }

    function getLabel(showing) {
        if(showing)
            return <Text style={styles.badge}>PREMIUM</Text>;
        return <View/>
    }

    function getThumbnail() {
        const image = props.data?.item?.featured_image?.data?.full_url;
        if(image == null)
            return Assets.PLACEHOLDER;
        return {uri: image};
    }

    function getCategory(id) {
        if("bulking" === id) {
            return "Musculación";
        } else if("cutting" === id) {
            return "Definición";
        } else if("mantainance" === id) {
            return "Mantenimiento"
        }
    }
};

const styles = {
    container: {
        height: 145,
        backgroundColor: "#000000",
        borderRadius: 12,
    },
    badge: {
        color: "white",
        textAlign: 'center',
        padding: 2,
        backgroundColor: "#ff2f33",
        borderRadius: 8,
        width: 90,
        position: "absolute",
        top: 18,
        left: 20,
    },
    text: {
        position: "absolute",
        left: 20,
    },
    image: {
        width: "100%",
        height: "100%",
        justifyContent: 'center',
    },
    pretitle: {
        // paddingTop: 110,
        // paddingTop: 45,
        // paddingTop: 50,
        color: "white",
        fontSize: 17,
        marginBottom: -2

    },
    title: {
        color: "white",
        fontSize: 21,
        fontWeight: "bold",

    }
};

export default WorkoutCover;
