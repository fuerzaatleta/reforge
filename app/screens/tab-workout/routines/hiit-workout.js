import React from 'react';
import {Text, View, FlatList, TouchableOpacity} from 'react-native';
import {Thumbnail} from 'native-base';
import FeaturedButtonV2 from '../gym/others/common/FeaturedButtonV2';
import {useContentStore} from '../../../mobx/content/content-store';
import {useSystemStore} from '../../../mobx/system/system-store';
import {Workout} from '../../../services/directus/workout';
import Exercise from '../../../services/directus/exercise';
import {useNavigationStore} from '../../../mobx/navigation/navigation-store';

const HiitWorkout = () => {
    const system = useSystemStore();
    const content = useContentStore();
    const id = system.getFlag("CURRENT_WORKOUT");
    const workout: Workout = content.workouts.getItem(id);
    return (
        <View style={styles.root}>
            <View style={styles.container}>
                <View style={styles.spacing}/>
                <Text style={styles.title}>Resumen</Text>
                <Text style={styles.description}>{ workout.getDescription() }</Text>
                <View style={styles.row}>
                    <Text style={styles.title}>Ejercicios</Text>
                    <Text style={styles.rounds}>  -  { workout.getRounds() } Rondas</Text>
                </View>
                <FlatList
                    data={workout.getContent()}
                    ListHeaderComponent={() => <View style={styles.separator}/>}
                    ListFooterComponent={() => <View style={styles.separator}/>}
                    ItemSeparatorComponent={() => <View style={styles.separator}/>}
                    renderItem={({ item, index }) => <Item item={item} index={index}/> }
                    keyExtractor={(item, index) => index.toString()} />
            </View>
            <FeaturedButtonV2 title={"Empezar"}/>
        </View>
    )
};

const Item = (props) => {
    const nav = useNavigationStore();
    const content = useContentStore();
    const exercise: Exercise = content.exercises.getItem(props.item.value);
    return (
        <TouchableOpacity style={item.touchable} onPress={() => nav.viewExercise(exercise)}>
            <View style={item.row}>
                <Thumbnail source={exercise?.getThumbnail()} style={item.thumbnail}/>
                <Text style={item.title}>5x {exercise?.getTitle()}</Text>
            </View>
        </TouchableOpacity>
    );
};

const item = {
    row: {
        flexDirection: "row",
        marginTop: 10,
        marginBottom: 10,
        textAlignVertical: "center",
        textAlign: "center",
        alignItems: 'center',
        paddingLeft: 8,
        paddingRight: 8,
    },
    thumbnail: {
        borderRadius: 8,
        height: 62,
        width: 62,
        borderWidth: 1,
        borderColor: "lightgray",
    },
    title: {
        fontSize: 17,
        paddingLeft: 15,
    }
};

const styles = {
    separator: {
        borderColor: "#d7d7d7",
        borderBottomWidth: 1,
    },
    root: {
        flex: 1,
        backgroundColor: "#fff",
    },
    row: {
        marginBottom: 15,
        flexDirection: "row",
        textAlignVertical: "center",
        textAlign: "center",
        alignItems: 'center',
    },
    spacing: {
        marginTop: 25,
    },
    description: {
        fontSize: 15,
        paddingTop: 10,
        paddingBottom: 20,
    },
    title: {
        fontSize: 22,
        fontWeight: "bold",
    },
    rounds: {
        fontSize: 18,
        color: "#8b8b8b",
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
        paddingLeft: 15,
        paddingRight: 15,
    }
};

export default HiitWorkout;
