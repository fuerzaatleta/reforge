import React from 'react';
import {FlatList, View} from 'react-native';
import GridLibrary from '../../../components/specific/grid-library/grid-library';
import {useContentStore} from '../../../mobx/content/content-store';
import {Equipment} from '../../../mobx/content/workout-db';

const Workouts = () => {
    const content = useContentStore();
    const collection = content.workouts.getByEquipment(Equipment.NO_EQUIPMENT);
    return (
        <View style={styles.container}>
            <FlatList
                numColumns={2}
                data={collection}
                initialNumToRender={6}
                showsVerticalScrollIndicator={false}
                renderItem={(props) => <GridLibrary {...props} />}
                keyExtractor={item => item.id.toString()}
            />
        </View>
    )
};

const styles = {
    container: {
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: "#fff",
        flex: 1,
    }
};

export default Workouts;
