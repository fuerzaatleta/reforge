import React, {useState} from 'react';
import {Image, Text, View} from 'react-native';
import {Icon} from "native-base";
import TouchableRipple from 'react-native-paper/src/components/TouchableRipple/TouchableRipple.native';
import app from '../../../../src/AppFramework';
import {Assets} from '../../../assets/Assets';
import {useSystemStore} from '../../../mobx/system/system-store';


const AnaerobicWorkouts = (props) => {
    const system = useSystemStore();
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.iconBox}>
                    <TouchableRipple borderless onPress={() => app.navigateBack()} style={styles.icon}>
                        <Icon style={{fontSize: 27}} type="MaterialCommunityIcons" name="arrow-left"/>
                    </TouchableRipple>
                </View>
                <View style={styles.descriptions}>
                    <Text style={styles.title}>Selecciona Equipamiento</Text>
                    <Text style={styles.description}>Para continuar deberás seleccionar el equipamiento que deseas utilizar.</Text>
                </View>
            </View>
            <View style={styles.content}>
                <Item source={Assets.EQUIPMENT_TRX}
                      onPress={() => app.navigateTo("ElasticBand")}
                      title={"Banda Elástica"}
                      description={"Extremadamente práctica, su polivalencia permite trabajar todas las partes del cuerpo."}
                      enabled={false} />
                <Item source={Assets.EQUIPMENT_DUMBELL}
                      onPress={() => app.navigateTo("Dumbell")}
                      title={"Mancuernas"}
                      description={"Las mancuernas te permiten entrenar la fuerza explosiva manteniendo una ámplia libertad de movimientos."}
                      enabled={false} />
                <Item source={Assets.EQUIPMENT_KETTLEBELL}
                      onPress={() => app.navigateTo("Kettlebell")}
                      title={"Kettlebell"}
                      description={"La kettlebell te permite entrenar la fuerza explosiva a través de ejercicios dinámicos."}
                      enabled={false} />
            </View>
        </View>
    );
};

const Item = (props) => {
    const system = useSystemStore();
    return (
        <TouchableRipple borderless style={styles.item} onPress={props.onPress}>
            <View>
                <View style={styles.itemContainer}>
                    <Image fadeDuration={0} source={props.source} style={styles.image} />
                    <View style={styles.innerBox}>
                        <Text style={styles.itemTitle}>{props.title}</Text>
                        <Text style={styles.itemSubtitle}>{props.description}</Text>
                    </View>
                </View>
            </View>
        </TouchableRipple>
    );

};

const Indicator = (props) => {
    const color = props.enabled ? "#49b425" : "#9b9b9b";
    const text = props. enabled ? "ACTIVADO" : "DESACTIVADO";
    return (
        <View style={[styles.indicator, {backgroundColor: color}]}>
            <Text style={styles.text}>{ text }</Text>
        </View>
    )
};

const styles = {
    text: {
        fontSize: 12,
        color: "white",
    },
    indicator: {
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
        minWidth: 105,
        padding: 5,
        position: "absolute",
        bottom: 7,
        right: 7,
        borderRadius: 8,
        backgroundColor: "#49b425",
    },
    itemContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: "row",
        padding: 13,
    },
    iconBox: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 0.2,
    },
    innerBox: {
        flex: 1,
        paddingLeft: 15,
        // textAlignVertical: "center",
        // textAlign: "center",
        // justifyContent: 'center',
    },
    itemTitle: {
        fontSize: 18,
    },
    itemSubtitle: {
        color: "#5d5d5d",
    },
    image: {
        borderRadius: 8,
        height: 98,
        width: 82,
        backgroundColor: "#cdcdcd",
    },
    item: {
        marginBottom: 12,
        borderColor: "#d6d6d6",
        marginLeft: 15,
        marginRight: 15,
        borderRadius: 12,
        borderWidth: 1,
    },
    icon: {
        padding: 12,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 15,
        paddingBottom: 15,
        borderRadius: 12,
    },
    header: {
        flexDirection: "row",
        paddingTop: 15,
        paddingBottom: 30,
    },
    descriptions: {
        // marginRight: 15,
        flex: 0.7,
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
    },
    title: {
        fontSize: 20,
    },
    description: {
        color: "#717171",
        fontSize: 15,
    },
};

export default AnaerobicWorkouts;
