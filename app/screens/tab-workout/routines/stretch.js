import {FlatList, View} from 'react-native';
import React from 'react';
import {useContentStore} from '../../../mobx/content/content-store';
import VerticalLibrary from '../../../components/specific/vertical-library';

const Stretch = () => {
    const content = useContentStore();
    const collection = content.workouts.getCategory(2);
    return (
        <View style={styles.container}>
            <FlatList
                data={collection}
                initialNumToRender={6}
                showsVerticalScrollIndicator={false}
                ItemSeparatorComponent={() => <Separator/>}
                renderItem={(props) => <VerticalLibrary {...props} />}
                keyExtractor={item => item.id.toString()}
            />
        </View>
    )
};

const Separator = () => {
    return (
        <View style={styles.separator}/>
    )
};

const styles = {
    separator: {
        borderBottomWidth: 1,
        borderColor: "#d0d0d0",
    },
    container: {
        backgroundColor: "#fff",
        flex: 1,
    }
};
export default Stretch
