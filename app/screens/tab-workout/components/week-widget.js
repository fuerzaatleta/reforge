import React from 'react';
import {Text, View} from 'react-native';
import {Icon} from "native-base";

const WeekWidget = (props) => {
    if (props.locked === true) {
        return <OpenWidget />
    } else if (props.locked === false) {
        return <ClosedWidget />
    } else {
        return <ClosedWidget/>
    }
};

const OpenWidget = (props) => {
    return (
        <View style={widget.open}>
            <Text style={widget.title}>Semana 1</Text>
            <View style={widget.container}>
                <DayWidget title="Lun" />
                <DayWidget title="Mar" />
                <DayWidget title="Mie" />
                <DayWidget title="Jue" />
                <DayWidget title="Vie" />
                <DayWidget title="Sab" />
                <DayWidget title="Dom" />
            </View>
        </View>
    )
};

const ClosedWidget = (props) => {
    return (
        <View style={widget.closed}>
            <Text>In Development</Text>
        </View>
    )
};

const DayWidget = (props) => {
    return (
        <View style={styles.root}>
            <View style={styles.circle}>
                <Icon style={styles.play} type="FontAwesome" name="play"/>
            </View>
            <Text style={styles.title}>{props.title}</Text>
        </View>
    )
};

const widget = {
    closed: {
        padding: 15,
        borderWidth: 1,
        borderRadius: 12,
        borderColor: "#d7d7d7",
    },
    open: {
        borderColor: "#d7d7d7",
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10,
        paddingBottom: 15,
        borderWidth: 1,
        borderRadius: 12,
        backgroundColor: "white",
    },
    container: {
        flexDirection: "row",
    },
    title: {
        fontSize: 17.5,
        paddingBottom: 12,
        color: "#696969",
    }
};

const styles = {
    circle: {
        width: 41,
        height: 41,
        borderRadius: 41 / 2,
        borderWidth: 1.2,
        borderColor: "#d7d7d7",
    },
    play: {
        flex: 1,
        color: "#d7d7d7",
        textAlignVertical: "center",
        textAlign: "center",
        fontSize: 16,
        paddingLeft: 3,
    },
    root: {
        flex: 1,
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
        // ----- Debug -----
        // marginLeft: 2,
        // backgroundColor: "yellow",
    },
    title: {
        paddingTop: 4,
        color: "#575757",
        textAlignVertical: "center",
        textAlign: "center",
        fontSize: 16,
    },
};

export default WeekWidget;
