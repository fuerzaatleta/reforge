import React, {useState, useEffect} from 'react';
import {View, FlatList, BackHandler} from 'react-native';
import {Body, Header, Icon, Left, Right, Title, ListItem, Thumbnail, Text} from 'native-base';
import {Button, Searchbar} from 'react-native-paper';
import app from '../../../../src/AppFramework';
import theme from '../../../theme/theme';
import {Assets} from '../../../assets/Assets';
import Collapsible from 'react-native-collapsible';
import {config} from '../../../config';
import Snackbar from 'react-native-snackbar';
import {Content} from '../../../mobx/content/content-store';
import {User} from '../../../mobx/user/user-store';

let exercisesToAdd = [];
const ExerciseFinder = () => {
    exercisesToAdd = [];
    let [input, setInput] = useState("");
    let content = Content.exercises.searchByName(input);
    if(config.EXERCISE_FILTER)
        content = content.filter((value) => value.thumbnail != null);

    // useEffect(() => {
    //     exercisesToAdd = [];
    //     return () => {
    //         store.session().addExercises(exercisesToAdd);
    //         Snackbar.dismiss();
    //     }
    // });

    useEffect(() => {
        exercisesToAdd = [];
        BackHandler.addEventListener("hardwareBackPress", backAction);
        return () =>
            BackHandler.removeEventListener("hardwareBackPress", backAction);
    }, []);

    return (
        <View style={styles.container}>
            <Header
                androidStatusBarColor={"white"}
                iosBarStyle="dark-content"
                style={styles.header}>
                <Left>
                    <Button color={"gray"} onPress={() => onBackButton() }>
                        <Icon style={{color: "#656565", fontSize: 27}} name='arrow-back'/>
                    </Button>
                </Left>
                <Body>
                    <Title style={{color: "black"}}>Lista de Ejercicios</Title>
                </Body>
                <Right>
                    <Button color={"gray"} onPress={() => alert("No implementado")}>
                        <Icon style={{color: "#656565", fontSize: 27}} name='add'/>
                    </Button>
                </Right>
            </Header>
            <View style={styles.searchBox}>
                <Searchbar
                    style={styles.searchbar}
                    placeholder="Buscar un ejercicio"
                    onChangeText={(text) => onSearchChange(text)}
                    value={input}/>
            </View>
            <FlatList
                data={content}
                renderItem={({ item, index }) => <Item key={index} id={item.id} data={item} parent={this}/> }
                keyExtractor={item => item.id.toString()}>
            </FlatList>
        </View>
    );

    function backAction() {
        const session = User.designer.session();
        session.addExercises(exercisesToAdd);
        Snackbar.dismiss();
    }

    function onSearchChange(text) {
        setInput(text);
    }

    function onBackButton() {
        backAction();
        app.navigateBack();
    }
};

const Item = (props) => {
    let [collapsed, setCollapsed] = useState(false);
    let image = Assets.PLACEHOLDER;
    if(props.data.thumbnail != null) {
        image = {uri: props.data.thumbnail.data.full_url};
    }

    return (
        <Collapsible collapsed={collapsed}>
            <ListItem noIndent thumbnail style={styles.item} onPress={() => onClick(props)}>
                <View style={exercise.left}>
                    <Thumbnail square style={styles.img} source={image}/>
                </View>
                <Body style={exercise.body}>
                    <Text style={styles.title}>{props.data.name}</Text>
                </Body>
            </ListItem>
        </Collapsible>
    );

    function onClick(props) {
        exercisesToAdd.push(props.id);
        setCollapsed(true);
        Snackbar.show({
            text: 'El ejercicio se ha añadido',
            duration: Snackbar.LENGTH_SHORT,
            action: {
                text: 'DESHACER',
                onPress: () => { /* Do something. */ },
            },
        });
    }
};


const styles = {
    searchBox: {
        marginBottom: 15,
    },
    searchbar: {
        borderRadius: 30,
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 5,
    },
    img: {
        // flex: 0.5,
        // backgroundColor: "#b6b6b6",
        backgroundColor: "white",
        borderWidth: 1,
        borderColor: "#d5d5d5",
        borderRadius: 7,
        height: 76,
        width: 76,
        marginTop: 2,
        marginBottom: 2,
    },
    title: {
        fontSize: 18,
    },
    item: {
        paddingTop: 6,
        paddingBottom: 6,
        borderTopWidth: 1,
        backgroundColor: "white",
        borderColor: "rgb(220,224,229)",
    },
    container: {
        flex: 1,
        backgroundColor: "white",
    },
    header: {
        backgroundColor: "white",
        marginBottom: 6,
        elevation: 0,
    },
};

const exercise = {
    animatedView: {
        height: 10,
    },
    exerciseSelected: {
        backgroundColor: theme.SUCCESS_COLOR,
        padding: 17,
        flexDirection: "row",
    },
    left: {

    },
    body: {
        borderBottomWidth: 0,
    },
    right: {
        marginRight: 22,
        borderRadius: 2,
    },
};

export default ExerciseFinder;
