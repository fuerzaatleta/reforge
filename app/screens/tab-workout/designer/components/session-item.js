import React from 'react';
import {Pressable, Text, View} from 'react-native';
import app from '../../../../../src/AppFramework';
import {ActionSheet, Icon} from 'native-base';
import Exercise from '../../../../utils/dataset/exercise';
import Theme from '../../../../theme/theme';
import {TouchableRipple} from 'react-native-paper';
import {Content} from '../../../../mobx/content/content-store';
import {observer} from 'mobx-react';
import Completable from '../../../../../src/components/widgets/Completable';
import {useWorkoutStore} from '../../../../mobx/workout/workout-store';

let height = 99;
let exercise: Exercise = null;


const SessionItem = observer((props) => {
    let store = useWorkoutStore();
    const index = props.data.index;
    const styles = props.data.isActive ? style.active : style.inactive;

    exercise = store.session().getExercise(index);
    const reps = exercise.getRepetitions(0);
    const sets = exercise.getSetNumber();
    const content = Content.exercises.getItem(props.data.item.id);
    return (
        <TouchableRipple style={style.touchable} borderless onPress={submit}>
            <View style={[style.container, styles]}>
                <View style={style.left}>
                    <Icon onLongPress={props.data.drag} style={style.draggable} type="MaterialCommunityIcons" name="drag-vertical"/>
                </View>
                <Completable
                    completed={exercise.isComplete()}
                    style={style.img}
                    source={content.getThumbnail()}/>
                <View style={style.center}>
                    <Text style={style.title}>{content.getName()}</Text>
                    <Text style={style.subtitle}>{sets} series de {reps} repeticiones</Text>
                </View>
                <Pressable onPress={onConfig} style={style.right}>
                    <Icon style={{ color: "#a4a4a4", fontSize: 22}} type="FontAwesome" name="ellipsis-v" size={70}/>
                </Pressable>
            </View>
        </TouchableRipple>
    );

    function submit() {
        if(!(props.data.item instanceof Exercise)) {
            console.warn("[WARN] Unable to select exercise");
        } else {
            store.setExerciseIndex(props.data.index);
            store.select(props.data.item);
            app.navigateTo("ExerciseViewer");
        }
    }

    function onConfig() {
        const BUTTONS = ["Editar", "Duplicar", "Eliminar"];
        const DESTRUCTIVE_INDEX = 3;
        const CANCEL_INDEX = 4;
        ActionSheet.show(
            {
                options: BUTTONS,
                cancelButtonIndex: CANCEL_INDEX,
                destructiveButtonIndex: DESTRUCTIVE_INDEX,
                label: "Sesión de Entrenamiento"
            },
            selection => { onUserAction(selection) }
        )
    }

    function onUserAction(index) {
        console.log("Selected action index... " + index);
        if (index === 2) {
            store.session().delete(index);
        } else if(index === 1) {
            store.session().duplicate(index);
        } else if(index === 0) {
            submit();
        }
    }
});

const style = {
    active: {
        backgroundColor: "#efefef",
    },
    inactive: {

    },
    touchable: {
        height: height,
    },
    right: {
        justifyContent: "center",
        padding: 6,
        paddingRight: 25,
    },
    center: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 20,
    },
    left: {
        justifyContent: "center",
        padding: 6,
    },
    draggable: {
        color: "gray",
        fontSize: 30,
    },
    async_img: {
        borderRadius: 12,
        backgroundColor: "#ffffff",
        borderColor: "#d5d5d5",
        borderWidth: 1,
        width: 80,
        height: 80,
    },
    img: {
        borderRadius: 12,
        backgroundColor: "#ffffff",
        borderColor: "#d5d5d5",
        borderWidth: 1,
        justifyContent: "center",
        alignItems: "center",
        // flex: 0.3,
        marginTop: 7,
        marginBottom: 7,
    },
    container: {
        paddingTop: 2,
        paddingBottom: 2,
        backgroundColor: "rgb(255,255,255)",
        flexDirection: "row",
        borderColor: "rgb(220,224,229)",
        borderBottomWidth: 1,
    },
    title: {
        fontSize: 18,
    },
    subtitle: {
        fontSize: 14,
        color: Theme.SUBTITLE_COLOR,
    },
    boxThumbnail: {
        justifyContent: "center",
        alignItems: "center",
        // flex: 0.3,
        marginTop: 7,
        marginBottom: 7,
    },
};

export default SessionItem;
