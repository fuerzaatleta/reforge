import React from 'react';
import {View} from 'react-native';
import {Button} from 'react-native-paper';
import app from '../../../../../src/AppFramework';
import {white} from '../../../../components/prototype/buttonTheme';
import ExerciseViewer from '../../../tab-exercises/aerobic/exercise-viewer';
import {observer} from 'mobx-react';
import Theme from '../../../../theme/theme';
import {useWorkoutStore} from '../../../../mobx/workout/workout-store';

const SessionStart = observer(() => {
    const store = useWorkoutStore();
    if (store.tracker.running)
        return <WorkoutRunning/>;
    else
        return <WorkoutNotRunning/>;
});

const WorkoutRunning = () => {
    const store = useWorkoutStore();
    return (
        <View style={[styles.root, {flexDirection: "row"}]}>
            <Button
                mode="contained"
                color={"#ef3e26"}
                style={[white.style, {flex: 1}]}
                contentStyle={[white.content]}
                labelStyle={white.label}
                onPress={() => store.tracker.stop(true)}>
                ABANDONAR
            </Button>
            <View style={{padding: 4}}/>
            <Button
                mode="contained"
                color={"#ffffff"}
                style={[white.style, {flex: 1}]}
                contentStyle={white.content}
                onPress={() => app.navigateTo("ExerciseViewer")}>
                CONTINUAR
            </Button>
        </View>
    )
};

const WorkoutNotRunning = () => {
    const store = useWorkoutStore();
    return (
        <View>
            <Button
                mode="contained"
                style={button.style}
                contentStyle={button.content}
                onPress={() => store.tracker.preparation()}>
                INICIAR ENTRENAMIENTO
            </Button>
        </View>
    )
};

export const button = {
    style: {
        elevation: 0,
        backgroundColor: Theme.PRIMARY_COLOR,
        marginTop: 5,
        borderRadius: 7,
        marginLeft: 12,
        marginRight: 12,
    },
    label: {
        color: "black",
    },
    content: {
        padding: 3,
    },
};


const styles = {
    root: {
        paddingRight: 20,
        paddingLeft: 20,
        paddingTop: 8,
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
    }
};

export default SessionStart;
