import React, {useState} from 'react';
import {Text, View} from 'react-native';
import Expandable from '../../../components/generic/expandable';
import TouchableRipple from 'react-native-paper/src/components/TouchableRipple/TouchableRipple.native';
import {Icon} from "native-base";
import {useSystemStore} from '../../../mobx/system/system-store';
import {observer} from 'mobx-react';

const ProgramItem = observer((props) => {
    const system = useSystemStore();
    let selected = system.getFlag("EXPANDED_PROGRAM");
    return (
        <TouchableRipple style={styles.touchable} rippleColor={"#7a7a7a"} borderless onPress={open}>
            <View style={styles.container}>
                <Expandable state={isOpen()} open={<OpenWidget/>} closed={<ClosedWidget/>}/>
            </View>
        </TouchableRipple>
    );

    function OpenWidget() {
        return (
            <View style={styles.widget}>
                <View style={styles.open}>
                    <View style={styles.header}>
                        <Text style={inner.title}>{props.title}</Text>
                        <TouchableRipple borderless style={styles.inspect} onPress={() => system.setFlag("PREMIUM_MODAL", true)}>
                            <View style={inner.touchablebox}>
                                <Text>INSPECCIONAR</Text>
                                <Icon style={inner.icon} type="MaterialCommunityIcons" name="chevron-right"/>
                            </View>
                        </TouchableRipple>
                    </View>
                    <Text>{props.description}</Text>
                </View>
            </View>
        )
    }

    function ClosedWidget() {
        return (
            <View style={styles.widget}>
                <View style={styles.closed}>
                    <Text style={styles.title}>{props.title}</Text>
                    <Text style={styles.subtitle}>26 días, 4 sesiones/semana</Text>
                    <Icon style={styles.icon} type="FontAwesome" name="chevron-down"/>
                </View>
            </View>
        )
    }

    function open() {
        if (isOpen()) {
            system.setFlag("EXPANDED_PROGRAM", -1);
        } else {
            system.setFlag("EXPANDED_PROGRAM", props.id);
        }
    }

    function isOpen() {
        if (props.id == null) {
            console.warn("There was a problem with the ID");
            return false
        } else if (props.id == system.getFlag("EXPANDED_PROGRAM", -1)) {
            return true
        } else {
            return false;
        }
    }
});

const inner = {
    title: {
        flex: 1,
    },
    icon: {
        color: "black",
    },
    touchablebox: {
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: "row",
    }
};

const styles = {
    header: {
        flexDirection: "row",
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
    },
    inspect: {
        borderRadius: 8,
        padding: 8,
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
        flex: 0.65,
    },
    widget: {
        width: "100%",
        justifyContent: 'center',
    },
    title: {
        paddingLeft: 30,
        color: "white",
        fontSize: 19,
    },
    subtitle: {
        paddingLeft: 30,
        color: "white",
        fontSize: 14
    },
    icon: {
        fontSize: 18,
        color: "white",
        right: 33,
        position: "absolute",
    },
    touchable: {
        borderRadius: 12,
        marginBottom: 11,
    },
    open: {
        borderRadius: 12,
        backgroundColor: "white",
        height: 200,
        borderWidth: 1,
        paddingTop: 12,
        padding: 20,
        borderColor: "#c9c9c9",
    },
    closed: {
        borderRadius: 12,
        justifyContent: 'center',
        backgroundColor: "black",
        height: 116,
    },
    container: {
        backgroundColor: "#fff",
    }
};

export default ProgramItem;
