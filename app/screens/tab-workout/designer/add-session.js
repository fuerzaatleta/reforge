import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {TextInput, TouchableRipple} from 'react-native-paper';
import {Icon} from "native-base";
import Theme from '../../../theme/theme';
import app from '../../../../src/AppFramework';
import { Item, Label, Picker, CheckBox} from 'native-base';
import {useWorkoutStore} from '../../../mobx/workout/workout-store';

const AddSession = () => {
    const store = useWorkoutStore();
    let [name, setName] = useState("");
    let [weekday, setWeekday] = useState("monday");
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={{flex: 0.25}}>
                    <TouchableRipple borderless style={styles.backTouchable} onPress={() => app.navigateBack()}>
                        <Icon style={{color: "#656565", fontSize: 27}} name='arrow-back' />
                    </TouchableRipple>
                </View>
                <View style={[{flex: 1}, Theme.center]}>
                    <Text style={styles.headerTitle}>Crear nueva sesión</Text>
                </View>
                <View style={{flex: 0.25}}>
                    <TouchableRipple borderless style={styles.backTouchable} onPress={onSubmit}>
                        <Icon style={{color: "#656565", fontSize: 23}} name='save' />
                    </TouchableRipple>
                </View>
            </View>
            <View style={styles.content}>
                <Item fixedLabel style={styles.input100}>
                    <Label>Día de la Semana</Label>
                    <Picker
                        mode="dropdown"
                        iosIcon={<Icon name="arrow-down" />}
                        style={{ width: undefined, fontSize: 20 }}
                        placeholder="Selecciona día de la semana..."
                        placeholderStyle={{ color: "#bfc6ea", fontSize: 17 }}
                        placeholderIconColor={"#007aff"}
                        selectedValue={weekday}
                        onValueChange={(value) => setWeekday(value)}>
                        <Picker.Item label="Lunes" value="monday" />
                        <Picker.Item label="Martes" value="tuesday" />
                        <Picker.Item label="Miércoles" value="wednesday" />
                        <Picker.Item label="Jueves" value="thursday" />
                        <Picker.Item label="Viernes" value="friday" />
                        <Picker.Item label="Sábado" value="saturday" />
                        <Picker.Item label="Domingo" value="sunday" />
                    </Picker>
                </Item>
                <TextInput
                    style={styles.input100}
                    label="Título de la Rutina"
                    value={name}
                    onChangeText={text => setName(text)}/>

            </View>
        </View>
    );

    function onSubmit() {
        store.designer.createSession(name, weekday);
        app.navigateBack();
    }
};

const styles = {
    input100: {
        backgroundColor: "white",
        marginLeft: 10,
        marginRight: 10
    },
    input50: {
        backgroundColor: "white",
        flex: 0.5,
        marginLeft: 10,
        marginRight: 10
    },
    save: {
        padding: 12,
        flex: 1,
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
    },
    backTouchable: {
        width: 52,
        height: 52,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        marginRight: 7,
    },
    back: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    header: {
        // borderBottomWidth: 1,
        borderColor: Theme.SEPARATOR_COLOR,
        flexDirection: "row",
    },
    content: {
        // flex: 1,
    },
    headerTitle: {
        fontSize: 20,
    },
    headerSubtitle: {
        fontSize: 17,
        color: "#636363",
    },
    container: {
        padding: 20,
        flex: 1,
        backgroundColor: "#fff",
    }
};

export default AddSession;
