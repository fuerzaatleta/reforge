import React from 'react';
import {FlatList, View} from 'react-native';
import { Icon, Fab } from 'native-base';
import {color} from '../../../theme/color';
import app from '../../../../src/AppFramework';
import {observer} from 'mobx-react';
import {log} from '../../../utils/logging';
import {useWorkoutStore} from '../../../mobx/workout/workout-store';
import DesignerItem from '../../../components/specific/designer-item/designer-item';

const Program = observer(() => {
    const store = useWorkoutStore();
    log.debug("Number of sessions... ", store.designer.getSessions().length);
    return (
        <View style={styles.container}>
            <FlatList
                style={styles.list}
                data={store.designer.getSessions()}
                initialNumToRender={6}
                showsVerticalScrollIndicator={false}
                renderItem={(props) => <DesignerItem {...props} />}
                keyExtractor={(item, index) => index}
            />
            <Fab
                active={false}
                direction="up"
                containerStyle={{ }}
                style={styles.fab}
                position="bottomRight"
                onPress={() => app.navigateTo("AddSession")}>
                <Icon name="add" />
            </Fab>
        </View>
    )
});

const styles = {
    container: {
        flex: 1,
        padding: 5,
        backgroundColor: "#fff",
    },
    list: {

    },
    fab: {
        backgroundColor: color.primary,
    },
};

export default Program;
