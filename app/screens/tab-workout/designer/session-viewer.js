import React from 'react';
import {View, Header, Left, Icon, Body, Title} from 'native-base';
import {Button} from 'react-native-paper';
import ExerciseFinder from './exercise-finder';
import Theme from '../../../theme/theme';
import app from '../../../../src/AppFramework';
import {observer} from 'mobx-react';
import DraggableFlatList from 'react-native-draggable-flatlist';
import {User} from '../../../mobx/user/user-store';
import SessionStart from './components/session-start';
import SessionItem from './components/session-item';
import {useWorkoutStore} from '../../../mobx/workout/workout-store';

const ITEM_HEIGHT = 99;
const SessionViewer = observer((props) => {
    const store = useWorkoutStore();
    const session = store.designer.session();
    // Hack. Si quitas esto MOBX deja de observar.
    const length = session.exercises.length;
    return (
        <View style={{ flex: 1, backgroundColor: "white", }}>
            <Header
                androidStatusBarColor={"white"}
                iosBarStyle="dark-content"
                style={styles.header}>
                <Left>
                    <Button color={"gray"} onPress={() => {app.navigateBack()}}>
                        <Icon style={{color: "#656565", fontSize: 27}} name='arrow-back' />
                    </Button>
                </Left>
                <Body>
                    <Title style={{color: "black"}}>Entrenamiento</Title>
                </Body>
                <View style={styles.btnAddItem}>
                    <Button color={"gray"} onPress={() => app.navigateTo("WorkoutEditor") }>
                        ANALÍTICAS
                    </Button>
                </View>
            </Header>
            <View style={{ flex: 1 }}>
                <DraggableFlatList
                    style={styles.flatlist}
                    data={session.getExercises()}
                    renderItem={(data) => <SessionItem data={data} />}
                    // keyExtractor={() => generateUID()} - FUCKING BUGGED!
                    keyExtractor={(item, index) => `draggable-item-${index}`}
                    initialNumToRender={0}
                    activationDistance={10}
                    getItemLayout={(data, index) => (
                    {length: ITEM_HEIGHT, offset: ITEM_HEIGHT * index, index}
                )}
                    onDragEnd={({ data }) => session.setExercises(data)}
                />
            </View>
            <View style={styles.bottomContainer}>
                <Button icon="plus-circle-outline"
                        mode="contained"
                        style={styles.addExercise_btn}
                        labelStyle={styles.addExercise_label}
                        contentStyle={styles.addExercise_content}
                        onPress={() => app.navigateTo("ExerciseFinder")}>
                    AÑADIR EJERCICIO
                </Button>
                <SessionStart/>
            </View>
        </View>
    )
});


const styles = {
    flatlist: {
        marginBottom: 115,
    },
    addExercise_label: {
        color: "black",
    },
    addExercise_btn: {
        elevation: 0,
        color: "black",
        paddingTop: 3,
        paddingBottom: 3,
        marginBottom: 1,
        marginLeft: 14,
        marginRight: 14,
        backgroundColor: "white",
        borderColor: "rgba(16,16,17,0.21)",
        borderWidth: 1,
    },
    addExercise_content: {
        color: "black",
    },
    btnAddItem: {
        justifyContent: 'center',
    },
    header: {
        backgroundColor: "white",
        borderBottomWidth: 1,
        elevation: 0,
        borderBottomColor: "rgb(220,224,229)",
    },
    item: {
        padding: 20,
    },
    btn: {
        elevation: 0,
        backgroundColor: Theme.PRIMARY_COLOR,
        marginTop: 5,
        borderRadius: 25,
        marginLeft: 10,
        marginRight: 10,
    },
    btnContent: {
        padding: 3,
    },
    bottomContainer: {
        position: 'absolute',
        bottom: 10,
        width: "100%",
    }
};

export default SessionViewer;
