import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import Program from './designer/program';
import MainHeader from '../../components/specific/main-header/main-header';
import PremadeWorkouts from '../tab-exercises/premade-workouts';

const TabWorkout = () => {
    const Tab = createMaterialTopTabNavigator();
    return (
        <Tab.Navigator
            swipeEnabled={false}
            backBehavior="none"
            style={styles.container}
            tabBar={(props) => <MainHeader {...props} />}>
            <Tab.Screen name="TAB1" component={PremadeWorkouts} />
            <Tab.Screen name="TAB2" component={Program} />
        </Tab.Navigator>
    );
};

const styles = {
    container: {
        marginLeft: 4,
        marginRight: 4,
        paddingTop: 15,
        backgroundColor: "white",
    }
};


export default TabWorkout;
