import React, {useEffect} from 'react';
import {BackHandler, Text, TouchableOpacity, View} from 'react-native';
import Modal from 'react-native-modal';
import {useSystemStore} from '../../mobx/system/system-store';
import {observer} from 'mobx-react';
import theme from '../../theme/theme';
import {color} from '../../theme/color';

const Countdown = observer((props) => {
    const system = useSystemStore();
    return (
        <Modal
            onBackButtonPress={() => system.countdown.show(false)}
            animationIn="fadeIn"
            animationOut="fadeOut"
            backdropColor={"white"}
            backdropOpacity={1}
            backdropTransitionOutTiming={0}
            isVisible={system.countdown.visible}
            onSwipeComplete={() => console.log("complete")}>
            <View style={styles.modal}>
                <Text style={styles.counter}>{system.countdown.getTime()}</Text>
                <Text style={styles.label}>¡Prepárate!</Text>
                <View style={styles.bottomContainer}>
                    <TouchableOpacity style={styles.btn} onPress={() => system.countdown.cancel()}>
                        <Text style={styles.btn_text}>Saltar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    );

    function backAction() {
        console.log("asd");
    }
});

const styles = {
    btn: {
        marginTop: 6,
        backgroundColor: "white",
        borderRadius: 6,
        borderWidth: 1,
        borderColor: "#acacac",
        padding: 11,
    },
    btn_highlight: {
        backgroundColor: "#ff3315",
        marginTop: 6,
        borderRadius: 6,
        padding: 11,
    },
    btn_text: {
        textAlign: "center",
        fontSize: 17,
    },

    bottomContainer: {
        position: "absolute",
        width: "95%",
        bottom: 6,
    },
    modal: {
        flex: 1,
        backgroundColor: "#FFF",
        height: "100%",
        alignItems: "center",
        justifyContent: 'center',
    },
    counter: {
        // color: "#e83b29",
        color: color.primary,
        fontSize: 72,
        fontWeight: "bold",
    },
    label: {
        marginTop: -10,
        fontSize: 23,
        color: theme.DARK_FONT_COLOR,
    },
    container: {
        backgroundColor: "#FFF",
        height: "100%",
        alignItems: "center",
        justifyContent: 'center',
    }
};

export default Countdown;
