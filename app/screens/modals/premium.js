import React, {useEffect} from 'react';
import {BackHandler, Text, View} from 'react-native';
import {Icon, Thumbnail} from 'native-base';
import Modal from 'react-native-modal';
import TouchableRipple from 'react-native-paper/src/components/TouchableRipple/TouchableRipple';
import Swiper from 'react-native-swiper'
import {Assets} from '../../assets/Assets';
import {useSystemStore} from '../../mobx/system/system-store';
import {observer} from 'mobx-react';
import {useUserStore} from '../../mobx/user/user-store';
import {color} from '../../theme/color';

const Premium = observer((props) => {
    const system = useSystemStore();
    let visible = system.getFlag(props.id, false);

    const user = useUserStore();
    return (
        <Modal
            onBackButtonPress={() => system.setFlag(props.id, false)}
            animationType="slide"
            backdropColor={"#000"}
            backdropOpacity={0.85}
            backdropTransitionOutTiming={0}
            isVisible={visible}
            onSwipeComplete={() => console.log("complete")}>
                <View style={styles.root}>
                    <Text style={styles.title}>Consigue Reforge Premium</Text>
                    <Content />
                    <View style={styles.prices}>
                        <View style={styles.box}>
                            <Text style={styles.monthsNumber}>12</Text>
                            <Text style={styles.monthsTitle}>meses</Text>
                            <Text style={styles.mothlyPrice}>2.99€ / mes</Text>
                        </View>
                        <View style={styles.box}>
                            <Text style={styles.monthsNumber}>6</Text>
                            <Text style={styles.monthsTitle}>meses</Text>
                            <Text style={styles.mothlyPrice}>4.99€ / mes</Text>
                        </View>
                        <View style={styles.box}>
                            <Text style={styles.monthsNumber}>3</Text>
                            <Text style={styles.monthsTitle}>meses</Text>
                            <Text style={styles.mothlyPrice}>7.99€ / mes</Text>
                        </View>
                    </View>
                    <TouchableRipple borderless onPress={() => user.billing.requestSubscription()} style={styles.button}>
                        <Text style={styles.buttonText}>CONTINUAR</Text>
                    </TouchableRipple>
                </View>
                <Text style={styles.legalTitle}>Facturación periódica, cancela en cualquier momento</Text>
                <Text style={styles.legalSubtitle}>
                    Si tocas "continuar", tu pago se cargará en tu cuenta de Google Play y tu suscripción
                    se renovará de forma automática, por el mismo precio y por el mismo periodo,
                    hasta que lo canceles en los ajustes de Google Play. Al tocar "continuar", aceptas
                    nuestras Condiciones.
                </Text>
        </Modal>
    )
});

const Content = () => {
    return(
        <Swiper
            activeDotColor={"#ff371e"}
            autoplay={true}
            autoplayTimeout={5}>
            <View>
                <View style={styles.circle}>
                    <Icon style={styles.icon} type="MaterialCommunityIcons" name="lock"/>
                </View>
                <Text style={styles.swiperTitle}>Editor de Rutinas</Text>
                <Text style={styles.swiperSubtitle}>Crea, gestiona y mejora tus rutinas con el editor avanzado
                    disponible en premium.</Text>
            </View>
            <View>
                <View style={styles.circle}>
                    <Icon style={styles.icon} type="MaterialCommunityIcons" name="lock"/>
                </View>
                <Text style={styles.swiperTitle}>Biblioteca de Ejercicios</Text>
                <Text style={styles.swiperSubtitle}>Desbloquea los ejercicios de la biblioteca de reforge.</Text>
            </View>
            <View>
                <View style={styles.circle}>
                    <Icon style={styles.icon} type="MaterialCommunityIcons" name="lock"/>
                </View>
                <Text style={styles.swiperTitle}>Desbloquea la Librería de Ejercicios</Text>
                <Text style={styles.swiperSubtitle}>Aumenta la variedad de ejercicios disponibles</Text>
            </View>
        </Swiper>
    )
};

const styles = {
    swiperBox: {
        // textAlignVertical: "center",
        // textAlign: "center",
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    swiperTitle: {
        fontSize: 18,
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
    },
    swiperSubtitle: {
        marginLeft: 30,
        marginRight: 30,
        fontSize: 14,
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
    },
    circle: {
        paddingTop: 22,
        color: color.border,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        fontSize: 16,
        textAlignVertical: "center",
        textAlign: "center",
        color: "white",
    },
    button: {
        backgroundColor: color.primary,
        marginLeft: 50,
        marginRight: 50,
        borderRadius: 20,
        margin: 20,
        padding: 12,
    },
    legalTitle: {
        marginTop: 6,
        marginBottom: 3,
        fontSize: 13.5,
        fontWeight: "bold",
        color: "white",
        textAlignVertical: "center",
        textAlign: "center",
    },
    legalSubtitle: {
        color: "#828282",
        fontSize: 12,
        textAlignVertical: "center",
        textAlign: "center",
    },
    monthsNumber: {
        textAlignVertical: "center",
        textAlign: "center",
        fontSize: 32,
    },
    monthsTitle: {
        textAlignVertical: "center",
        textAlign: "center",
        marginTop: -9,
        fontSize: 16,
    },
    mothlyPrice: {
        marginTop: 7,
        marginBottom: -8,
    },
    prices: {
        backgroundColor: "#f9f9f9",
        flexDirection: "row",
    },
    box: {
        padding: 20,
        borderColor: "#d7d7d7",
        borderWidth: 1,
        borderLeftWidth: 0,
        flex: 0.33,
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        marginTop: 14,
        fontSize: 18,
        fontWeight: "bold",
        textAlignVertical: "center",
        textAlign: "center",
    },
    root: {
        flex: 0.64,
        borderRadius: 8,
        backgroundColor: "#fff",
    }
};

export default Premium;
