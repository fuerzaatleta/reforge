import React from 'react';
import {View, Text} from 'react-native';
import Modal from 'react-native-modal';
import {useSystemStore} from '../../mobx/system/system-store';

const Validation = (props) => {
    const system = useSystemStore();
    const visible = system.getFlag(props.id, false);
    return (
        <Modal
            animationIn="fadeIn"
            animationOut="fadeOut"
            backdropColor={"#000"}
            backdropOpacity={0.85}
            backdropTransitionOutTiming={0}
            isVisible={visible}
            onSwipeComplete={() => console.log("complete")}>
            <View style={styles.root}>
                <Text>Confirma tu Correo Electrónico</Text>
                <Text></Text>
            </View>
        </Modal>
    )
};

const styles = {
    root: {
        flex: 1,
        backgroundColor: "white",
        justifyContent: 'center',
        alignItems: 'center',
    },
};

export default Validation;
