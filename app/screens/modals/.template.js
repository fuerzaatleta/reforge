import React from 'react';
import {View} from 'react-native';
import Modal from 'react-native-modal';

const Template = () => {
    return (
        <Modal
            animationIn="fadeIn"
            animationOut="fadeOut"
            backdropColor={"#000"}
            backdropOpacity={0.85}
            backdropTransitionOutTiming={0}
            isVisible={false}
            onSwipeComplete={() => console.log("closed")}>
            <View style={styles.root}>
                /* Content */
            </View>
        </Modal>
    )
};

const styles = {
    root: {
        flex: 1,
        backgroundColor: "white",
    },
};

export default Template;
