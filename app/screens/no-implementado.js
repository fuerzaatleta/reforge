import React from 'react';
import {Text, View} from 'react-native';

const NoImplementado = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>No Implementado</Text>
        </View>
    )
};

const styles = {
    text: {
        fontSize: 17,
        flex: 1,
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
    }
};

export default NoImplementado;
