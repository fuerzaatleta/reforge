import React from 'react';
import {FlatList, Text, View} from 'react-native';
import {Calendar} from 'react-native-calendars';
import CalendarItem from '../../components/prototype/calendar-item';
import TouchableRipple from 'react-native-paper/src/components/TouchableRipple/TouchableRipple.native';

const TabAnalytics = () => {
    const color = "#ee3a2a";
    return (
        <View style={styles.container}>
            <Calendar
                style={styles.calendar}
                markingType={'period'}
            />
            <View style={styles.flatlist}>
                <FlatList
                    data={[1, 2]}
                    // ListHeaderComponent={() => <View style={styles.separator}/>}
                    // ListFooterComponent={() => <View style={styles.separator}/>}
                    ItemSeparatorComponent={() => <View style={styles.separator}/>}
                    showsVerticalScrollIndicator={false}
                    renderItem={() => <CalendarItem /> }
                    keyExtractor={item => item.toString()}
                />
                <TouchableRipple style={styles.button} onPress={() => console.log("asd")}>
                    <Text style={styles.buttonTitle}>REGISTRAR ACTIVIDAD</Text>
                </TouchableRipple>
            </View>
        </View>
    )
};

const Arrow = () => {
    return (
        <View>
            <Text>arrow</Text>
        </View>
    )
};

const styles = {
    buttonTitle: {
        fontSize: 15,
        color: "#727272",
    },
    calendar: {
        borderWidth: 1,
        marginTop: 18,
        margin: 12,
        borderRadius: 12,
        borderColor: "#d7d7d7",
        // padding: 8,
    },
    flatlist: {
        paddingLeft: 12,
        paddingRight: 12,
    },
    separator: {
        borderColor: "#d7d7d7",
        borderBottomWidth: 1,
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
    },
    button: {
        marginTop: 10,
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#e1e1e1",
        borderRadius: 8,
        padding: 13,
    },
};

export default TabAnalytics;
