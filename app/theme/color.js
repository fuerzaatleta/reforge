export const color = {
    primary: "#f42327",
    primaryDarker: "#fff",
    border: "#d1d1d1",
    line: "#e4e4e4",
    text: "#fff",
};
