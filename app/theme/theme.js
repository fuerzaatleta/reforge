export default {
    PRIMARY_COLOR: "rgb(232,56,27)",
    SECONDARY_COLOR: 'rgb(80,80,80)',
    SUCCESS_COLOR: 'rgb(48,169,63)',
    DANGER_COLOR: 'rgb(238,29,37)',
    INFO_COLOR: 'rgb(70,111,238)',
    SUBTITLE_COLOR: 'rgb(151,149,156)',
    DARK_FONT_COLOR: 'rgb(8,9,19)',
    CONTAINER_COLOR: "#ffffff",
    STATUSBAR_LIGHT_COLOR: "#ffffff",
    STATUSBAR_DARK_COLOR: "#f8f8f8",
    SEPARATOR_COLOR: "#d5d5d5",
    BLACK_BACKGROUND: "#141414",

    INPUT_COLOR: "#ececec",
    INPUT_RIPLE: "#c5c5c5",

    RIPPLE_COLOR: "#bbbbbb",
    RIPPLE_COLOR_DARK: "#6d6d6d",
    CONTAINER_PADDING: 20,

    // Categorías
    STRENGTH_HEADER: "#d62708",
    MUSCLE_HEADER: "#d62708",
    WEIGHT_LOSS_HEADER: "#d62708",

    h1: {
        color: "#121212",
        fontSize: 20,
        marginBottom: 14,
        fontWeight: "bold",
    },
    h2: {
        fontSize: 19,
        color: "#171717"
    },
    h3: {
        fontSize: 17,
        color: "#171717"
    },
    h4: {
        fontSize: 15,
        color: "#505050"
    },
    p: {
        fontSize: 16,
        color: "#171717"
    },
    center: {
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
        backgroundColor: "#fff",
        paddingLeft: 20,
        paddingRight: 20,
    },

    header: {
        backgroundColor: "#f8f8f8",
        elevation: 0,
    },

    img: {
        borderRadius: 12,
        resizeMode: 'cover',
    },
    row: {
        flexDirection: "row",
    },
    separator: {
        borderBottomWidth: 1,
        borderColor: "#d5d5d5",
        width: "88%",
    },
};
