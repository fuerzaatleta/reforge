import RegisterScreen from '../screens/login/register-screen';
import React from 'react';
import {View} from 'react-native'
import {createStackNavigator} from '@react-navigation/stack';
import StandardNoHeader from '../../src/framework/transitions/StandardNoHeader';
import RecoveryScreen from '../screens/login/recovery-screen';
import InstantTransition from '../../src/framework/transitions/InstantTransition';
import LoginScreen from '../screens/login/login-screen';
import PrivacyPolicy from '../screens/login/privacy-policy';
import TermsAndConditions from '../screens/login/terms-and-conditions';
import NoImplementado from '../screens/no-implementado';

const Stack = createStackNavigator();
const LoginNavigation = () => {
    return (
        <View style={{flex: 1}}>
            <Stack.Navigator>
                <Stack.Screen name="Register" component={RegisterScreen} options={InstantTransition()}/>
                <Stack.Screen name="Login" component={LoginScreen} options={InstantTransition()}/>
                <Stack.Screen name="Signin" component={RecoveryScreen} options={InstantTransition("Registrarse")}/>
                <Stack.Screen name="Privacy" component={PrivacyPolicy} options={InstantTransition("Política de Privacidad")}/>
                <Stack.Screen name="Terms" component={TermsAndConditions} options={InstantTransition("Términos & Condiciones")}/>
                <Stack.Screen name="NoImplementado" component={NoImplementado} options={StandardNoHeader} />
            </Stack.Navigator>
        </View>
    )
};

export default LoginNavigation;
