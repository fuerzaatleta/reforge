import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import StandardNoHeader from '../../src/framework/transitions/StandardNoHeader';
import WorkoutEditor from '../screens/tab-workout/gym/WorkoutEditor';
import WorkoutFinder from '../screens/tab-workout/gym/WorkoutFinder';
import SessionViewer from '../screens/tab-workout/designer/session-viewer';
import ExerciseViewer from '../screens/tab-exercises/aerobic/exercise-viewer';
import Sandbox from '../screens/tab-workout/gym/others/deprecated/profile/Sandbox';
import ChallengeScreen from '../screens/tab-workout/gym/others/deprecated/challenge/ChallengeScreen';
import CheckupScreen from '../screens/tab-workout/gym/others/deprecated/checkup/CheckupScreen';
import InstantTransition from '../../src/framework/transitions/InstantTransition';
import ExerciseInfo from '../screens/tab-workout/gym/tab-workout/exercise-info';
import ExerciseFinder from '../screens/tab-workout/designer/exercise-finder';
import CalendarScreen from '../screens/tab-workout/gym/others/deprecated/analytics/CalendarScreen';
import MetricsCreator from '../screens/tab-workout/gym/others/deprecated/profile/MetricsCreator';
import PremiumScreen from '../screens/tab-workout/gym/others/profile/subscreens/PremiumScreen';
import NotificationsScreen from '../screens/tab-workout/gym/others/profile/subscreens/NotificationsScreen';
import MetricsScreen from '../screens/tab-workout/gym/others/deprecated/profile/MetricsScreen';
import TrainingConfig from '../screens/tab-workout/gym/tab-profile/constitution';
import WelcomeScreen from '../screens/tab-workout/gym/others/deprecated/profile/Login/WelcomeScreen';
import VideoPlayer from '../screens/tab-workout/gym/others/deprecated/utils/VideoPlayer';
import Development from '../screens/tab-workout/gym/tab-profile/development';
import WorkoutViewer from '../screens/tab-workout/gym/tab-library/workout-viewer';
import AddSession from '../screens/tab-workout/designer/add-session';
import AddWorkout from '../screens/tab-workout/gym/tab-workout/add-workout';
import Filters from '../screens/tab-workout/gym/tab-workout/filters';
import ExerciseList from '../screens/tab-workout/gym/tab-analytics/exercise-list';
import Configuration from '../screens/tab-workout/gym/tab-profile/configuration';
import Warmup from '../screens/tab-workout/routines/warmup';
import Workouts from '../screens/tab-workout/routines/workouts';
import Stretch from '../screens/tab-workout/routines/stretch';
import AnaerobicWorkouts from '../screens/tab-workout/routines/anaerobic-workouts';
import HiitViewer from '../screens/tab-workout/routines/hiit-workout';
import Navigation from '../../src/framework/modules/other/Navigation';
import EquipmentConfig from '../screens/tab-exercises/equipment-config';
import Countdown from '../screens/modals/countdown';
import Premium from '../screens/modals/premium';
import {View} from 'react-native';
import Validation from '../screens/modals/validation';
import {observer} from 'mobx-react';
import {useNavigationStore} from '../mobx/navigation/navigation-store';
import NoImplementado from '../screens/no-implementado';
import DumbellWorkouts from '../screens/tab-workout/anaerobic/dumbell-workouts';
import KettlebellWorkouts from '../screens/tab-workout/anaerobic/kettlebell-workouts';
import ElasticbandWorkouts from '../screens/tab-workout/anaerobic/elasticband-workouts';
import Workout from '../screens/tab-workout/workout';
import Personalized from '../screens/tab-exercises/personalized/personalized';

const Stack = createStackNavigator();
const RootNavigation = observer(() => {
    const navigation = useNavigationStore();
    const workout = navigation.getCurrentWorkout();
    return (
        <View style={{flex: 1}}>
            {/* Modals */}
            <Countdown id="COUNTDOWN_MODAL" />
            <Premium id="PREMIUM_MODAL" />
            <Validation id="EMAIL_MODAL" />
            <Stack.Navigator>
                {/* Gym */}
                <Stack.Screen name="Navigation" component={Navigation} options={StandardNoHeader}/>
                <Stack.Screen name="HiitWorkout" component={HiitViewer} options={InstantTransition(workout.getTitle())}/>
                <Stack.Screen name="WorkoutViewer" component={WorkoutViewer} options={StandardNoHeader}/>
                <Stack.Screen name="WorkoutEditor" component={WorkoutEditor} options={StandardNoHeader}/>
                <Stack.Screen name="WorkoutFinder" component={WorkoutFinder} options={StandardNoHeader}/>
                <Stack.Screen name="ExerciseViewer" component={ExerciseViewer} options={InstantTransition()}/>
                <Stack.Screen name="ExerciseList" component={ExerciseList} options={StandardNoHeader}/>

                <Stack.Screen name="ChallengeScreen" component={ChallengeScreen} options={StandardNoHeader}/>
                <Stack.Screen name="Sandbox" component={Sandbox} options={StandardNoHeader}/>
                <Stack.Screen name="Configuration" component={Configuration} options={StandardNoHeader}/>
                <Stack.Screen name="CheckupScreen" component={CheckupScreen} options={StandardNoHeader}/>
                <Stack.Screen name="ExerciseInfo" component={ExerciseInfo} options={StandardNoHeader}/>
                <Stack.Screen name="CalendarScreen" component={CalendarScreen} options={StandardNoHeader}/>
                <Stack.Screen name="MetricsCreator" component={MetricsCreator} options={StandardNoHeader}/>
                <Stack.Screen name="PremiumScreen" component={PremiumScreen} options={InstantTransition()} />
                <Stack.Screen name="NotificationsScreen" component={NotificationsScreen} options={StandardNoHeader} />
                <Stack.Screen name="MetricsScreen" component={MetricsScreen} options={StandardNoHeader} />
                <Stack.Screen name="TrainingConfig" component={TrainingConfig} options={StandardNoHeader} />
                <Stack.Screen name="DevelopmentScreen" component={Development} options={StandardNoHeader} />
                <Stack.Screen name="WelcomeScreen" component={WelcomeScreen} options={StandardNoHeader} />
                <Stack.Screen name="VideoPlayer" component={VideoPlayer} options={StandardNoHeader} />
                <Stack.Screen name="PostLogin" component={WelcomeScreen} options={StandardNoHeader} />
                <Stack.Screen name="EquipmentConfig" component={EquipmentConfig} options={StandardNoHeader} />
                <Stack.Screen name="NoImplementado" component={NoImplementado} options={StandardNoHeader} />

                {/* General */}
                <Stack.Screen name="Workout" component={Workout} options={InstantTransition()}/>
                <Stack.Screen name="WorkoutCompleted" component={Filters} options={StandardNoHeader}/>
                <Stack.Screen name="Filters" component={Filters} options={InstantTransition("Filtros")}/>
                <Stack.Screen name="Personalized" component={Personalized} options={InstantTransition("Mis Rutinas")}/>

                {/* Anaeróbico */}
                <Stack.Screen name="Dumbell" component={DumbellWorkouts} options={InstantTransition("Mancuernas")} />
                <Stack.Screen name="ElasticBand" component={ElasticbandWorkouts} options={InstantTransition("Banda Elástica")} />
                <Stack.Screen name="Kettlebell" component={KettlebellWorkouts} options={InstantTransition("Kettlebell")} />

                {/* Bodyweight */}
                <Stack.Screen name="Warmup" component={Warmup} options={InstantTransition("Calentamiento")} />
                <Stack.Screen name="Workouts" component={Workouts} options={InstantTransition('Entrenamiento') } />
                <Stack.Screen name="AdvancedWorkouts" component={AnaerobicWorkouts} options={InstantTransition()} />
                <Stack.Screen name="Stretch" component={Stretch} options={InstantTransition('Estiramientos')} />

                {/* Designer */}
                <Stack.Screen name="AddSession" component={AddSession} options={StandardNoHeader}/>
                <Stack.Screen name="AddWorkout" component={AddWorkout} options={StandardNoHeader}/>
                <Stack.Screen name="SessionViewer" component={SessionViewer} options={StandardNoHeader}/>
                <Stack.Screen name="ExerciseFinder" component={ExerciseFinder} options={StandardNoHeader}/>
            </Stack.Navigator>
        </View>
    )
});
export default RootNavigation;
