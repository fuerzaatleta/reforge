import React,{useState} from 'react';
import {TouchableOpacity, Text, View} from 'react-native';
import {Icon} from "native-base";
import {WorkoutStoreInstance} from '../../../mobx/workout/deprecated-workout-store';

const Stepper = (props) => {
    const timer = WorkoutStoreInstance.tracker.timer;
    let [value, setValue] = useState(15);
    return (
        <View style={[props.style, styles.container]}>
            <TouchableOpacity style={styles.button} onPress={() => substract()}>
                <Icon style={styles.icon} type="FontAwesome" name="minus"/>
            </TouchableOpacity>
            <Text style={styles.counter}>{value} s</Text>
            <TouchableOpacity style={styles.button} onPress={() => add()}>
                <Icon style={styles.icon} type="FontAwesome" name="plus"/>
            </TouchableOpacity>
        </View>
    );

    function add() {
        timer.addTime(15)
    }

    function substract() {
        timer.substractTime(15);
    }
};

const styles = {
    container: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
    },
    button: {
        borderColor: "#acacac",
        padding: 12,
        borderRadius: 4,
        borderWidth: 1,
    },
    icon: {
        fontSize: 20,
    },
    counter: {
        fontSize: 18,
        paddingLeft: 15,
        paddingRight: 15,
    },
};

export default Stepper;
