import React, {useState, useEffect} from 'react';
import {Text, View, Image, ActivityIndicator} from 'react-native';
import Video from 'react-native-video';

const FRAME_SPEED = 1200;
let currentFrame = 0;
let title = "[UNTITLED_ITEM]";

const ExercisePreview = (props) => {
    setTitle(props.name);
    if(props.video != null) {
        return <VideoMode video={props.video}/>
    } else if (props.frames != null) {
        return <ImageMode frames={props.frames}/>
    } else {
        return <ErrorScreen/>
    }

    function setTitle(name) {
        if(name !== null)
            title = name;
    }
};

const ErrorScreen = (props) => {
    return (
        <View style={styles.errorScreen}>
            <Text style={styles.errorTitle}>Error al cargar recursos</Text>
            {/*<TouchableRipple style={styles.reload} onPress={() => alert("No Implementado")}>*/}
            {/*    <Text>Reintentar</Text>*/}
            {/*</TouchableRipple>*/}
        </View>
    )
};

const ImageMode = (props) => {
    let [image, setImage] = useState(props.frames[0]);
    let interval;
    useEffect(() => {
        interval = setInterval(() => repeat(), FRAME_SPEED);
        return () => clearInterval(interval);
    });
    return (
        <View style={styles.container}>
            <Image  style={[props.style, styles.image]} source={image} fadeDuration={0}/>
        </View>
    );
    function repeat() {
        if(currentFrame === 0) {
            setImage(props.frames[0]);
            currentFrame = 1;
        } else if(currentFrame === 1) {
            setImage(props.frames[1]);
            currentFrame = 0;
        }
    }
};

const VideoMode = (props) => {
    let [loading, setLoading] = useState(true);

    return (
        <View style={styles.videoContainer}>
            <ActivityIndicator animating={loading} style={styles.loading} size="large" color={"#e84225"} />
            <Video repeat
                   onReadyForDisplay={() => setLoading(false)}
                   resizeMode="cover"
                   source={{uri: props.video}}
                   style={styles.video}/>
               {/*<TouchableRipple borderless*/}
               {/*                 rippleColor={theme.INPUT_RIPLE}*/}
               {/*                 style={styles.touchable}*/}
               {/*                 onPress={() => app.navigateTo("ExerciseInfo")}>*/}
               {/*    <Text style={styles.title}>{title}</Text>*/}
               {/*</TouchableRipple>*/}
        </View>
    )
};

const styles = {
    reload: {
        padding: 8,
        paddingLeft: 25,
        paddingRight: 25,
        borderRadius: 6,
        marginTop: 7,
        borderColor: "#c3c3c3",
        backgroundColor: "rgba(102,185,255,0.13)",
    },
    errorTitle: {
        fontSize: 17,
    },
    errorScreen: {
        height: 280,
        width: 350,
        justifyContent: "center",
        alignItems: "center",
    },
    touchable: {
        borderRadius: 4,
        paddingTop: 4,
        paddingBottom: 4,
        marginBottom: -5,
    },
    title: {
        fontSize: 19,
        textAlignVertical: "center",
        textAlign: "center",
    },
    videoContainer: {
        backgroundColor: "white",
        marginTop: 30,
        width: "100%",
        height: 250,
    },
    video: {
        flex: 1,
        margin: 25,
        marginBottom: 15,
    },
    loading: {
        position: "absolute",
        marginLeft: "auto",
        marginRight: "auto",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
};
export default ExercisePreview;
