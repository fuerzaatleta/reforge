import React, {useState} from 'react';
import {Text, TextInput, View} from 'react-native';
import {Icon} from 'native-base';
import ExerciseSerial from './exercise-serial';
import {TouchableRipple} from 'react-native-paper';
import {useWorkoutStore} from '../../../../mobx/workout/deprecated-workout-store';
import {SwipeableRow} from '../../../../../src/components/deprecated/SwipeableRow';
import Collapsible from 'react-native-collapsible';
import ExerciseCheck from './exercise-check';
import {observer} from 'mobx-react';
import ExerciseLast from './exercise-last';
import Set from '../../../../utils/dataset/set';

const BG_COLOR = "#c5c5c5";

const ExerciseRow = observer((props) => {
    const index = props.index;
    const exercise = useWorkoutStore().exercise();
    const set = exercise.getExerciseSet(index);
    const color = set.completed ? style.active : style.disabled;
    let [collapsed, setCollapsed] = useState(false);
    return (
        <Collapsible onAnimationEnd={() => exercise.deleteSet(index - 1)} collapsed={collapsed}>
            <SwipeableRow
                onOpenRight={() => setCollapsed(true)}
                onOpenLeft={() => setCollapsed(true)}>
                <View style={[style.row, color]}>
                    <View style={[style.item, {flex: 0.12}]}>
                        <ExerciseSerial set={set} number={props.index}/>
                    </View>
                    <View style={[style.item, {flex: 0.35}]}>
                        <ExerciseLast set={set} />
                    </View>
                    <View style={[style.item, {flex: 0.32}]}>
                        <TextInput
                            value={set.weight.toString()}
                            onChangeText={value => set.setWeight(value)}
                            keyboardType="numeric"
                            style={style.input}/>
                    </View>
                    <View style={[style.item, {flex: 0.32}]}>
                        <TextInput
                            value={set.repetitions.toString()}
                            onChangeText={value => set.setRepetitions(value)}
                            keyboardType="numeric"
                            style={style.input}/>
                    </View>
                    <View style={[style.item, {flex: 0.1}]}>
                        <ExerciseCheck set={set}  />
                    </View>
                </View>
            </SwipeableRow>
        </Collapsible>
    );
});

function remove() {

}

const style = {
    active: {
        backgroundColor: "rgba(66,255,36,0.13)",
    },
    disabled: {
        backgroundColor: "white",
    },
    touchable: {
        padding: 6,
        borderRadius: 6,
    },
    lastBox: {
        justifyContent: 'center',
        alignItems: 'center',
        textAlignVertical: "center",
        textAlign: "center",
        borderRadius: 5,
    },

    icon: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    item: {
        // backgroundColor: "yellow",
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 12,
        flexDirection: "row",
        marginBottom: 6,
        marginTop: 6,
        margin: 4,
    },
    row: {
        paddingLeft: 8,
        paddingRight: 8,
        backgroundColor: "white",
        flexDirection: "row",
    },
    input: {
        // justifyContent: 'center',
        // alignItems: 'center',
        // textAlignVertical: "center",
        textAlign: "center",
        backgroundColor: "#ececec",
        color: "#0f0f0f",
        borderRadius: 6,
        fontSize: 16,
        padding: 2,
        flex: 1,
    },
    measure: {
        paddingTop: 16,
        fontSize: 17,
        color: "#858585",
        paddingLeft: 5,
    }
};

export default ExerciseRow;
