import React from 'react';
import {View, Text} from 'react-native';
import {TouchableRipple} from 'react-native-paper';
import {Icon} from "native-base";
import {useWorkoutStore} from '../../../../mobx/workout/deprecated-workout-store';
import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';

const ExerciseCheck = (props) => {
    let store = useWorkoutStore();
    if(store.tracker.running) {
        return <CheckMode set={props.set}/>
    } else {
        return <LockMode/>
    }
};

const LockMode = (props) => {
    return (
        <View>
            <Icon style={{fontSize: 22, color: "#bbbbbb"}} type="Entypo" name="lock"/>
        </View>
    )
};

const CheckMode = (props) => {
    const store = useWorkoutStore();
    const exercise = store.exercise();
    const bgStyle = props.set.completed ? styles.active : styles.disabled;
    return (
        <TouchableRipple
            borderless
            style={styles.check}
            onPress={() => complete() }>
            <View style={[styles.box, bgStyle]}>
                <Icon style={[{fontSize: 22, color: "gray"}, bgStyle]} type="MaterialIcons" name="check"/>
            </View>
        </TouchableRipple>
    );

    function complete() {
        if (props.set.complete()) {
            SoundPlayer.playSoundFile("check", 'mp3');
        }
        if (exercise.isComplete()) {
            store.nextExercise();
        }
    }
};

const styles = {
    active: {
        backgroundColor: "#3eb127",
        color: "white",
    },
    disabled: {

    },
    box: {
        backgroundColor: "#ececec",
        padding: 4,
    },
    check: {
        borderRadius: 5,
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
    }
};

export default ExerciseCheck;
