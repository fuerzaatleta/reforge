import React from 'react';
import {TouchableOpacity, Text, FlatList, View} from 'react-native';
import ExerciseRow from './exercise-row';
import {useWorkoutStore} from '../../../../mobx/workout/deprecated-workout-store';
import {observer} from 'mobx-react';

const SetTable = observer(() => {
    const exercise = useWorkoutStore().exercise();
    const length = exercise.sets.length;
    return (
        <View style={styles.container}>
            <FlatList
                style={styles.flatlist}
                data={exercise.sets}
                renderItem={({item, index}) => <ExerciseRow index={index}/>}
                ListFooterComponent={ () => <AddSet/>}
                keyExtractor={(item, index) => index.toString()}
            />
        </View>
    )
});

const AddSet = observer(() => {
    const exercise = useWorkoutStore().exercise();
    return (
        <View style={styles.center}>
            <TouchableOpacity onPress={() => exercise.addExerciseSet()} style={styles.addset}>
                <Text style={styles.title}>AÑADIR SERIE</Text>
            </TouchableOpacity>
        </View>
    )
});

const styles = {
    container: {
        flex: 1,
        backgroundColor: "#fff",
    },
    center: {
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        color: "#ff4c3f",
        fontSize: 14,
    },
    addset: {
        marginTop: 14,
        alignItems: 'center',
        borderColor: "#ff4c3f",
        borderWidth: 1,
        borderRadius: 6,
        padding: 8,
        width: "40%",
    }
};

export default SetTable;
