import React from 'react';
import {Text, View} from 'react-native';
import {TouchableRipple} from 'react-native-paper';
import theme from '../../../../theme/theme';

const ExerciseLast = () => {
    const reps = 10;
    const weight = 25;
    return (
        <TouchableRipple borderless
                         style={styles.touchable}
                         rippleColor={theme.INPUT_RIPLE}
                         onPress={() => console.log("nothing")}>
            <Text style={styles.last}>-</Text>
        </TouchableRipple>
    )
};

const styles = {
    last: {
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
        paddingRight: 6,
        paddingLeft: 6,
        color: "#6c6c6c",
    },
    touchable: {
        minWidth: 70,
        padding: 6,
        borderRadius: 6,
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
    }
};

export default ExerciseLast;
