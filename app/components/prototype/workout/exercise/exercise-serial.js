import React, {useState} from 'react';
import {Text, View} from 'react-native';
import theme from '../../../../theme/theme';
import {TouchableRipple} from 'react-native-paper';
import {ActionSheet} from "native-base";
import {SetTypes} from '../../../../utils/dataset/set';
import {observer} from 'mobx-react';
import {Menu, MenuOption, MenuOptions, MenuProvider, MenuTrigger} from 'react-native-popup-menu';

const ExerciseSerial = observer((props) => {
    const type = props.set.type;
    const set: Set = props.set;
    return (
        <View style={styles.container}>
            <Menu onSelect={value => set.setType(value)}>
                <MenuTrigger customStyles={triggerStyles} style={styles.set} text={getLabel()} />
                <MenuOptions style={styles.menu}>
                    <MenuOption style={styles.row} value={SetTypes.NORMAL} text='Normal' />
                    <MenuOption style={styles.row} value={SetTypes.WARMUP} text='Calentamiento' />
                    <MenuOption style={styles.row} value={SetTypes.MUSCLE_FAILURE} text='Fallo Muscular' />
                    <MenuOption style={styles.row} value={SetTypes.DROPSET} text='Dropset' />
                </MenuOptions>
            </Menu>
        </View>
    );

    function getLabel() {
        switch (props.set.type) {
            case SetTypes.NORMAL:
                return props.number + 1;
            case SetTypes.MUSCLE_FAILURE:
                return "F";
            case SetTypes.WARMUP:
                return "C";
            case SetTypes.DROPSET:
                return "D";
            default:
                return "ERR";
        }
    }

    function onPress() {
        const BUTTONS = ["1. Serie Normal", "2. Calentamiento",  "3. Fallo Muscular", "4. Dropset"];
        ActionSheet.show(
            {
                options: BUTTONS,
                label: "Sesión de Entrenamiento"
            },
            selection => { onActionSheetChanged(selection) }
        )
    }

    function onActionSheetChanged(value) {
        set.setType(value);
    }
});

const triggerStyles = {
    triggerText: {
        color: "#ff311d",
        fontSize: 15,
        fontWeight: "bold",
    },
    triggerOuterWrapper: {
        overflow: 'hidden',
        borderRadius: 6,
    },
    triggerWrapper: {
        padding: 6,
        borderRadius: 6,
    },
    triggerTouchable: {
        borderRadius: 6,
    },
};

const styles = {
    menu: {
        marginTop: 10,
        marginBottom: 10,
    },
    row: {
        fontSize: 20,
        padding: 12,
    },
    container: {
        flex: 1,
    },
    set: {
        color: "#ff311d",
        justifyContent: 'center',
        alignItems: 'center',


    },
};

export default ExerciseSerial;
