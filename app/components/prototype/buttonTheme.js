import Theme from '../../theme/theme';


export const white = {
    style: {
        elevation: 0,
    },
    content: {
        borderWidth: 1,
        borderColor: "rgba(15,15,16,0.21)",
        borderRadius: 4,
    },
    label: {
        color: "white",
    }
};

