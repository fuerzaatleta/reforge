import React from 'react';
import {Text, View} from 'react-native';
import {TouchableRipple} from 'react-native-paper';
import app from '../../../../src/AppFramework';
import {Icon} from "native-base";
import Theme from '../../../theme/theme';
import Share from 'react-native-share';


const BasicHeader = (props) => {
    return (
        <View style={styles.header}>
            <View style={{flex: 0.25}}>
                <TouchableRipple borderless style={styles.backTouchable} onPress={() => app.navigateBack()}>
                    <Icon style={{color: "#656565", fontSize: 27}} name='arrow-back' />
                </TouchableRipple>
            </View>
            <View style={[{flex: 1}, Theme.center]}>
                <Text style={styles.title}>{props.title}</Text>
            </View>
            <View style={{flex: 0.25}}>
                <TouchableRipple borderless style={styles.backTouchable} onPress={() => console.log("share")}>
                    <Icon style={{color: "#656565", fontSize: 23}} type="MaterialCommunityIcons" name='share-variant' />
                </TouchableRipple>
            </View>
        </View>
    )
};

function share() {
    Share.open(options)
        .then((res) => {
            console.log(res);
        })
        .catch((err) => {
            err && console.log(err);
        });
}

const styles = {
    title: {
        fontSize: 17,
    },
    container: {
        flex: 1,
        backgroundColor: "#fff",
    },
    header: {
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 15,
        // borderBottomWidth: 1,
        borderColor: Theme.SEPARATOR_COLOR,
        flexDirection: "row",
    },
    backTouchable: {
        width: 52,
        height: 52,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        marginRight: 7,
    },
};

export default BasicHeader;
