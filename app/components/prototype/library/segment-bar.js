import React from 'react';
import {View} from 'react-native';

let max = 4;
let value = 0;
const SegmentBar = (props) => {
    max = props.max;
    value = props.value;
    return (
        <View style={[props.style, styles.container]}>
            <Content/>
        </View>
    )
};

const Content = () => {
    let array = [];
    for(let i = 0; i < max; i++) {
        let active = i < value;
        array.push(<Segment active={active} />);
    }
    return array;
};

const Segment = (props) => {
    if(props.active) {
        return <View style={[styles.segment, styles.enabled]}/>
    } else {
        return <View style={[styles.segment, styles.disabled]}/>
    }
};

const styles = {
    enabled: {
        backgroundColor: "#ff342c",
    },
    disabled: {
        backgroundColor: "#d6d6d6",
    },
    segment: {
        height: 8,
        flex: 1,
        margin: 1,
    },
    container: {
        flexDirection: "row",
        backgroundColor: "#fff",
        justifyContent: 'center',
        alignItems: 'center',
    }
};

export default SegmentBar;
