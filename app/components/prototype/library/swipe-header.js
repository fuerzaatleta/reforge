import React from 'react';
import {Text, View} from 'react-native';
import {Icon} from "native-base";

const Header = (props) => {
    return (
        <View style={styles.root}>
            <View style={styles.left}>
                <Icon style={styles.icon} type="FontAwesome" name="chevron-left"/>
            </View>
            <View style={styles.center}>
                <Text style={styles.title}>{getTitle(props.index)}</Text>
                <Text style={styles.subtitle}>{getSubtitle(props.index)}</Text>
            </View>
            <View style={styles.right}>
                <Icon style={styles.icon} type="FontAwesome" name="chevron-right"/>
            </View>
        </View>
    )
};

function getTitle(index) {
    switch (index) {
        case 0: return "Popular";
        case 1: return "Principiante";
        case 2: return "Hipertrofia";
        case 3: return "En Casa";
    }
}

function getSubtitle(index) {
    switch (index) {
        case 0: return "Las rutinas con más exito entre los usuarios de reforge";
        case 1: return "Las rutinas con más exito entre los usuarios de reforge";
        case 2: return "Las rutinas con más exito entre los usuarios de reforge";
        case 3: return "Las rutinas con más exito entre los usuarios de reforge";
    }
}



const styles = {
    root: {
        paddingTop: 17,
        marginBottom: 20,
        flexDirection: "row",
    },
    left: {
        flex: 0.2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    center: {
        flex: 1,
    },
    right: {
        flex: 0.2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 27,
        textAlign: "center",
        marginTop: 8,
    },
    subtitle: {
        marginLeft: 10,
        marginRight: 10,
        fontSize: 16,
        textAlign: "center",
        color: "#414141",
    },
    icon: {
        fontSize: 21,
        color: "#323232",
    }
};

export default Header;
