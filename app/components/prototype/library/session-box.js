import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {Icon} from "native-base";
import {TouchableRipple} from 'react-native-paper';

const SessionBox = (props) => {
    let [open, setOpen] = useState(false);

    return (
        <TouchableRipple style={styles.root} borderless onPress={() => toggle()}>
                {open ? <OpenBox/> : <ClosedBox/>}
        </TouchableRipple>
    );

    function ClosedBox() {
        return (
            <View style={styles.container}>
                <View style={styles.left}>
                    <Text>Día {props.day}</Text>
                </View>
                <View style={styles.center}>
                    <Text style={styles.title}>{props.title}</Text>
                    <Text style={styles.subtitle}>11 ejercicios, 65 minutos</Text>
                </View>
                <View style={styles.right}>
                    <Icon style={styles.icon} type="FontAwesome" name="chevron-down"/>
                </View>
            </View>
        )
    }

    function OpenBox() {
        return (
            <View style={styles.container}>
                <Text>No implementado</Text>
            </View>
        )
    }

    function toggle() {
        setOpen(!open)
    }
};

const styles = {
    root: {
        borderRadius: 8,
        borderWidth: 1,
        borderColor: "#d3d3d3",
        marginTop: 13,
    },
    left: {
        marginRight: 25,
        justifyContent: 'center',
        alignItems: 'center',
    },
    center: {
        flex: 1,
    },
    right: {
        flex: 0.1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
        flexDirection: "row",
        padding: 26,
        paddingTop: 20,
        paddingBottom: 20,
        backgroundColor: "#fff",
    },
    title: {
        fontSize: 17,
    },
    subtitle: {
        color:'#848484',
    },
    icon: {
        fontSize: 18,
        color: "#828282",
    }
};

export default SessionBox;
