import React from 'react';
import {FlatList, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {color} from '../../theme/color';
import {Icon} from "native-base";
import {Content} from '../../mobx/content/content-store';
import {observer} from 'mobx-react';
import {User} from '../../mobx/user/user-store';

const Recommended = observer((props) => {
    // const lenght = User.getRecentWorkouts().length;
    const content = [];

    if (content.length <= 0) {
        return <Empty content={content} />
    } else {
        return <Widget content={content} />
    }

});

const Widget = (props) => {
    return (
        <View>
            <View style={[styles.container, props.style]}>
                <Icon style={styles.icon} type="MaterialCommunityIcons" name="history"/>
                <Text style={styles.text}>Rutinas Recientes</Text>
            </View>
            <FlatList
                data={props.content}
                initialNumToRender={6}
                showsVerticalScrollIndicator={false}
                renderItem={(props) => <Item {...props} />}
                keyExtractor={item => item}
            />
        </View>
    )
};

const Empty = (props) => {
    return (
        <View>
            <View style={[styles.container, props.style]}>
                <Icon style={styles.icon} type="MaterialCommunityIcons" name="history"/>
                <Text style={styles.text}>Rutinas Recientes</Text>
            </View>
            <View>
                <Text>No has realizado ninguna rutina</Text>
            </View>
        </View>
    )
};

const Item = (props) => {
    const workout = Content.workouts.getItem(props.item);
    return (
        <TouchableOpacity onPress={() => console.log("touchable")}>
            <View style={item.box}>
                <View style={{flex: 1}}>
                    <Text style={item.title}>Hipertrofia con Mancuernas</Text>
                    <Text style={item.subtitle}>Realizada hace 3 días</Text>
                </View>
                <Icon style={item.icon} type="MaterialCommunityIcons" name="chevron-right"/>
            </View>
        </TouchableOpacity>
    )
};

const item = {
    icon: {
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 24,
        color: "#343434",
    },
    box: {
        flexDirection: "row",
        borderWidth: 1,
        borderRadius: 12,
        padding: 12.5,
        paddingLeft: 20,
        marginTop: 10,
        borderColor: color.border,
    },
    title: {
        fontSize: 17,
        marginBottom: -3,
    },
    subtitle: {
        fontSize: 14,
        color: "#929292",
    }
};

const styles = {
    icon: {
        textAlignVertical: "center",
        textAlign: "center",
        fontSize: 26,
        paddingRight: 7,
    },
    container: {
        paddingTop: 22,
        paddingLeft: 5,
        flexDirection: "row",
        backgroundColor: "#fff",
        textAlignVertical: "center",
        textAlign: "center",
    },
    text: {
        color: "black",
        fontSize: 18,
        fontWeight: "bold",
    },
    touchable: {
        // color: "#e02600",
        color: "#404040",
        fontSize: 18,
        fontWeight: "bold",
        textDecorationLine: "underline",
    },
};

export default Recommended;
