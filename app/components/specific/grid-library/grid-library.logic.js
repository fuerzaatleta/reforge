import FastImage from 'react-native-fast-image';
import {Assets} from '../../../assets/Assets';
import {AppNavigation} from '../../../mobx/navigation/navigation-store';

export function onClicked(item) {
    if (item.id == null) {
        console.error("[COMPONENTS] Current workout is null");
    } else if(item.isPremium()) {
        alert("locked");
    } else {
        AppNavigation.viewWorkout(item);
    }
}

export function getOpacity(item) {
    if (item.isPremium()) {
        return {opacity: 0.4};
    } else {
        return {opacity: 1};
    }
}

export function getImage(item) {
    try {
        return {uri: item.thumbnail.data.thumbnails[4].url, FastImage};
    } catch {
        return Assets.PLACEHOLDER;
    }
}
