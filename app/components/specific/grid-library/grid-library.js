import React from 'react';
import {Text, ImageBackground, View} from 'react-native';
import {Icon} from 'native-base';
import {TouchableRipple} from 'react-native-paper';
import {Workout} from '../../../services/directus/workout';
import {getImage, getOpacity, onClicked} from './grid-library.logic';
import {styles} from './grid-library.styles';

const GridLibrary = (props) => {
    const item: Workout = props.item;
    return (
        <TouchableRipple borderless
                         style={styles.touchable}
                         onPress={() => onClicked(props.item)}>
            <View style={styles.container}>
                <ImageBackground
                    fadeDuration={0}
                    source={getImage(item)}
                    imageStyle={[styles.image, getOpacity(item)]}
                    style={styles.imageBackground}>
                    <Locked item={item}/>
                </ImageBackground>
                <Text style={styles.title}>{item.getTitle()}</Text>
                <Text style={styles.subtitle}>{item.getMuscleTarget()} • {item.getDuration()} Minutos</Text>
                <Text style={styles.description}>{item.getShortDescription()}</Text>
            </View>
        </TouchableRipple>
    );
};

const Locked = (props) => {
    if (!props.item.isPremium()) {
        return <View/>
    }
    return (
        <View style={styles.lockbox}>
            <Icon style={styles.icon} type="FontAwesome" name="lock"/>
            <Text style={styles.blocked}>BLOQUEADO</Text>
        </View>
    )
};

export default GridLibrary;
