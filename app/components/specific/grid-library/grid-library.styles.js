export const styles = {
    touchable: {
        borderRadius: 5,
        flex: 1,
    },
    lockbox: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    icon: {
        color: "white",
        fontSize: 19,
        marginBottom: -1,
    },
    blocked: {
        fontSize: 11,
        color: "white",
    },
    imageBackground: {
        borderRadius: 6,
        backgroundColor: "black",
        width: "100%",
        height: 115,
    },
    image: {
        borderRadius: 6,
        backgroundColor: "#ececec",
    },
    title: {
        marginTop: 5,
        color: "#111111",
        fontSize: 16,
        fontWeight: "bold",
    },
    subtitle: {
        fontSize: 12,
        color: "#4e4e4e",
    },
    description: {
        marginTop: 4,
        fontSize: 12,
        color: "#4e4e4e",
    },
    container: {
        margin: 7,
        marginBottom: 8,
        marginTop: 12,
    }
};
