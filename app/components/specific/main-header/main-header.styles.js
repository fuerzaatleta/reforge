export const styles = {
    selected: {
        backgroundColor: "#ebebeb",
    },
    tabView: {
        padding: 15,
        flexDirection: "row",
    },
    separator: {
        borderLeftWidth: 1,
        borderColor: "#d6d6d6",
        marginBottom: 7,
        marginTop: 7,
    },
    text: {
        fontSize: 15,
    },
    icon: {
        fontSize: 17,
        color: "#5b5b5b",
        paddingRight: 5,
    },
    tab: {
        borderRadius: 6,
        flexDirection: "row",
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
        flexDirection: "row",
        backgroundColor: "#fff",
        borderColor: "#cdcdcd",
        borderWidth: 1,

        margin: 6,
        borderRadius: 8,
    }
};
