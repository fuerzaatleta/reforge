
export function openTab(navigation, setSelected, tab) {
    navigation.jumpTo(tab);
    setSelected(tab);
}
