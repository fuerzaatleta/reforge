import React, {useState} from 'react';
import {Text, View} from 'react-native';
import {Icon} from "native-base";
import TouchableRipple from 'react-native-paper/src/components/TouchableRipple/TouchableRipple.native';
import {styles} from './main-header.styles';
import {openTab} from './main-header.logic';

/** Swich element from "tab-workout.js" **/
const MainHeader = ({ state, descriptors, navigation, position }) => {
    let [selected, setSelected] = useState("TAB1");
    let tab1style = selected === "TAB1" ? styles.selected : null;
    let tab2style = selected === "TAB2" ? styles.selected : null;
    return (
        <View style={styles.container}>
            <TouchableRipple
                borderless
                disabled={selected === "TAB1"}
                style={[tab1style, styles.tab]} onPress={() => openTab(navigation, setSelected, "TAB1")}>
                <View style={styles.tabView}>
                    <Icon style={styles.icon} type="Ionicons" name="flash"/>
                    <Text style={styles.text}>Rutinas</Text>
                </View>
            </TouchableRipple>
            <View style={styles.separator}/>
            <TouchableRipple
                borderless
                disabled={selected === "TAB2"}
                style={[tab2style, styles.tab]} onPress={() => openTab(navigation, setSelected, "TAB2")}>
                <View style={styles.tab}>
                    <Icon style={[styles.icon, {fontSize: 14.5}]} type="FontAwesome5" name="drafting-compass"/>
                    <Text style={styles.text}>Diseñador</Text>
                </View>
            </TouchableRipple>
        </View>
    );
};

export default MainHeader;
