import {color} from '../../../theme/color';

export const styles = {
    interior: {
        flexDirection: "row",
        textAlignVertical: "center",
        textAlign: "center",
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 15,
    },
    subtitle: {
        fontSize: 14,
        color: "#7e7e7e",
        marginTop: -2,
    },
    touchable: {
        marginBottom: 8,
        borderWidth: 1,
        borderRadius: 12,
        borderColor: color.border,
        paddingTop: 22,
        paddingBottom: 22,
        paddingLeft: 22,
        paddingRight: 15,
    }
};

export const icon = {
    touchable: {
        paddingLeft: 4,
        paddingRight: 4,
        paddingTop: 6,
        paddingBottom: 6,
        borderRadius: 5,
    },
    icon: {
        color: "#7a7a7a",
    }
};
