import app from '../../../../src/AppFramework';
import {ActionSheet} from "native-base";
import {log} from '../../../utils/logging';
import {WorkoutStoreInstance} from '../../../mobx/workout/workout-store';

export function OpenSession(session) {
    app.navigateTo("SessionViewer");
    WorkoutStoreInstance.select(session);
}

export function OpenConfig() {
    const BUTTONS = ["Editar", "Duplicar", "Eliminar"];
    const DESTRUCTIVE_INDEX = 3;
    const CANCEL_INDEX = 4;
    ActionSheet.show(
        {
            options: BUTTONS,
            cancelButtonIndex: CANCEL_INDEX,
            destructiveButtonIndex: DESTRUCTIVE_INDEX,
            label: "Sesión de Entrenamiento"
        },
        selection => { onActionSheet(selection) }
    )
}

function onActionSheet(selection) {
    switch (selection) {
        case 0:
            log.debug("selection is 0");
            break;
        case 1:
            log.debug("selection is 1");
            break;
        case 2:
            log.debug("selection is 2");
            break;
    }
}
