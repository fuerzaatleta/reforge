import React from 'react';
import {Text, View} from 'react-native';
import Session from '../../../utils/dataset/session';
import {TouchableRipple} from 'react-native-paper';
import {Icon} from "native-base";
import {icon, styles} from './designer-item.styles';
import {OpenConfig, OpenSession} from './designer-item.logic';

const DesignerItem = (props) => {
    const session: Session = props.item;
    return (
        <TouchableRipple borderless style={styles.touchable} onPress={() => OpenSession(session) }>
            <View style={styles.interior}>
                <View style={{flex: 1,}}>
                    <Text style={styles.title}>{session.name}</Text>
                    <Text style={styles.subtitle}>Realizada hace 4 días</Text>
                </View>
                <TouchableRipple style={icon.touchable} borderless onPress={() => OpenConfig() }>
                    <Icon style={icon.icon} type="MaterialCommunityIcons" name="dots-vertical"/>
                </TouchableRipple>
            </View>
        </TouchableRipple>
    )
};

export default DesignerItem;
