import React from 'react';
import {Text, Image, View} from 'react-native';
import Workout from '../../utils/dataset/workout';
import {useSystemStore} from '../../mobx/system/system-store';
import {TouchableRipple} from 'react-native-paper';

const VerticalLibrary = (props) => {
    const item: Workout = props.item;
    const system = useSystemStore();
    return (
        <TouchableRipple onPress={() => console.log("asd")}>
            <View style={styles.container}>
                <Image style={styles.img} />
                <View style={styles.box}>
                    <Text style={styles.title}>{item.name}</Text>
                    <Text style={styles.subtitle}>Et harum quidem rerum fac quidem.</Text>
                </View>
            </View>
        </TouchableRipple>
    )
};

const styles = {
    title: {
        fontSize: 18,
        // fontWeight: "bold",
    },
    subtitle: {
        color: "#7a7a7a",
    },
    img: {
        marginLeft: 8,
        backgroundColor: "black",
        borderRadius: 8,
        height: 100,
        width: 100,
    },
    container: {
        flex: 1,
        marginTop: 8,
        marginBottom: 8,
        paddingLeft: 5,
        paddingRight: 5,
        flexDirection: "row",
    },
    box: {
        paddingLeft: 15,
        justifyContent: 'center',
    }
};

export default VerticalLibrary;
