import React from 'react';
import {View, Image, ActivityIndicator} from 'react-native';

let parent = null;

/** Shows a loading indicator while downloading the image. **/
const AsyncThumbnail = (props) => {
    parent = props.style;
    return (
        <View style={styles.container}>
            <LoadingIndicator parentStyle={props.style} style={styles.loading}/>
            <Image square style={props.style} source={props.source} />
        </View>
    )
};

const LoadingIndicator = (props) => {
  return <ActivityIndicator size={"large"} color={"#ff3524"} style={props.style}/>;
};

const styles = {
    container: {
        flex: 1,
        backgroundColor: "#fff",
    },
    loading: {
        position: "absolute",
        zIndex: 2,
        top: 23,
        left: 23,
    }
};

export default AsyncThumbnail;
