import React from 'react';

/** tap on this element will open/close it **/
const Expandable = (props) => {
    if (props.state) {
        return props.open;
    } else {
        return props.closed;
    }
};

const styles = {
    container: {
        flex: 1,
        backgroundColor: "#fff",
    }
};

export default Expandable;
