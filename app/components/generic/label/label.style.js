export const styles = {
    on: {
        backgroundColor: "#8fa1ff",
        color: "white",
        fontSize: 13,
        padding: 8,
        paddingLeft: 12,
        paddingRight: 12,
    },
    off: {
        backgroundColor: "#9ae9ff",
        color: "black",
        fontSize: 13,
        padding: 8,
        paddingLeft: 12,
        paddingRight: 12,
    },
    touchable: {
        backgroundColor: "#9ae9ff",
        borderRadius: 25,
        marginRight: 6,
    },
};
