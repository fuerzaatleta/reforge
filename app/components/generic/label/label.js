import React, {useState} from 'react';
import {Text} from 'react-native';
import {useSystemStore} from '../../../mobx/system/system-store';
import TouchableRipple from 'react-native-paper/src/components/TouchableRipple/TouchableRipple.native';
import {select} from './label.logic';
import {styles} from './label.style';

const Label = (props) => {
    const system = useSystemStore();
    let [selected, setSelected] = useState(system.getFlag(props.id, false));
    let color = selected ? styles.on : styles.off;
    return (
        <TouchableRipple borderless
                         onPress={() => select(props.id, selected, setSelected)}
                         style={styles.touchable}>
            <Text style={color}>{props.name}</Text>
        </TouchableRipple>
    );
};
export default Label;
