import {System} from '../../../mobx/system/system-store';

export function select(id, selected, setSelected) {
    System.setFlag(id, selected, true);
    setSelected(!selected);
}
