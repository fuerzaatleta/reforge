import React from 'react';
import {ActivityIndicator, TouchableRipple} from 'react-native-paper';

/** Botón con indicador de actividad. **/
const AsyncButton = (props) => {
    return (
        <TouchableRipple style={props.style} onPress={props.onPress}>
            { props.loading ? <ActivityIndicator color="#fff" /> : props.children }
        </TouchableRipple>
    )
};

export default AsyncButton;
