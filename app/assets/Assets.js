
export class AssetManager {

    PLACEHOLDER = require("./images/placeholder.png");
    PLACEHOLDER_500 = require("./images/placeholder.png");
    TEST_AVATAR = require("./images/explore/grit.png");
    STATS = require("./images/placeholder.png");
    PREMIUM_FEATURE1 = require("./images/profile/analytics.png");
    PREMIUM_FEATURE2 = require("./images/profile/bench-press.png");
    PREMIUM_FEATURE3 = require("./images/profile/analytics.png");
    WORKOUT_MAP = require("./images/placeholder.png");
    WORKOUT_ALERT = require("./images/workout/broken.png");
    WELCOME_HELLO = require("./images/animations/welcome-hello.png");
    WELCOME_ANALYTICS = require("./images/animations/welcome-analytics.png");
    WELCOME_VIDEO = require("./images/animations/welcome-video.jpg");
    PHYSICAL_TEST = require("./images/workout/fast.png");
    RETO_DE_TITANES = require("./images/explore/reto-de-titanes.jpg");
    SEARCH = require("./images/explore/search.png");
    OPTIMIZATION_ICON = require("./images/optimization.png");
    EXERCISE_PLACEHOLDER = require("./images/placeholder/exercise.jpg");
    PREMIUM_SCREEN = require("./images/profile/premium.jpg");
    HIIT_CATEGORY = require("./images/workout/training_presentation.jpg");
    TAB1_DISABLED = require("./images/header/disabled_training.png");
    TAB2_DISABLED = require("./images/header/disabled_play.png");
    TAB3_DISABLED = require("./images/header/disabled_stats.png");
    TAB4_DISABLED = require("./images/header/disabled_user.png");
    TAB1_ACTIVE = require("./images/header/active_training.png");
    TAB2_ACTIVE = require("./images/header/active_play.png");
    TAB3_ACTIVE = require("./images/header/active_stats.png");
    TAB4_ACTIVE = require("./images/header/active_user.png");
    STRENGTH_TALENTTREE = require("./images/talents/strength-talenttree.jpg");
    ARROW_DOWN = require("./images/talents/arrow.png");
    EMPTY = require("./images/workout/empty.png");
    SUCCESS_SOUND = require("../../android/app/src/main/res/raw/check.mp3");
    DIAMOND = require("./images/modals/diamond.png");
    LOGO_WHITE = require("./images/logo-white.png");

    /* Categories */
    CATEGORY_WORKOUT = require("./images/categories/workout.jpeg");
    CATEGORY_WARMUP = require("./images/categories/warmup.jpeg");
    CATEGORY_STRETCH = require("./images/categories/stretch.jpeg");
    CATEGORY_PRACTICE = require("./images/categories/practice.jpeg");

    /* Equipment */
    EQUIPMENT_DUMBELL = require("./images/equipment/dumbell.png");
    EQUIPMENT_KETTLEBELL = require("./images/equipment/kettlebell.png");
    EQUIPMENT_TRX = require("./images/equipment/trx.png");

    constructor() {
        this.items = new Map();
        this.items.set("HIIT", require("./images/workout/hiit_icon.png"));
        this.items.set("DUMBELL", require("./images/workout/dumbell.png"));
        this.items.set("KETTLEBELL", require("./images/workout/kettlebell.png"));
        console.log("Asset initialize has finished. Size: " + this.items.size);
    }

    get(assetName) {
        return this.items.get(assetName);
    }

}

export const Assets = new AssetManager();
