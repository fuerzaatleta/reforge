
/** Configuración del servicio de persistencia **/
export const Preferences = {
    DB_USER: '',
    DB_PASSWORD: '',
    TABLES: ['workout', "user_settings"],
};
