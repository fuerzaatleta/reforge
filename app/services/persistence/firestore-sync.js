import firestore from '@react-native-firebase/firestore';
import * as AsyncStorage from 'react-native/Libraries/Storage/AsyncStorage';
import {User} from '../../mobx/user/user-store';
import {Preferences} from './config';

/** Sincroniza AsyncStorage (almacenamiento local) con Firebase
 *  Firestore (almacenamiento online) **/
class FirestoreSync {

    async load() {
        config.forEach((table) => this.loadDocument(table));
    }

    async save() {
        console.info("Initializing Persistence save. (UID: " + User.uid + ")");
        Preferences.TABLES.forEach((table) => this.saveDocument(table));
        console.info("End of persistent save");
    }

    reset() {
        console.info("Reseting storage...");
        AsyncStorage.clear();
    }

    async saveDocument(table) {
        const document = await AsyncStorage.getItem(table);
        if(document == null || table == null) {
            console.warn("Null document received")
        } else {
            firestore()
                .collection(table)
                .doc(User.uid)
                .set(JSON.parse(document))
                .then(() => {})
        }
    }

    async loadDocument(table) {
        const doc = await AsyncStorage.getItem(table);
        if(doc != null) {
            console.info("Document already exists")
        } else {
            firestore()
                .collection(table)
                .doc(User.uid)
                .get()
        }
    }
}
export default FirestoreSync;
