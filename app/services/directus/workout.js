export class Workout {

    id; title; thumbnail; short_description; description;
    muscle_target; time; premium; repetitions; content; equipment; category;

    getID() {
        if (this.id == null) {
            return "[NULL]";
        } else {
            return this.id;
        }
    }

    getTitle() {
        if (this.title == null) {
            return "[NULL]";
        } else {
            return this.title;
        }
    }

    getThumbnail() {
        return {uri: this.thumbnail.data.full_url}
    }

    getShortDescription() {
        if (this.short_description == null) {
            return "[NULL]";
        } else {
            return this.short_description;
        }
    }

    getDescription() {
        if (this.description == null) {
            return "[NULL]";
        } else {
            return this.description;
        }
    }

    getMuscleTarget() {
        if (this.muscle_target == null) {
            return "[NULL]";
        } else if (this.muscle_target === "0") {
            return "Cuerpo Completo"
        } else if (this.muscle_target === "1") {
            return "Tren Superior"
        } else if (this.muscle_target === "2") {
            return "Tren Inferior"
        } else if (this.muscle_target === "3") {
            return "Abdominales"
        }
    }

    getDuration() {
        if (this.time == null) {
            return this.time;
        } else {
            return this.time;
        }
    }

    isPremium() {
        if (this.premium == null) {
            return false;
        } else {
            return this.premium;
        }
    }

    getRounds() {
        if (this.repetitions == null) {
            return 0;
        } else {
            return this.repetitions;
        }
    }

    getCategory() {
        if (this.category == null) {
            return -1;
        } else {
            return this.category;
        }
    }

    getEquipment() {
        if (this.equipment == null) {
            return "[NULL]";
        } else {
            return this.equipment;
        }
    }

    getContent() {
        if (this.content == null) {
            return null;
        } else {
            return this.content;
        }
    }
}
