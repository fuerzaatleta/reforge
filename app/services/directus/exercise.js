export class Exercise {

    id; thumbnail; video; name; difficulty;
    description; targetMuscles; equipment;

    getID() {
        if (this.id == null) {
            return 0;
        } else {
            return this.id;
        }
    }

    getThumbnail() {
        return {uri: this.thumbnail.data.full_url}
    }

    getVideo() {
        if(this.video == null) {
            console.error("[EXERCISE] Empty variable. VIDEO: " + this.video);
        } else {
            return this.video.data.full_url;
        }
    }

    getName() {
        return this.getTitle();
    }

    getTitle() {
        if (this.name == null) {
            return "[NULL]";
        } else {
            return this.name;
        }
    }

    getDescription() {
        if (this.description == null) {
            return "[NULL]";
        } else {
            return this.description;
        }
    }

    getDifficulty() {
        if (this.difficulty == null) {
            return "[NULL]";
        } else if(this.difficulty == 0) {
            return "Fácil";
        } else if(this.difficulty == 1) {
            return "Intermedio";
        } else if (this.difficulty == 2) {
            return "Difícil";
        }
    }

    getEquipment() {
        if (this.equipment == null) {
            return "[NULL]";
        } else if(this.equipment == 0) {
            return "Ninguno";
        } else if(this.equipment == 1) {
            return "Mancuernas";
        } else if (this.equipment == 2) {
            return "Kettlebell";
        } else if (this.equipment == 3) {
            return "Banda Elástica";
        } else if (this.equipment == 4) {
            return "Barra de Dominadas";
        } else if (this.equipment == 5) {
            return "Rueda de Abdominales";
        }
    }

    getTargetMuscles() {
        if (this.targetMuscles) {
            return "[NULL]";
        } else {
            return this.targetMuscles;
        }
    }
}
export default Exercise;
