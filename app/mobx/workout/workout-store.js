import {createContext, useContext} from 'react';
import Designer from '../user/designer';
import {makeAutoObservable} from 'mobx';
import Exercise from '../../utils/dataset/exercise';
import Session from '../../utils/dataset/session';
import WorkoutTracker from './workout-tracker';
import {log} from '../../utils/logging';

class WorkoutStore {

    MAX_ITEMS = 3;
    constructor() {
        this.recent = [];
        this.tracker = new WorkoutTracker();
        this.designer = new Designer();
        makeAutoObservable(this);
    }

    select = (selection) => {
        if (selection == null) {
            log.debug("you're selecting a null item on workout-store.js");
        } if (selection instanceof Session) {
            this.selectedSession = selection;
        } else if (selection instanceof Exercise) {
            this.selectedExercise = selection;
        }
    };

    exercise = (): Exercise => {
        if(this.selectedExercise == null)
            this.selectedExercise = new Exercise();
        return this.selectedExercise;
    };

    session = (): Session => {
        if(this.selectedSession == null)
            this.selectedSession = new Session();
        return this.selectedSession;
    };

    reset() {
        this.workouts = [];
        this.selectedWorkout = null;
        this.selectedSession = null;
        this.selectedWorkout = null;
        this.workoutsLoaded = true;
    }

    /** If the user starts a program it will show as recent **/
    addRecentWorkout(id) {
        if (id == null) {
            log.debug("Ignoring recent workout");
        } else if (this.recent.length > this.MAX_ITEMS) {
            this.recent.shift();
        } else {
            this.recent.push(id);
            log.debug("Recent workout added... " + id);
            save();
        }
    }

    /** Returns the latest completed workouts **/
    getRecentWorkouts() {
        return this.recent;
    }

}

export const WorkoutStoreInstance = new WorkoutStore();
export const context = createContext(WorkoutStoreInstance);
export function useWorkoutStore(): WorkoutStore {
    return useContext(context);
}
