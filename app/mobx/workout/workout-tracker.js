import {makeAutoObservable} from 'mobx';
import Timer from '../../utils/timer';
import app from '../../../src/AppFramework';
import {Alert} from 'react-native';
// import {WorkoutStoreInstance} from './workout-store';

class WorkoutTracker {

    isPreparing = false;
    running = false;
    timer = new Timer();
    showRestTimer = false;

    constructor() {
        makeAutoObservable(this);
    }

    preparation() {
        if(this.running) {
            app.navigateTo("ExerciseViewer");
        } else {
            app.navigateTo("ExerciseViewer");
            this.isPreparing = true;
            this.running = false;
        }
    }

    start() {
        WorkoutStoreInstance.setExerciseIndex(0);
        this.running = true;
        this.isPreparing = false;
    }

    stop(confirmation = false) {
        if(confirmation) {
            Alert.alert(
                'Abandonar sesión',
                'Está a punto de abandonar la sesión de entrenamiento ¿desea continuar?',
                [
                    { text: 'NO', onPress: () => console.info('resuming workout') },
                    { text: 'SI', onPress: () => this.forceStop() }
                ],
                { cancelable: false }
            );
        } else {
            this.forceStop();
        }
    }

    forceStop() {
        WorkoutStoreInstance.session().reset();
        app.navigateTo("WorkoutCompleted");
        console.info("stopping tracker");
        this.running = false;
        this.isPreparing = false;
    }

    complete() {
        this.timer.start();
        this.showRestTimer = true;
    }

    showTimer(value) {
        this.showRestTimer = value;
    }

    countdown() {
        return this.timer;
    }

}
export default WorkoutTracker;
