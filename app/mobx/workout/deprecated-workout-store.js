// import {makeAutoObservable} from 'mobx';
// // import Session from './session';
// // import Exercise from './exercise';
// import Workout from '../../utils/dataset/workout';
// import {createContext, useContext} from 'react';
// import * as AsyncStorage from 'react-native/Libraries/Storage/AsyncStorage';
// import WorkoutTracker from './workout-tracker';
// import {log} from '../../utils/logging';
//
// class OldWorkoutStore {
//     selectedWorkout: Workout = null;
//     selectedSession: Session = null;
//     selectedExercise: Exercise = null;
//     selectedExerciseIndex = null;
//     tracker = new WorkoutTracker();
//     workouts = [];
//     workoutsLoaded = false;
//
//     constructor() {
//         makeAutoObservable(this);
//         this.load();
//     }
//
//     workout = (): Workout => {
//         if(this.selectedWorkout == null)
//             this.selectedWorkout = new Workout();
//         return this.selectedWorkout;
//     };
//
//     session = (): Session => {
//         if(this.selectedSession == null)
//             this.selectedSession = new Session();
//         return this.selectedSession;
//     };
//
//     exercise = (): Exercise => {
//         if(this.selectedExercise == null)
//             this.selectedExercise = new Exercise();
//         return this.selectedExercise;
//     };
//
//     reset() {
//         this.workouts = [];
//         this.selectedWorkout = null;
//         this.selectedSession = null;
//         this.selectedWorkout = null;
//         this.workoutsLoaded = true;
//     }
//
//     createWorkout(workout) {
//         if(workout == null) {
//             console.warn("workout is null");
//         } else if(!(workout instanceof Workout)) {
//             console.info("parameter is not a workout")
//         } else {
//             const workout = new Workout();
//             this.workouts.push(workout);
//             this.save();
//         }
//     }
//
//     setWorkouts(workouts) {
//         if(workouts == null) {
//             console.info("stored workouts is empty");
//             this.workouts = [];
//         } else if(workouts.length > 0 && !(workouts[0] instanceof Workout)) {
//             console.error("ERROR: JSON instead of Workout item.");
//             this.workouts = [];
//         } else {
//             this.workouts = workouts;
//         }
//     }
//
//     getWorkouts() {
//         if (this.workouts === null) {
//             console.warn("ALERT: Stored workouts is null");
//             return [];
//         } else if (this.workouts.length === 0) {
//             return this.workouts;
//         } else if (!(this.workouts[0] instanceof Workout)) {
//             console.error("ERROR: JSON instead of Workout item");
//         } else {
//             return this.workouts;
//         }
//     };
//
//     setLoaded(value) {
//         this.workoutsLoaded = value;
//     }
//
//     isLoaded() {
//         return this.workoutsLoaded;
//     }
//
//     select(item) {
//         if(item == null) {
//             console.warn("[WARN] Selecting null item");
//         } else if(item instanceof Workout) {
//             this.selectedWorkout = item;
//         } else if(item instanceof Session) {
//             this.selectedSession = item;
//         } else if(item instanceof Exercise) {
//             this.selectedExercise = item;
//         }
//     };
//
//     setExerciseIndex(value) {
//         this.selectedExerciseIndex = value;
//     }
//
//     nextExercise() {
//         if (this.selectedSession == null) {
//             console.warn("workout is null")
//         } else if (this.selectedExerciseIndex + 1 > this.selectedSession.count()) {
//             alert("finished");
//         } else {
//             const item = this.selectedSession.exercises[this.selectedExerciseIndex + 1];
//             this.select(item);
//         }
//     }
//
//     toJSON() {
//         const workouts = [];
//         this.workouts?.forEach((value) => { workouts.push(value.toJSON()); });
//         const json = {};
//         json.selectedWorkout = this.selectedWorkout?.toJSON();
//         json.workouts = workouts;
//         return json;
//     }
//
//     save() {
//         try {
//             const json = this.toJSON();
//             const string = JSON.stringify(json);
//             AsyncStorage.setItem("workout", string);
//             console.log(json);
//         } catch(error) {
//             console.warn("Unable to save workout. " + error);
//         }
//     }
//
//     async load() {
//         const json = await AsyncStorage.getItem("workout");
//         const deserialized = JSON.parse(json);
//         if (json == null) {
//             log.debug("Deserialization JSON is null");
//             this.setLoaded(true);
//         } else if (deserialized == null) {
//             log.info("Deserialization content is null");
//             this.setLoaded(true);
//         } else {
//             const workouts = [];
//             deserialized.workouts.forEach((item) => {
//                 const element = new Workout().deserialize(item);
//                 workouts.push(element);
//             });
//             this.selectedWorkout = new Workout().deserialize(deserialized.selectedWorkout);
//             this.setWorkouts(workouts);
//             this.setLoaded(true);
//         }
//     }
//
// }
// export const WorkoutStoreInstance = new OldWorkoutStore();
// export const context = createContext(WorkoutStoreInstance);
// export function useWorkoutStore(): OldWorkoutStore {
//     return useContext(context);
// }
