import {makeAutoObservable} from 'mobx';
import React, {createContext, useContext} from 'react';
import RNBootSplash from "react-native-bootsplash";
import Countdown from './countdown';
import * as AsyncStorage from 'react-native/Libraries/Storage/AsyncStorage';
import {log} from '../../utils/logging';

class SystemStore {

    constructor() {
        makeAutoObservable(this);
        this.countdown = new Countdown();
        this.flags = [];
        AsyncStorage.getItem("PERSISTENT_FLAGS")
            .then((result) => this.pFlags = result)
            .catch((err) => log.warn(err))
            .finally(() => {
                const result = + this.pFlags != null ? "OK" : "ERR";
                console.info("flags persistence... [ " + result +" ]")
            });
    }

    setFlag(id, value, persistent = false) {
        if (this.pFlags == null) {
            this.pFlags = [];
        }

        if (id == null) {
            log.warn("[FLAG] Flag ID cannot be null.");
        } else if (value == null) {
            log.warn("[FLAG] Flag value cannoe be null.");
            this.flags[id] = "";
            this.pFlags[id] = "";
        } else if (persistent) {
            this.pFlags[id] = value;
            AsyncStorage.setItem("PERSISTENT_FLAGS", this.pFlags.toString());
        } else {
            log.debug("Flag " + id+ " has been set to " + value);
            this.flags[id] = value;
            AsyncStorage.setItem(id, value.toString())
        }
    }

    getFlag(id, defaultValue = "") {
        let value = this.flags[id];
        if (value == null) {
            return defaultValue;
        } else {
            return this.flags[id];
        }
    }

    navigateTo(screen) {
        this.navigation.navigateTo(screen);
    }

    finishSplash() {
        RNBootSplash.hide({ fade: true })
    }

    setNavigation(navigation) {
        this.navigation = navigation;
    }
}

export const System = new SystemStore();
export const Context = createContext(System);
export function useSystemStore(): SystemStore {
    return useContext(Context);
}
