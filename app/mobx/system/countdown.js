import {makeAutoObservable} from 'mobx';
import app from '../../../src/AppFramework';
import {User} from '../user/user-store';
import {AppNavigation} from '../navigation/navigation-store';

class Countdown {

    constructor() {
        this.COUNTDOWN_TIME = 4;
        this.timer = this.COUNTDOWN_TIME;
        this.visible = false;
        this.interval = null;
        makeAutoObservable(this);
    }

    show(visible) {
        if(!(typeof visible === "boolean")) {
            console.warn("Parameter must be boolean");
        } else if (visible === true) {
            // app.navigateTo("Workout");
            this.visible = true;
            this.reset();
            if(this.interval === null)
                this.interval = setInterval(() => this.update(), 1000);
        } else if (visible === false) {
            this.visible = false;
            clearInterval(this.interval);
            this.interval = null;
        }
    }

    update() {
        this.timer -= 1;
        if(this.timer <= 0) {
            this.complete();
        }
    }

    getTime() {
        return this.timer;
    }

    isVisible() {
        return this.visible;
    }

    reset() {
        this.timer = this.COUNTDOWN_TIME;
    }

    complete() {
        const workout = AppNavigation.getCurrentWorkout();
        this.show(false);
        app.navigateTo("Workout");
        // User.addRecentWorkout(workout.getID());
        console.log("Countown Completed. Selected workout... " + workout.getID());
    }

    cancel() {
        console.log("countdown: CANCELED");
        this.show(false);
    }

    skip() {
        console.log("countdown: SKIPPED");
        this.show(false);
    }
}
export default Countdown;
