import {makeAutoObservable} from 'mobx';
import {createContext, useContext} from 'react';
import WorkoutDB from './workout-db';
import ExerciseDB from './exercise-db';

class PersistenceStore {

    exercises = new ExerciseDB();
    workouts = new WorkoutDB();

    constructor() {
        makeAutoObservable(this);
    }

}

export const Content = new PersistenceStore();
export const Context = createContext(Content);
export function useContentStore(): PersistenceStore {
    return useContext(Context);
}
