import Collection from '../../utils/collection';
import {Exercise} from '../../services/directus/exercise';
import {Content} from './content-store';
import {makeAutoObservable} from 'mobx';
import {log} from '../../utils/logging';

export default class ExerciseDB {

    loaded = false;

    constructor() {
        makeAutoObservable(this);
        this.params = "?fields=thumbnail.data.full_url," + "video.data.full_url," + "*";
        this.collection = new Collection(Exercise, "/items/exercises_v2", this.params);
        this.collection.initialize()
            .then(() => Content.exercises.setLoaded(true));
    }

    setLoaded(value) {
        this.loaded = value;
    }

    isLoaded() {
        return this.loaded;
    }

    getCollection(): [] {
        return this.collection.getCollection();
    }

    getItem(id): Exercise {
        let item = this.collection.getCollection().find((value) => value.getID() == id);
        if(item == null) {
            console.warn("[MOBX] Item is null.");
            return null;
        } else {
            return item;
        }
    }

    searchByMuscle(id): Exercise {
        if(id == null) {
            console.info("id is null");
            return [];
        } else {
            let content = this.collection.getCollection();
            let result = content.filter((value) => value.muscle_group == id.toString());
            if(result === null || result === undefined)
                result = [];
            return result;
        }
    }

    searchByName(input: String) {
        return this.collection.searchByName(input);
    }
}
