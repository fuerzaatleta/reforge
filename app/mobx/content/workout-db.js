import Collection from '../../utils/collection';
import {Workout} from '../../services/directus/workout';

export const Equipment = {
    NO_EQUIPMENT: 0,
    DUMBELL: 1,
    KETTLEBELL: 2,
    ELASTIC_BAND: 3,
};

export default class WorkoutDB {

    constructor() {
        this.params = "?fields=thumbnail.data.full_url, *";
        this.collection = new Collection(Workout, "/items/workouts", this.params);
        this.collection.initialize();
    }

    getCollection(): [] {
        return this.collection.getCollection();
    }

    getItem(id): Workout {
        let item = this.collection.getCollection().find((value) => value.getID() === id);
        if(item == null) {
            console.warn("[MOBX] Result is null.");
            return null;
        } else {
            return item;
        }
    }

    getByEquipment(equipment: Equipment): [] {
        let item = this.collection.getCollection().filter((value) => value.getEquipment() == equipment);
        if(item == null) {
            console.warn("[MOBX] Result is null.");
            return null;
        } else {
            return item;
        }
    }

    getCategory(category): [] {
        let item = this.collection.getCollection().filter((value) => value.getCategory() == category);
        if(item == null) {
            console.warn("[MOBX] Item is null.");
            return null;
        } else {
            return item;
        }
    }

}
