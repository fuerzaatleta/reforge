import {makeAutoObservable} from 'mobx';
import {createContext, useContext} from 'react';
import app from '../../../src/AppFramework';
import {System} from '../system/system-store';
import {Workout} from '../../services/directus/workout';
import {Exercise} from '../../services/directus/exercise';

class NavigationStore {

    currentExercise = null;
    currentWorkout = null;
    showPremiumPopup = false;

    constructor() {
        makeAutoObservable(this);
    }

    /** Navega hacia un "workout" **/
    viewWorkout(workout) {
        if(workout == null) {
            console.warn("[SYSTEM] Null. Unable to open workout.")
        } else {
            this.currentWorkout = workout;
            System.setFlag("CURRENT_WORKOUT", workout.getID());
            app.navigateTo("HiitWorkout");
            // app.navigateTo("WorkoutViewer");
        }
    }

    /** Navega hacia un "exercise" **/
    viewExercise(exercise) {
        if(exercise == null) {
            console.warn("[SYSTEM] Null. Unable to open exercise.")
        } else {
            this.currentExercise = exercise;
            app.navigateTo("ExerciseViewer");
            System.setFlag("SELECTED_EXERCISE", exercise.getID())
        }
    }

    getCurrentWorkout() {
        if (this.currentWorkout == null) {
            return new Workout();
        } else {
            return this.currentWorkout;
        }
    }

    getCurrentExercise() {
        if (this.currentExercise == null) {
            return new Exercise();
        } else {
            return this.currentExercise;
        }
    }

}

export const AppNavigation = new NavigationStore();
export const Context = createContext(AppNavigation);
export function useNavigationStore(): NavigationStore {
    return useContext(Context);
}
