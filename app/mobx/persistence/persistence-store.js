import {makeAutoObservable} from 'mobx';
import {createContext, useContext} from 'react';
import Firestore from '../../services/persistence/firestore-sync';
import {Alert, ToastAndroid} from 'react-native';
import {WorkoutStoreInstance} from '../workout/deprecated-workout-store';

class PersistenceStore {

    constructor() {
        this.persistence = new Firestore();
        this.workout = WorkoutStoreInstance;
        makeAutoObservable(this);
    }

    save() {
        this.workout.save();
        // this.persistence.save();
    }

    reset() {
        Alert.alert(
            'Resetear datos',
            'Está a punto de purgar los datos de este dispositivo ¿desea continuar?',
            [
                { text: 'NO', onPress: () => console.info('clear canceled') },
                { text: 'SI', onPress: () => {
                    this.persistence.reset();
                    this.workout.reset();
                    ToastAndroid.show("Almacenamiento local reseteado", ToastAndroid.SHORT)
                }}
            ],
            { cancelable: false }
        );
    }

}

export const Persistence = new PersistenceStore();
export const Context = createContext(Persistence);
export function usePersistenceStore(): PersistenceStore {
    return useContext(Context);
}
