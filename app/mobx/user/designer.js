import Session from '../../utils/dataset/session';
import {log} from '../../utils/logging';
import {makeAutoObservable} from 'mobx';

class Designer {

    constructor() {
        this.sessions = [];
        makeAutoObservable(this);
    }

    createSession(name, weekday) {
        const session = new Session(name, weekday);
        if(session == null) {
            log.debug("Unable to create: session is null.");
        } else if(!(session instanceof Session)) {
            log.info("Unable to create: not instance of session")
        } else {
            log.info("Session created... " + session);
            this.sessions.push(session);
        }
    }

    session() {
        return this.getSession(0);
    }

    getSession(index): Session {
        let session = this.sessions[index];
        if (session == null) {
            log.warn("invalid session");
            return new Session("", "");
        } else {
            return session;
        }
    }

    getSessions() {
        return this.sessions;
    }

}
export default Designer;
