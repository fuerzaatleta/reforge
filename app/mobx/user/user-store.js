import {makeAutoObservable} from 'mobx';
import {createContext, useContext} from 'react';
import {Alert} from 'react-native';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';
import {LoginManager, AccessToken} from 'react-native-fbsdk';
import {config} from '../../config';
import firestore from '@react-native-firebase/firestore';
import Billing from './billing';
import Designer from './designer';

class UserStore {

    constructor() {
        this.recent = [];
        this.designer = new Designer();
        this.billing = new Billing();
        this.authenticated = false;
        this.authenticatingWithGoogle = false;
        this.authenticatingWithFacebook = false;
        this.billing.initialize();
        auth().onAuthStateChanged(this.onAuthentication.bind(this));
        makeAutoObservable(this);
    }

    /** If the user starts a program it will show as recent **/
    addRecentWorkout(id) {
        console.log("Recent workout added... " + id);
        this.recent.push(id);
        if (this.recent.length > 3) {
            this.recent.shift();
        }
    }

    /** Returns the latest completed workouts **/
    getRecentWorkouts() {
        return this.recent;
    }

    get uid() {
        return this.currentUser.uid;
    }

    onAuthentication() {
        this.currentUser = auth().currentUser;
        let value = auth().currentUser != null;
        this.setAuthenticated(value);
    }

    createUser(name, lastname, email, password) {
        if(email == "" || password == "") {
            console.warn("El correo o contraseña están vacíos");
        }  else {
            console.log("name: " + name);
            console.log("lastname: " + lastname);
            console.log("email:" + email);
            console.log("password:" + password);
            auth()
                .createUserWithEmailAndPassword(email, password)
                .then(() => success())
                .catch(error => {
                    if (error.code === 'auth/email-already-in-use') {
                        Alert.alert("Error", "El correo electrónico ya está en uso.");
                    } else if (error.code === 'auth/invalid-email') {
                        Alert.alert("Error", "El correo electrónico es inválido");
                    }
                });

            function success() {
                auth().currentUser
                    .sendEmailVerification()
                    .done();
                firestore()
                    .collection("user_data")
                    .doc(this.uid)
                    .set({name: name, lastname: lastname})
                    .then(() => {})
            }
        }
    }

    authwithCredentials(email, password) {
        auth()
            .signInWithEmailAndPassword(email, password)
            .then(() => console.log("User account created"))
            .catch(error => {
                Alert.alert("Error", "Los datos introducidos no son válidos.");
            })
    }

    authWithGoogle() {
        if (this.authenticatingWithGoogle || this.authenticatingWithFacebook) {
            console.log("No action. User is already authenticating.")
        } else {
            this.authenticatingWithGoogle = true;
            GoogleSignin.configure({webClientId: config.AUTH_CLIENT_ID});
            this.googleAuth()
                .then(() => {
                    this.setAuthenticated(true);
                    console.log("Authentication state: " + this.authenticated ? "CONECTED" : "DISCONECTED");
                })
                .catch((error) => {
                    console.log("Authentication failed. " + error);
                })
                .finally(() => this.authenticatingWithGoogle = false)
        }
    }

    authWithFacebook() {
        if (this.authenticatingWithFacebook || this.authenticatingWithGoogle) {
            console.log("No action. User is already authenticating.")
        } else {
            this.authenticatingWithFacebook = true;
            this.facebookAuth()
                .then(() => {
                    console.log("Authenticated with Facebook");
                })
                .catch((error) => {
                    console.log("Facebook Auth Failed. " + error);
                })
                .finally(() => this.authenticatingWithFacebook = false)
        }
    }

    signout() {
        Alert.alert(
            'Cerrar Sesión',
            'Está a punto de cerrar la sesión ¿desea continuar?',
            [
                { text: 'NO', onPress: () => console.info('Sign out Cancel') },
                { text: 'SI', onPress: () => this.signoutAction() }
            ],
            { cancelable: false }
        );
    }

    signoutAction() {
        if(auth().currentUser != null) {
            this.setAuthenticated(false);
            auth().signOut().then(response => {
                console.log("Logout");
            });
        }
    }

    /** Autenticación con Google **/
    async googleAuth() {
        const { idToken } = await GoogleSignin.signIn();
        const googleCredential = auth.GoogleAuthProvider.credential(idToken);
        return auth().signInWithCredential(googleCredential)
    }

    /** Autenticación con Facebook **/
    async facebookAuth() {
        const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);
        if (result.isCancelled)
            throw 'User cancelled the login process';
        const data = await AccessToken.getCurrentAccessToken();
        if (!data)
            throw 'Something went wrong obtaining access token';
        const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);
        return auth().signInWithCredential(facebookCredential)
    }

    setAuthenticated(value) {
        this.authenticated = value;
    }

    hasInternet() {
        // TODO
        return true;
    }

    /** User has purchased premium **/
    isPremium() {
        return false;
    }

    /** User has been authenticated **/
    isAuthenticated() {
        return this.authenticated;
    }

    /** Email has been verified. **/
    isEmailVerified() {
        return auth().currentUser.emailVerified;
    }
}

export const User = new UserStore();
export const context = createContext(User);
export function useUserStore(): UserStore {
    return useContext(context);
}
