import * as RNIap from 'react-native-iap';
import {getSubscriptions} from 'react-native-iap';
import {log} from '../../utils/logging';

class Billing {

    SKU_TIER3 = "tier3_subscription";
    SKU_TIER2 = "tier2_subscription";
    SKU_TIER1 = "tier1_subscription";

    async initialize() {
        try {
            const result = await RNIap.initConnection();
            if (result) {
                log.info("in-app purchases... [ OK ]");
            }
        } catch (err) {
            console.warn(err.code, err.message);
        }
    }

    async requestSubscription(sku) {
        try {
            const subscriptions = [this.SKU_TIER1, this.SKU_TIER2, this.SKU_TIER3];
            const sub = await RNIap.getSubscriptions(subscriptions);
            console.log(sub);
            const request = await RNIap.requestSubscription("tier3_subscription");
        } catch (err) {
            console.warn(err.code, err.message);
        }
    }

    unsubscribe() {

    }

    async hasPurchased() {
        try {
            const sku = [this.SKU_TIER1, this.SKU_TIER2, this.SKU_TIER3];
            const purchases = await getSubscriptions(sku);
            return purchases[0] != null;
        } catch (err) {
            console.warn(err.code, err.message);
        }
    }

}
export default Billing;
