import Collection from '../app/utils/collection';
import {Workout} from '../app/services/directus/workout';
import Exercise from '../app/services/directus/exercise';

/* Testing Parameters */
const collections = ["workouts", "exercises_v2"]

test('[1] Connects to server', async () => {
    for (let i = 0; i < collections.length; i++) {
        const collection = new Collection(Workout, "/items/" + collections[i]);
        let connected = await collection.isConnected();
        expect(connected).toBe(true);
    }
});

test('[2] Has permissions', async () => {
    for (let i = 0; i < collections.length; i++) {
        const collection = new Collection(Workout, "/items/" + collections[i]);
        const result = await collection.hasPermission();
        expect(result).toBe(true);
    }
});

test('[3] Downloads the content', async () => {
    for (let i = 0; i < collections.length; i++) {
        const collection = new Collection(Workout, "/items/" + collections[i]);
        await collection.download();
        let result = collection.getCollection();
        expect(result).not.toBeUndefined();
    }
});
